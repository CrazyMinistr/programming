import java.io.*
import java.util.*

//fun max(a : Int, b : Int) : Int = if (a > b) a else b
//data class makePair(var X : Int, var Y : Int)

fun main(args : Array<String>) {
    val sc = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = sc.nextInt()
    var a = IntArray(n + 1)
    for (i in 1..n)
        a[i] = sc.nextInt()

    val m = sc.nextInt()
    var b = IntArray(m + 1)
    for (i in 1..m)
        b[i] = sc.nextInt()

    var f = Array(n + 1) { IntArray(m + 1) }
    var prevX = Array(n + 1) { IntArray(m + 1) }
    var prevY = Array(n + 1) { IntArray(m + 1) }

    for (i in 1..n) {
        for (j in 1..m) {
            if (a[i] == b[j]) {
                f[i][j] = f[i - 1][j - 1] + 1
                prevX[i][j] = i - 1
                prevY[i][j] = j - 1
            } else {
                if (f[i - 1][j] > f[i][j - 1]) {
                    prevX[i][j] = i - 1
                    prevY[i][j] = j
                } else {
                    prevX[i][j] = i
                    prevY[i][j] = j - 1
                }
                f[i][j] = Math.max(f[i - 1][j], f[i][j - 1])
            }
        }
    }
    var size = f[n][m]
    println(size)

    var ans = IntArray(size)
    var px = n
    var py = m

    while (px > 0 && py > 0) {
        if (px - 1 == prevX[px][py] && py - 1 == prevY[px][py]) {
            ans[--size] = a[px]
        }

        var t = px
        px = prevX[t][py]
        py = prevY[t][py]
    }

    for (i in 0..ans.size - 1)
        print("${ans[i]} ")
}

class FastScanner(val reader: BufferedReader) {
    private var st: StringTokenizer? = null
    fun next(): String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext(): Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
