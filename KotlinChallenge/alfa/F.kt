import java.io.*
import java.util.*

val inf : Int = Integer.MAX_VALUE

class Edge(val _from : Int, val _to : Int, val _capacity : Long, val _flow : Long) {
    var from = _from
    var to = _to
    var capacity = _capacity
    var flow = _flow
    var backEdge : Edge ?= null
}

class Graph (N : Int) {
    var n = N
    val s = 0
    val t = n - 1
    var d = IntArray(n)
    var idx = IntArray(n)
    var g = Array(n) { ArrayList<Edge>() }

    public fun addEdge(from : Int, to : Int, capacity : Long) {
        var e1 = Edge(from, to, capacity, 0)
        var e2 = Edge(to, from, 0, 0)
        e1.backEdge = e2
        e2.backEdge = e1
        g[from].add(e1)
        g[to].add(e2)
    }

    private fun bfs() : Boolean {
        for (i in 0 .. n - 1)
            d[i] = inf

        d[s] = 0
        var q = ArrayDeque<Int>()
        q.add(s)
        while (!q.isEmpty() && d[t] == inf) {
            var cur : Int = q.poll() as Int
            for (e : Edge in g[cur]) {
                if (d[e.to] == inf && e.flow < e.capacity) {
                    d[e.to] = d[cur] + 1
                    q.add(e.to)
                }
            }
        }
        return d[t] != inf
    }

    private fun dfs(cur : Int, flow : Long) : Long {
        if (flow.equals(0))
            return 0

        if (cur == t)
            return flow

        while (idx[cur] < g[cur].size()) {
            var e = g[cur].get(idx[cur])
            if (d[e.to] != d[e.from] + 1) {
                ++idx[cur]
                continue
            }
            var f = dfs(e.to, Math.min(flow, e.capacity - e.flow))
            if (f > 0) {
                e.flow += f
                e.backEdge!!.flow -= f
                return f
            }
            ++idx[cur]
        }
        return 0
    }

    public fun maxFlow() : Long {
        var flow : Long = 0
        while (true) {
            if (!bfs())
                break

            for (i in 0 .. n - 1)
                idx[i] = 0

            var f : Long = 0
            do {
                f = dfs(s, inf.toLong())
                flow += f
            } while (f > 0)
        }
        return flow
    }
}

fun main(args : Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = inp.nextInt()
    val m = inp.nextInt()
    var g = Graph(n)

    for (i in 1 .. m) {
        val from = inp.nextInt() - 1
        val to = inp.nextInt() - 1
        val capacity = inp.nextLong()
        g.addEdge(from, to, capacity)
    }
    println(g.maxFlow())
}

class FastScanner(val reader : BufferedReader) {
    private var st : StringTokenizer? = null
    fun next() : String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext() : Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
