/**
 * Created with IntelliJ IDEA.
 * User: ministr
 * Date: 22.10.13
 * Time: 8:35
 * To change this template use File | Settings | File Templates.
 */

import java.util.Scanner

fun check(x : Int) : Boolean {
    var i = 2
    while (i * i <= x) {
        if (x % i == 0)
            return false

        ++i
    }
    return true
}

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    var ans = 0
    for (i in 2..n / 2) {
        ans += if (check(i) && check(n - i)) 1 else 0
    }

    println(ans)
}
