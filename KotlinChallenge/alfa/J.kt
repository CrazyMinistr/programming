import java.io.*
import java.util.*

fun main(args : Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val t : String = inp.readLine().toString()
    var s : String = inp.readLine().toString()
    val y = s.length
    s += "$" + t
    val n = s.length
    var p = Array(n) {0}
    var x = 0
    var ans = 0
    for (i in 1..n - 1) {
        while (x > 0 && s[i] != s[x])
            x = p[x - 1]

        if (s[i] == s[x])
            ++x

        p[i] = x
        if (x == y)
            ++ans
    }
    println(ans)
}

class FastScanner(val reader : BufferedReader) {
    private var st : StringTokenizer? = null
    fun next() : String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext() : Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
