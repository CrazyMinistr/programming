import java.io.*
import java.util.*


fun main(args : Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = inp.nextInt()
    val m = inp.nextInt()
    val s = inp.nextInt()
    var x = IntArray(m + 1)
    var y = IntArray(m + 1)
    var z = LongArray(m + 1)
    for (i in 1..m) {
        x[i] = inp.nextInt()
        y[i] = inp.nextInt()
        z[i] = inp.nextLong()
    }

    var inf : Long = 3000000000000000000
    var d = LongArray(n + 1)
    for (i in 1..n)
        d[i] = inf

    d[s] = 0
    var iter = 0
    var repeat : Boolean = true
    while (repeat) {
        repeat = false
        for (i in 1..m)
            if (d[x[i]] < inf && d[y[i]] > -inf) {
                var w : Long = d[x[i]] + z[i]
                if (w < d[y[i]]) {
                    repeat = true
                    if (iter <= n) {
                        if (w > inf)
                            w = inf

                        if (w < -inf)
                            w = -inf
                        d[y[i]] = w
                    } else
                        d[y[i]] = -inf
                }
            }
        ++iter
    }

    for (i in 1..n)
        if (d[i] == inf)
            println("*")
        else if (d[i] == -inf)
            println("-")
        else
            println(d[i])
}

class FastScanner(val reader : BufferedReader) {
    private var st : StringTokenizer? = null
    fun next() : String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext() : Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
