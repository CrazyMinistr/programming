/**
 * Created with IntelliJ IDEA.
 * User: ministr
 * Date: 22.10.13
 * Time: 7:58
 * To change this template use File | Settings | File Templates.
 */

import java.util.Scanner

fun sum(n : Int) : Int {
    return n % 10 +
        (n / 10) % 10 +
        (n / 100) % 10
}

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    val m = n / 1000
    println(when (sum(n) == sum(m)) {
        true -> "Lucky ticket"
        else -> "Unlucky ticket"
    })
}
