import java.io.*
import java.util.*

data class Queue(var x : Int, var y : Int, var z : Int)

fun main(args : Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    var used = Array(7) { Array(7) { BooleanArray(7) } }
    var q = Array(300) { Queue (0, 0, 0) }
    var q1 = 0
    var q2 = 0
    val a = inp.nextInt()
    val b = inp.nextInt()
    val c = inp.nextInt()
    var from = Array(7) { Array(7) { Array(7) { Queue(0, 0, 0) } } }

    fun bfs(x : Queue, prev : Queue) : Unit {
        if (x.x < 1 || x.y < 1 || x.z < 1 || x.x > a || x.y > b || x.z > c || used[x.x][x.y][x.z])
            return

//        println("${prev.x} ${prev.y} ${prev.z} -> ${x.x} ${x.y} ${x.z}")
        used[x.x][x.y][x.z] = true
        from[x.x][x.y][x.z] = prev
        q[q1++] = x
    }

    bfs(Queue(1, 1, 1), Queue(0, 0, 0))
    while (q1 != q2) {
        val cur = q[q2]
//        println(cur)
        bfs(Queue(cur.x, cur.y + 1, cur.z + 2), cur)
        bfs(Queue(cur.x, cur.y + 1, cur.z - 2), cur)

        bfs(Queue(cur.x, cur.y - 1, cur.z + 2), cur)
        bfs(Queue(cur.x, cur.y - 1, cur.z - 2), cur)

        bfs(Queue(cur.x, cur.y + 2, cur.z + 1), cur)
        bfs(Queue(cur.x, cur.y + 2, cur.z - 1), cur)

        bfs(Queue(cur.x, cur.y - 2, cur.z + 1), cur)
        bfs(Queue(cur.x, cur.y - 2, cur.z - 1), cur)

        bfs(Queue(cur.x + 1, cur.y, cur.z + 2), cur)
        bfs(Queue(cur.x + 1, cur.y, cur.z - 2), cur)

        bfs(Queue(cur.x - 1, cur.y, cur.z + 2), cur)
        bfs(Queue(cur.x - 1, cur.y, cur.z - 2), cur)

        bfs(Queue(cur.x + 2, cur.y, cur.z + 1), cur)
        bfs(Queue(cur.x + 2, cur.y, cur.z - 1), cur)

        bfs(Queue(cur.x - 2, cur.y, cur.z + 1), cur)
        bfs(Queue(cur.x - 2, cur.y, cur.z - 1), cur)

        bfs(Queue(cur.x + 1, cur.y + 2, cur.z), cur)
        bfs(Queue(cur.x + 1, cur.y - 2, cur.z), cur)

        bfs(Queue(cur.x - 1, cur.y + 2, cur.z), cur)
        bfs(Queue(cur.x - 1, cur.y - 2, cur.z), cur)

        bfs(Queue(cur.x + 2, cur.y + 1, cur.z), cur)
        bfs(Queue(cur.x + 2, cur.y - 1, cur.z), cur)

        bfs(Queue(cur.x - 2, cur.y + 1, cur.z), cur)
        bfs(Queue(cur.x - 2, cur.y - 1, cur.z), cur)

        ++q2
    }

    var cur = from[a][b][c]
    if (cur.x == 0 || cur.y == 0 || cur.z == 0) {
        println("NO")
        return
    }

    var ans = ArrayList<Queue>()

    while (cur.x > 0) {
        ans.add(Queue(cur.x, cur.y, cur.z))
        cur = from[cur.x][cur.y][cur.z]
    }
//    println(ans)
    println("YES")
//    println(ans.get(0))
    for (i in ans.size() - 1 downTo 0)
        println("${ans.get(i).x} ${ans.get(i).y} ${ans.get(i).z}")
    println("$a $b $c")
}

class FastScanner(val reader : BufferedReader) {
    private var st : StringTokenizer? = null
    fun next() : String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext() : Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
