import java.io.*
import java.util.*

fun main(args : Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = inp.nextInt()
    var rmq = Array(20) { IntArray(n + 1) }
    for (i in 1 .. n)
        rmq[0][i] = inp.nextInt()

    var log = IntArray(n + 1)
    for (i in 2 .. n)
        log[i] = log[i shr 1] + 1

    for (i in 1..log[n])
        for (j in 1 .. n - (1 shl (i - 1)))
            rmq[i][j] = Math.min(rmq[i - 1][j], rmq[i - 1][j + (1 shl (i - 1))])

    val m = inp.nextInt()
    for (i in 1 .. m) {
        var l = inp.nextInt()
        var r = inp.nextInt()
        if (l > r) {
            val t = l
            l = r
            r = t
        }
        val w = log[r - l + 1]
        println(Math.min(rmq[w][l], rmq[w][r + 1 - (1 shl w)]))
    }
}

class FastScanner(val reader : BufferedReader) {
    private var st : StringTokenizer? = null
    fun next() : String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext() : Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
