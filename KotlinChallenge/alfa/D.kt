import java.util.Scanner

val inf = 1000000000

fun min(x : Int, y : Int) : Int = if (x < y) x else y

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    val m = sc.nextInt()

    var ms : Array<Array<Int>>
    ms = Array(n + 1) { Array(n + 1) { inf } }

    for (i in 1..m) {
        val x = sc.nextInt()
        val y = sc.nextInt()
        val z = sc.nextInt()
        ms[x][y] = min(ms[x][y], z)
    }

    for (k in 1..n)
        for (i in 1..n)
            for (j in 1..n)
                if (i != j)
                    ms[i][j] = min(ms[i][j], ms[i][k] + ms[k][j])

    for (i in 1..n)
    {
        for (j in 1..n)
            print(if (ms[i][j] == inf) "0 " else "${ms[i][j]} ")
        println("")
    }
}
