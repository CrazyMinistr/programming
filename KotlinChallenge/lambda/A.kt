import java.io.*
import java.util.*

fun main(args: Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    var n = inp.nextInt()
    for (i in 1..n) {
        var a = inp.nextInt()
        var b = inp.nextInt()
        var c = inp.nextInt()
        var d = inp.nextInt()
        println(if (c <= a && d <= a - c + b) "YES" else "NO")
    }
}

class FastScanner(val reader: BufferedReader) {
    private var st: StringTokenizer? = null
    fun next(): String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext(): Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
