import java.io.*
import java.util.*

fun is_prime(x: Int): Boolean {
    if (x == 1)
        return false

    var i = 2
    while (i * i <= x) {
        if (x % i == 0)
            return false

        ++i
    }
    return true
}

fun main(args: Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = inp.nextInt()
    var sum = 0
    for (i in 2 .. n) {
        if (is_prime(i))
            sum += i
    }
    println(sum)
    println(if (is_prime(sum)) "YES" else "NO")
}

class FastScanner(val reader: BufferedReader) {
    private var st: StringTokenizer? = null
    fun next(): String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext(): Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
