import java.io.*
import java.util.*

fun main(args: Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val n = inp.nextInt()
    val k = inp.nextInt()
    val s = inp.next()
    var cnt = IntArray(26)
    var l = 0
    var r = 0
    var len = 0
    var start = 0
    while (r < n) {
        val cur: Int = s[r] - 'a'
        ++cnt[cur]
        while (cnt[cur] > k) {
            --cnt[s[l] - 'a']
            ++l
        }
        if (r - l + 1 > len) {
            len = r - l + 1
            start = l
        }
        ++r
    }
    println("${len} ${start + 1}")
}

class FastScanner(val reader: BufferedReader) {
    private var st: StringTokenizer? = null
    fun next(): String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext(): Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
