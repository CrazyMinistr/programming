import java.io.*
import java.util.*
import com.sun.xml.internal.fastinfoset.util.StringArray

fun main(args: Array<String>) {
    val inp = FastScanner(BufferedReader(InputStreamReader(System.`in`)))
    val t = inp.nextInt()
    for (T in 1..t) {
        val n = inp.nextInt()
        val s = inp.next()
        var res = IntArray(n)
        for (i in 0 .. n - 1)
            res[i] = 1 // and's

        var find = false
        for (i in n - 1 downTo 0) {
            if (s[i] == '1') {
                find = true
                var j = i
                while (j > 0) {
                    --j
                    if (s[j] == '1')
                        continue

                    res[j] = 0
                    break
                }
                if (i < n - 1)
                    res[i] = 0
                break
            }
        }
        if (find) {
            for (i in 0 .. n - 3)
                print(if (res[i] == 0) "or " else "and ")
            println(if (res[n - 2] == 0) "or" else "and")
        } else
            println("Impossible")
    }
}

class FastScanner(val reader: BufferedReader) {
    private var st: StringTokenizer? = null
    fun next(): String {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine())
        return st!!.nextToken()
    }
    fun hasNext(): Boolean {
        while (!(st?.hasMoreTokens() ?: false)) st = StringTokenizer(reader.readLine() ?: return false)
        return true
    }
    fun nextInt() = next().toInt()
    fun nextLong() = next().toLong()
    fun nextFloat() = next().toFloat()
    fun nextDouble() = next().toDouble()
    fun readLine() = reader.readLine()
}
