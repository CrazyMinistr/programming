# -*- coding: utf-8 -*-
from pyPdf import PdfFileWriter, PdfFileReader
import ghostscript
import Image
import os
import glob
from xml.dom.minidom import *

def parse_xml(path, xml_name):
    os.chdir(path)
    report_dict = []
    try:
        xml = parse(open(xml_name, 'r'))
        xml_preflight_result_entries = xml.getElementsByTagName('PreflightResultEntry')

        for entry in xml_preflight_result_entries:
            type_of_entry = entry.getAttribute('type')
            if type_of_entry != "Fix" and type_of_entry != "Check":
                continue

            level = entry.getAttribute('level')
            message = entry.getElementsByTagName('Message')[0].firstChild.nodeValue
            string_context = entry.getElementsByTagName('StringContext')[0]
            constants = string_context.getElementsByTagName('Const')
            instances = string_context.getElementsByTagName('Instance')

            rectangles = []
            # for instance in instances:
            locations = string_context.getElementsByTagName('Location')
            rectangle_set = set()
            rectangle_set.clear()

            for location in locations:
                maxX = (float(location.getAttribute('maxX')))
                maxY = (float(location.getAttribute('maxY')))
                minX = (float(location.getAttribute('minX')))
                minY = (float(location.getAttribute('minY')))
                new_rectangle = dict(maxX=maxX, maxY=maxY, minX=minX, minY=minY, top=0, left=0, width=0, height=0)
                if not new_rectangle in rectangle_set:
                    rectangles.append(new_rectangle)
                    rectangle_set.add(new_rectangle)

            for const in constants:
                if const.getAttribute('name') == 'ActionID':
                    action_id = int(const.firstChild.nodeValue)

                    type = "fix"
                    if type_of_entry == "Check":
                        type = level

                    report_dict.append(dict(type=type, message=message, locations=rectangles))
        return report_dict
    except:
        raise
        # print "!!! xml parser failed :( !!!"
        # return report_dict


def convert(pdf, jpg):
    args = ["-q", "-dSAFER", "-dBATCH", "-dNOPAUSE", "-sDEVICE=jpeg", "-r150",
            "-dTextAlphaBits=4", "-dGraphicsAlphaBits=4", "-dMaxStripSize=8192",
            "-sOutputFile=" + jpg, pdf]
    ghostscript.Ghostscript(*args)


# def get_errors():
#     # maxX="297.741" maxY="231.889" minX="44.5645" minY="215.971"
#     # maxX="253.36" maxY="285.114" minX="44.6606" minY="260.039"
#     # maxX="404.399" maxY="590.482" minX="45.4032" minY="546.033"
#     # maxX="246.246" maxY="74.9011" minX="109.704" minY="30.1895"
#
#     def add(input_pdf, maxX, maxY, minX, minY):
#         page = input_pdf.getPage(0)
#         page.mediaBox.upperRight = (maxX, maxY)
#         page.mediaBox.lowerLeft = (minX, minY)
#         output.addPage(page)
#
#     output = PdfFileWriter()
#     input_pdf1 = PdfFileReader(file("A5.pdf", "rb"))
#     input_pdf2 = PdfFileReader(file("A5.pdf", "rb"))
#     input_pdf3 = PdfFileReader(file("A5.pdf", "rb"))
#     input_pdf4 = PdfFileReader(file("A5.pdf", "rb"))
#
#     add(input_pdf1, 297.741, 231.889, 44.5645, 215.971)
#     add(input_pdf2, 253.36, 285.114, 44.6606, 260.039)
#     add(input_pdf3, 404.399, 590.482, 45.4032, 546.033)
#     add(input_pdf4, 246.246, 74.9011, 109.704, 30.1895)
#
#     outputStream = file("output.pdf", "wb")
#     output.write(outputStream)
#     outputStream.close()

# get_errors()


def image(pdf_file_path, jpg_file_path, report_dict):
    input_pdf = PdfFileReader(file(pdf_file_path, "rb"))
    page = input_pdf.getPage(0)
    width = page.mediaBox.getUpperRight_x()
    height = page.mediaBox.getUpperRight_y()
    # print "size pdf: ", width, height

    im = Image.open(jpg_file_path)
    # print "default size jpg: ", im.size[0], im.size[1]

    dy = float(im.size[1]) / float(height)
    dx = float(im.size[0]) / float(width)
    # print "dy = %lf" % dy
    # print "dx = %lf" % dx

    # num = 0
    for report in report_dict:
        for rectangle in report['locations']:
            x1 = int(rectangle['minX'] * dx)
            y1 = int(rectangle['minY'] * dy)
            x2 = int(rectangle['maxX'] * dx)
            y2 = int(rectangle['maxY'] * dy)
            rectangle['top'] = im.size[1] - y2
            rectangle['left'] = x1
            rectangle['width'] = x2 - x1
            rectangle['height'] = (im.size[1] - y1) - rectangle['top']

            # new points: x1, im.size[1] - y2, x2, im.size[1] - y1
            # num += 1
            # box = [x1, im.size[1] - y2, x2, im.size[1] - y1]
            # er = im.crop(box)
            # er.save('/home/ministr/pyTest/convert_to_pdf/er%d.jpg' % num)



report_dict = parse_xml(
    '/home/ministr/pyTest/convert_to_pdf/xmls/',
    'A5_NOK_xml_report.xml') # path_to_dir_with_xml_file, xml_file_name

convert(
    '/home/ministr/pyTest/convert_to_pdf/A5.pdf',
    '/home/ministr/pyTest/convert_to_pdf/A5.jpg') # full_path_to_pdf, full_path_to_jpg_for_creating_her

image('/home/ministr/pyTest/convert_to_pdf/A5.pdf',
      '/home/ministr/pyTest/convert_to_pdf/A5.jpg',
      report_dict) # full_path_to_pdf, full_path_to_new_jpg, list_with_info_from_xml

# print report_dict
# for report in report_dict:
#     print report
