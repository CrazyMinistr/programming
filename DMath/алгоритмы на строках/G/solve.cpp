#include <string>
#include <vector>
#include <iostream>
#include <queue>
#include <cstdio>
#pragma comment(linker, "/STACK:33554432")
 
using namespace std;
 
class automate
{
private:
    struct trie_node
    {
        vector <int> terminals;
        trie_node *parent;
        trie_node *suf;
        trie_node *super_suf;
        trie_node *ch[26];
        int simbol;
 
        trie_node() : terminals(), parent(NULL), suf(NULL), super_suf(NULL), simbol(-1)
        {
            for (int i = 0; i < 26; i++)
                ch[i] = NULL;
        }
 
        trie_node(trie_node *parent, int simbol) : terminals(0), parent(parent), suf(NULL), super_suf(NULL), simbol(simbol)
        {
            for (int i = 0; i < 26; i++)
                ch[i] = NULL;
        }
 
        ~trie_node()
        {
            for (int i = 0; i < 26; i++)
                delete ch[i];
        }
 
        void add_string(string::iterator it, string::iterator end, int number);
        bool terminal();
    };
 
    trie_node* make_automate(vector <string> &p);
 
    trie_node *root;
public:
    automate(vector <string> p)
    {
        root = make_automate(p);
    }
 
    ~automate()
    {
        delete root;
    }
 
    void search(const string &text, vector <int> &ans);
};
 
void automate::trie_node::add_string(string::iterator it, string::iterator end, int number)
{
    if (it == end)
    {
        terminals.push_back(number);
        return;
    }
    if (ch[(*it) - 'a'] == NULL)
        ch[(*it) - 'a'] = new trie_node(this, (*it) - 'a');         
    ch[(*it) - 'a']->add_string(it + 1, end, number);
}
 
bool automate::trie_node::terminal()
{
    return super_suf != this && !terminals.empty();
}
 
automate::trie_node* automate::make_automate(vector <string> &p)
{
    trie_node *root = new trie_node();
    root->parent = root;
    for (int i = 0; i < p.size(); i++)
        root->add_string(p[i].begin(), p[i].end(), i);
    root->suf = root;
    root->super_suf = root;
    queue <trie_node*> q;
    q.push(root);
    trie_node *current_node;
    trie_node *current_suf;
    while (!q.empty())
    {
        current_node = q.front();
        q.pop();
        current_suf = current_node->parent->suf;
        if (current_node != root)
        {
            if (current_node->parent == root)
                current_node->suf = root;
            else
            {
                while (current_suf->ch[current_node->simbol] == NULL && current_suf != current_suf->suf)
                    current_suf = current_suf->suf;
                if (current_suf->ch[current_node->simbol] != NULL)
                    current_suf = current_suf->ch[current_node->simbol];
                current_node->suf = current_suf;
            }
        }
 
        if (current_node->suf->terminal())
            current_node->super_suf = current_node->suf;
        else
            current_node->super_suf = current_node->suf->super_suf;
 
        for (int i = 0; i < 26; i++)
            if (current_node->ch[i] != 0)
                q.push(current_node->ch[i]);
    }
    return root;
}
 
void automate::search(const string &text, vector <int> &ans)
{
    trie_node *current_node = root;
    trie_node *s, *s1;
    for (int i = 0; i < text.size(); i++)
    {
        if (current_node->ch[text[i] - 'a'] != NULL)
            current_node = current_node->ch[text[i] - 'a'];
        else
        {
            while (current_node != current_node->suf && current_node->suf->ch[text[i] - 'a'] == NULL)
                current_node = current_node->suf;
            if (current_node->suf->ch[text[i] - 'a'] != NULL)
                current_node = current_node->suf->ch[text[i] - 'a'];
        }
        s = current_node;
        s1 = s;
        while (s->super_suf->terminal() || s->terminal())
        {
            if (s->terminal())
                for (int j = 0; j < s->terminals.size(); j++)
                    ans[s->terminals[j]] = 1;
            s = s->super_suf;
            s1->super_suf = s1;
            s1 = s;
        }
    }
}
 
int n;
char buffer[1000010];
vector <string> p;
vector <int> ans;
 
int main()
{
    freopen("search4.in", "r", stdin);
    freopen("search4.out", "w", stdout);
    scanf("%d", &n);
    ans.resize(n, 0);
    for (int i = 0; i < n; i++)
    {
        scanf("%s", buffer);
        p.push_back(string(buffer));
    }
    scanf("%s", buffer);
    string text(buffer);
    automate a(p);
    a.search(text, ans);
    for (int i = 0; i < n; i++)
        if (ans[i] == 1)
            printf("YES\n");
        else
            printf("NO\n");
    return 0;
}
