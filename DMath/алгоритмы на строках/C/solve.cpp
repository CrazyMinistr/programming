#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }
template <typename T> T _min(T a, T b) { return a < b ? a : b; }
template <typename T> T _max(T a, T b) { return a > b ? a : b; }

using namespace std;
#define N 2000000

char s[N];
int p[N], x;

int main()
{
//*
	freopen("prefix.in", "r", stdin);
	freopen("prefix.out", "w", stdout);
//*/
	gets(s + 1);
	int n = strlen(s + 1);
	p[1] = x = 0;
	for (int i = 2; i <= n; i++)
	{
		x = p[i - 1];
		while (x && s[x + 1] != s[i])
			x = p[x];
		(s[i] == s[x + 1]) ? p[i] = x + 1 : p[i] = 0;
	}
	for (int i = 1; i < n; i++)
		cout<< p[i]<< " ";
	cout<< p[n]<< endl;
	return 0;
}
