#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }
template <typename T> T _min(T a, T b) { return a < b ? a : b; }
template <typename T> T _max(T a, T b) { return a > b ? a : b; }

using namespace std;
#define N 3000000

char s[N];
int p[N], x;
vector<int> ans;

int main()
{
//*
	freopen("search2.in", "r", stdin);
	freopen("search2.out", "w", stdout);
//*/

	gets(s + 1);
	int n = strlen(s + 1);
	s[n + 1] = '$';
	gets(s + n + 2);
	int m = strlen(s + 1);
	// puts(s + 1);

	p[1] = x = 0;
	for (int i = 2; i <= m; i++)
	{
		x = p[i - 1];
		while (x && s[x + 1] != s[i])
			x = p[x];
		(s[i] == s[x + 1]) ? p[i] = x + 1 : p[i] = 0;
		if (p[i] == n)
			ans.push_back(i - n - n);
	}
	cout<< ans.size()<< endl;
	for (int i = 0; i < (int) ans.size(); i++)
		cout<< ans[i]<< " ";
	return 0;
}
