#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <algorithm>

using namespace std;

#define N 2000123

char s1[N], s2[N], t1[N], t2[N], s[N];
vector<int> ans, f1, f2;
int len;

vector<int> Z(const char *s)
{
	vector<int> z(len + 1, 0);
	for (int i = 1, l = 0, r = 0; i < len; i++)
	{
		if (i <= r)
			z[i] = min(r - i, z[i - l]);
		while (s[z[i] + 1] == s[i + z[i] + 1])
			++z[i];
		if (i + z[i] > r)
			l = i, r = i + z[i];
	}
	z.insert(z.begin(), 0);
	return z;
}

int main()
{
//*
	freopen("search3.in", "r", stdin);
	freopen("search3.out", "w", stdout);
//*/
	gets(s1 + 1);
	gets(t1 + 1);
	int n = strlen(s1 + 1);
	int m = strlen(t1 + 1);
	len = n + m + 1;
	memmove(s2, s1, sizeof(s1));
	memmove(t2, t1, sizeof(t1));
	reverse(s2 + 1, s2 + n + 1);
	reverse(t2 + 1, t2 + m + 1);
	memmove(s + 1, s1 + 1, n * sizeof(char));
	s[n + 1] = '$';
	memmove(s + n + 2, t1 + 1, m * sizeof(char));
	f1 = Z(s);
	memmove(s + 1, s2 + 1, n * sizeof(char));
	memmove(s + n + 2, t2 + 1, m * sizeof(char));
	f2 = Z(s);
	reverse(f2.begin() + n + 1, f2.end());
	for (int i = n + 2; i <= len - n + 1; i++)
		if (f1[i] == n || f1[i] + f2[i + n - 1] >= n - 1)
			ans.push_back(i - n - 1);

	cout<< ans.size()<< endl;
	for (int i = 0; i < (int) ans.size(); i++)
		cout<< ans[i]<< " ";
	puts("");
	return 0;
}
