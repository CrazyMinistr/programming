#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

#define N 222222 *  2
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii

int n, m, timer, mcol, ii = 0;
int ret[N], enter[N], color[N], x[N], y[N], p[N];
bool used[N];

void dfs(int v, int pred = -1)
{
	used[v] = true;
	enter[v] = ret[v] = timer++;
	for (int q = x[v]; q; q = p[q])
	{
		int to = y[q];
		if (to == pred) continue;
		if (used[to])
			ret[v] = min(ret[v], enter[to]);
		else
		{
			dfs(to, v);
			ret[v] = min(ret[v], ret[to]);
		}
	}
}

void paint(int v, int col)
{
	color[v] = col;
	for (int q = x[v]; q; q = p[q])
	{
		int to = y[q];
		if (color[to] == 0)
		{
			if (ret[to] == enter[to])
			{
				mcol++;
				paint(to, mcol);
			}
			else
				paint(to, col);
		}
	}
}

int main()
{
	freopen("bicone.in", "r", stdin);
	freopen("bicone.out", "w", stdout);
	cin >> n >> m;
	int a, b;
	while (m--)
	{
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}
	timer = 0;
	for (int i = 1; i <= n; i++)
		if (!used[i])
			dfs(i);

	mcol = 0;
	for (int i = 1; i <= n; i++)
		if (color[i] == 0)
		{
			mcol++;
			paint(i, mcol);
		}

	cout << mcol << endl;
	for (int i = 1; i <= n; i++)
		printf("%d%c", color[i], i == n ? '\n' : ' ');
	return 0;
}
