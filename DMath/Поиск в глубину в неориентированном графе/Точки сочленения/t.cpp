#include<set>
#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222 * 2

#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii

bool used[N];
set<int> points;
int timer = 0, tin[N], fup[N], x[N], y[N], p[N], n, m, ii = 0;
 
void dfs(int v, int pred = -1)
{
	used[v] = 1;
	tin[v] = fup[v] = timer++;
	int children = 0;
	for (int q = x[v]; q; q = p[q])
	{
		if (y[q] == pred) continue;
		if (used[y[q]])
			fup[v] = min(fup[v], tin[y[q]]);
		else
		{
			dfs(y[q], v);
			fup[v] = min(fup[v], fup[y[q]]);
			if (fup[y[q]] >= tin[v] && pred != -1)
				points.insert(v);
			++children;
		}
	}
	if (pred == -1 && children > 1)
		points.insert(v);
}
 
int main()
{
//*
	freopen("points.in", "r", stdin);
	freopen("points.out", "w", stdout);
//*/
	cin>> n>> m;
	for (int i = 1; i <= m; i++)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}
	for (int i = 1; i <= n; ++i)
		if (!used[i])
			dfs(i);

	cout<< points.size()<< endl;
	for (set<int> :: iterator it = points.begin(); it != points.end(); it++)
		cout<< *it<< " ";
	return 0;
}
