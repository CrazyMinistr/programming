#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222 * 2

#define addedge(a, b, c) y[++ii] = b, p[ii] = x[a], x[a] = ii, w[ii] = c

bool used[N];
int timer = 0, tin[N], fup[N], x[N], y[N], p[N], w[N];
vector<int> bridges;
int n, m, ii = 0;

void dfs(int v, int pred = -1)
{
	used[v] = 1;
	tin[v] = fup[v] = timer++;
	for (int q = x[v]; q; q = p[q])
	{
		if (y[q] == pred) continue;
		if (used[y[q]])
			fup[v] = min(fup[v], tin[y[q]]);
		else
		{
			dfs(y[q], v);
			fup[v] = min(fup[v], fup[y[q]]);
			if (fup[y[q]] > tin[v])
				bridges.push_back(w[q]);
		}
	}
}
 
int main()
{
//*
	freopen("bridges.in", "r", stdin);
	freopen("bridges.out", "w", stdout);
//*/
	cin>> n>> m;
	for (int i = 1; i <= m; i++)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b, i);
		addedge(b, a, i);
	}
	for (int i = 1; i <= n; ++i)
		if (!used[i])
			dfs(i);

	cout<< bridges.size()<< endl;
	sort(bridges.begin(), bridges.end());
	for (size_t i = 0; i < bridges.size(); i++)
		printf("%d%c", bridges[i], i == bridges.size() - 1 ? '\n' : ' ');
	return 0;
}
