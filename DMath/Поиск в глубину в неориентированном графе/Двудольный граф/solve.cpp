#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222 * 2

#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii

int part[N], x[N], y[N], p[N];
int n, m, ii = 0;

int main()
{
/*
	freopen("bipartite.in", "r", stdin);
	freopen("bipartite.out", "w", stdout);
//*/
	cin>> n>> m;
	for (int i = 1; i <= m; i++)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}

	for (int i = 1; i <= n; i++)
		part[i] = -1;

	bool ok = true;
	vector<int> e(n);
	for (int st = 1; st <= n; ++st)
		if (part[st] == -1)
		{
			int h = 0, t = 0;
			e[t++] = st;
			part[st] = 0;
			while (h < t)
			{
				int v = e[h++];
				for (int q = x[v]; q; q = p[q])
				{
					int to = y[q];
					if (part[to] == -1)
						part[to] = !part[v], e[t++] = to;
					else
						ok &= part[to] != part[v];
				}
			}
		}
	puts(ok ? "YES" : "NO");
	return 0;
}
