#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;
#define N 222222 * 2
#define addedge(a, b, c) y[++ii] = b, p[ii] = x[a], x[a] = ii, w[ii] = c

int n, m, timer, next_color, ii = 0;
int p[N], x[N], y[N], enter[N], color[N], ret[N], w[N];
bool used[N];

void dfs(int v, int pred)
{
	used[v] = true;
	enter[v] = ret[v] = timer++;
	for (int q = x[v]; q; q = p[q])
	{
		int to = y[q];
		if (to == pred) continue;
		if (used[to])
			ret[v] = min(ret[v], enter[to]);
		else
		{
			dfs(to, v);
			ret[v] = min(ret[v], ret[to]);
		}
	}
}

void paint(int v, int c, int pred)
{
	used[v] = true;
	for (int q = x[v]; q; q = p[q])
	{
		int u = y[q];
		if (u == pred) continue;
		if (!used[u])
		{
			if (ret[u] >= enter[v])
			{
				next_color++;
				color[w[q]] = next_color;
				paint(u, next_color, v);
			}
			else
			{
				color[w[q]] = c;
				paint(u, c, v);
			}
		}
		else
			if (enter[u] <= enter[v])
				color[w[q]] = c;
	}
}

int main()
{
	freopen("biconv.in", "r", stdin);
	freopen("biconv.out", "w", stdout);
	cin >> n >> m;
	int a, b;
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d", &a, &b);
		addedge(a, b, i);
		addedge(b, a, i);
	}
	timer = 0;
	for (int i = 1; i <= n; i++)
		if (!used[i])
			dfs(i, -1);

	memset(used, 0, sizeof(used));
	next_color = 0;
	for (int i = 0; i < n; i++)
		if (!used[i])
			paint(i, -1, -1);

	cout << next_color << endl;
	for (int i = 1; i <= m; i++)
		printf("%d%c", color[i], i == m ? '\n' : ' ');
	return 0;
}
