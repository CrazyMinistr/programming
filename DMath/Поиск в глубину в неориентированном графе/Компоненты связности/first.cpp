#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222 * 2

#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii

int ii = 0, n, m, a, b, cl = 0;
int y[N], x[N], p[N], color[N];

void dfs(int v)
{
	color[v] = cl;
	for (int q = x[v]; q; q = p[q])
		if (!color[y[q]])
			dfs(y[q]);
}

int main()
{
//*
	freopen("components.in", "r", stdin);
	freopen("components.out", "w", stdout);
//*/
	scanf("%d%d", &n, &m);
	while (m--)
	{
		int a, b;
		scanf("%d%d", &a, &b);
		addedge(a, b);
		addedge(b, a);
	}
	for (int i = 1; i <= n; i++)
		if (!color[i])
		{
			++cl;
			dfs(i);
		}
	cout<< cl<< endl;
	for (int i = 1; i <= n; i++)
		printf("%d%c", color[i], i == n ? '\n' : ' ');
	return 0;
}
