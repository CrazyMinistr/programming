#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 111111

int x, n;
string s;
vector<int> st;
struct myHeap
{
	int n;
	int h[N], ph[N], ps[N];
	myHeap()
	{
		memset(h, 0, sizeof(h));
		memset(ps, 0, sizeof(ps));
		memset(ph, 0, sizeof(ph));
		n = 0;
	}

	void up(int t)
	{
		while (t > 1 && h[t] < h[t >> 1])
		{
			swap(h[t], h[t >> 1]);
			swap(ph[t], ph[t >> 1]);
			ps[ph[t]] = t;
			t >>= 1;
			ps[ph[t]] = t;
		}
	}

	void insert(int x, int i)
	{
		h[++n] = x;
		ps[i] = n;
		ph[n] = i;
		up(n);
	}

	int pop()
	{
		int ans = h[1];

		ps[ph[1]] = 0;
		ph[1] = ph[n];
		ps[ph[1]] = 1;
		h[1] = h[n--];

		int t = 1, w;
		while (1)
		{
			w = t << 1;
			if (w > n) break;
			w += (w < n && h[w + 1] < h[w]);
			if (h[t] > h[w])
			{
				swap(h[t], h[w]);
				swap(ph[t], ph[w]);
				ps[ph[t]] = t;
				t = w;
				ps[ph[t]] = t;
			}else break;
		}
		return ans;
	}

	void decrease_key(int i, int newKey)
	{
		h[ps[i]] = newKey;
		up(ps[i]);
	}
} heap;

int main()
{
//*
	freopen("priorityqueue.in", "r", stdin);
	freopen("priorityqueue.out", "w", stdout);
//*/
	while (cin>> s)
	{
		if (s[0] == 'p')
		{
			scanf("%d", &x);
			st.push_back(x);
			heap.insert(x, st.size());
        }
        else if (s[0] == 'e')
        {
        	st.push_back(0);
        	if (heap.n)
        		printf("%d\n", heap.pop());
        	else
        		puts("*");
        }
        else
        {
        	st.push_back(0);
        	scanf("%d%d", &n, &x);
        	heap.decrease_key(n, x);
        }
	}
	return 0;
}
