{$R-,S-,Q-,I-,O+}
var
 kh,n,k,i,x,y:longint;
 ans:int64;
 p,h,pz,ph,left,right,s,a:array[0..100010]of int64;

function findset(x:longint):longint;
begin
    if x<>p[x] then p[x]:=findset(p[x]);
    findset:=p[x];
end;

procedure up(t:longint);
var
 tmp:int64;
begin
    while (t>1)and(h[t]<h[t shr 1])do
    begin
        tmp:=h[t]; h[t]:=h[t shr 1]; h[t shr 1]:=tmp;
        tmp:=ph[t]; ph[t]:=ph[t shr 1]; ph[t shr 1]:=tmp;
        pz[ph[t]]:=t;
        t:=t shr 1;
        pz[ph[t]]:=t;
    end;
end;

procedure down(t:longint);
var
 w:longint;
 tmp:int64;
begin
    while true do
    begin
        w:=t shl 1;
        if w>kh then break;
        if w<kh then if h[w+1]<h[w] then inc(w);
        if h[t]>h[w] then
        begin
            tmp:=h[t]; h[t]:=h[w]; h[w]:=tmp;
            tmp:=ph[t]; ph[t]:=ph[w]; ph[w]:=tmp;
            pz[ph[t]]:=t;
            t:=w;
            pz[ph[t]]:=t;
        end else break;
    end;
end;

procedure del(t:longint);
begin
    h[t]:=-1000000000001;
    up(t);
    pz[ph[1]]:=0;
    h[1]:=h[kh];
    ph[1]:=ph[kh];
    pz[ph[1]]:=1;
    dec(kh);
    down(1);
end;

begin
    assign(input,'backup.in');reset(input);
    assign(output,'backup.out');rewrite(output);
    read(n,k);
    inc(n);
    for i:=2 to n do read(a[i]);
    for i:=2 to n-1 do a[i]:=a[i+1]-a[i];
    a[1]:=100000000001; a[n]:=100000000001;
    s[0]:=0;
    for i:=1 to n do s[i]:=a[i]-s[i-1];
    kh:=0;
    for i:=1 to n do
    begin
        p[i]:=i;
        left[i]:=i;
        right[i]:=i;
        inc(kh);
        h[kh]:=a[i];
        ph[kh]:=i;
        pz[i]:=kh;
        up(kh);
    end;
    ans:=0;
    while k>0 do
    begin
        ans:=ans+h[1];
        i:=ph[1];
        x:=findset(left[i]-1);
        y:=findset(right[i]+1);
        del(pz[x]);
        del(pz[y]);
        p[x]:=i;
        p[y]:=i;
        left[i]:=left[x];
        right[i]:=right[y];
        h[pz[i]]:=s[right[i]]+s[left[i]-1];
        down(pz[i]);
        dec(k);
    end;
    writeln(ans);
end.
