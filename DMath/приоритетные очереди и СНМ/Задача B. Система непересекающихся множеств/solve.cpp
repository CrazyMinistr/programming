#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, x, y;
string s;
struct T{
	int max, min, kol, x;
	T(){ kol = 1; }
} p[100011];

int root(int x)
{
	if (x != p[x].x) p[x].x = root(p[x].x);
	return p[x].x;
}

int main()
{
//*
	freopen("dsu.in", "r", stdin);
	freopen("dsu.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++) p[i].x = p[i].max = p[i].min = i;
	while (cin>> s)
	{
		if (s[0] == 'u')
		{
			scanf("%d%d", &x, &y);
			if ((x = root(x)) != (y = root(y)))
			{
				p[y].x = x;
				p[x].max = _max(p[x].max, p[y].max);
				p[x].min = _min(p[x].min, p[y].min);
				p[x].kol += p[y].kol;
			}
		}
		else
		{
			scanf("%d", &x);
			x = root(x);
			printf("%d %d %d\n", p[x].min, p[x].max, p[x].kol);
		}
	}
	return 0;
}
