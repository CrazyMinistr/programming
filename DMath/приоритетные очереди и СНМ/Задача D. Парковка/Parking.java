import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.StringTokenizer;

public class Parking {
	static int n, m;
	static int k = 0;
	static String str;
	static String[] stray = new String[2];
	static BufferedReader br;
	static PrintWriter pw;
	static StringTokenizer st;

	String nextToken() throws IOException {
		while (st == null || !st.hasMoreTokens()) {
			st = new StringTokenizer(br.readLine());
		}
		return st.nextToken();
	}

	int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	char nextChar() throws IOException {
		return (char) br.read();
	}

	public static void main(String[] args) throws IOException {
		br = new BufferedReader(new FileReader("parking.in"));
		pw = new PrintWriter("parking.out");
		disSet dis = new disSet();
		str = br.readLine();
		st = new StringTokenizer(str, " \t\n\r,.");
		n = Integer.parseInt(st.nextToken());
		m = Integer.parseInt(st.nextToken());
		dis.init(n);

		for (int i = 0; i < m; i++) {
			str = br.readLine();
			st = new StringTokenizer(str, " \t\n\r,.");
			while (st.hasMoreTokens()) {
				stray[k] = st.nextToken();
				k++;
			}
			k = 0;
			if (stray[0].charAt(1) == 'n') {
				pw.println(dis.union(Integer.parseInt(stray[1]) - 1));
			} else {
				dis.del(Integer.parseInt(stray[1]) - 1);
			}
		}

		br.close();
		pw.close();
	}
}

class disSet {
	int[] elements = new int[100001];

	public void init(int n) {
		for (int i = 0; i < n; i++) {
			elements[i] = i;
		}
	}

	public void del(int x) {
		elements[x] = x;
		int tmp = x;
		x = (x - 1 + Parking.n) % Parking.n;
		while (elements[x] != x) {
			elements[x] = tmp;
			x = (x - 1 + Parking.n) % Parking.n;
		}
	}

	public int get(int x) {
		if (elements[x] != x) {
			elements[x] = get(elements[x]);
		}
		return elements[x];
	}

	public int union(int x) {
		x = get(x);
		elements[x] = (x + 1) % Parking.n;
		return x + 1;
	}
}
