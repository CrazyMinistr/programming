#include<ctime>
#include<cstdio>
#include<string>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
#define N 100111

int n, m, x, y, p[N];
char s[5];
bool f[N];

int root(int x)
{
	if (x != p[x]) p[x] = root(p[x]);
	return p[x];
}

int main()
{
	freopen("parking.in", "r", stdin);
	freopen("parking.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++) p[i] = i;
	for (int i = 0; i < m; i++)
	{
		scanf("%s%d", s, &x);
		if (s[1] == 'n')
		{
			x = root(x);
			f[x] = 1;
			printf("%d\n", x);
			y = x + 1;
			if (y > n) y = 1;
			y = root(y);
			p[x] = y;
		}
		else
		{
			f[x] = 0;
			p[x] = x;
			int z = x - 1;
			if (z == 0) z = n;
			while (f[z])
			{
				p[z--] = x;
				if (z == 0) z = n;
			}
		}
	}
	return 0;
}
