#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, a[22];
LL f[22];
bool used[22];
int main()
{
//*
	freopen("perm2num.in", "r", stdin);
	freopen("perm2num.out", "w", stdout);
//*/
	cin>> n;
	f[0] = 1;
	for (int i = 1; i <= n; i++) f[i] = 1ll * f[i - 1] * i;
	for (int i = 0; i < n; i++) cin>> a[i];
	LL ans = 0;
	for (int i = 0; i < n; i++)
	{
		int c = 0;
		for (int j = 0; j < a[i] - 1; j++) if (!used[j]) ++c;
		used[a[i] - 1] = 1;
		ans += 1ll * c * f[n - i - 1];
	}
	cout<< ans<< endl;
	return 0;
}
