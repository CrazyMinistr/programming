#include<set>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, a[111111], b[111111];
set<pair<int, int> > s;
set<pair<int, int> > :: iterator it;
void next_perm()
{
	s.clear();
	bool f = 1;
	int Max = INT_MIN;
	for (int i = n; i; i--)
	{
		s.insert(make_pair(a[i], i));
		it = s.find(make_pair(a[i], i));
		if (a[i] > Max)
		{
			Max = a[i];
			continue;
		}
		++it;
		f = 0;
		swap(a[i], a[(*it).second]);
		sort(a + i + 1, a + n + 1);
		break;
	}
	for (int i = 1; i < n; i++) printf("%d ", f ? 0 : a[i]);
	printf("%d\n", f ? 0 : a[n]);
}

void prev_perm()
{
	s.clear();
	bool f = 1;
	int Min = INT_MAX;
	for (int i = n; i; i--)
	{
		s.insert(make_pair(a[i], i));
		it = s.find(make_pair(a[i], i));
		if (a[i] < Min)
		{
			Min = a[i];
			continue;
		}
		--it;
		f = 0;
		swap(a[i], a[(*it).second]);
		sort(a + i + 1, a + n + 1, greater<int>());
		break;
	}
	for (int i = 1; i < n; i++) printf("%d ", f ? 0 : a[i]);
	printf("%d\n", f ? 0 : a[n]);
}

int main()
{
//*
	freopen("nextperm.in", "r", stdin);
	freopen("nextperm.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
	memmove(b, a, sizeof(b));
	prev_perm();
	memmove(a, b, sizeof(a));
	next_perm();
	return 0;
}
