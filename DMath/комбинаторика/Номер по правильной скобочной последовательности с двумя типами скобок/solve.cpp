#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#define LL long long
using namespace std;

int n, m, stn = 0;
LL d[55][55];
char st[55], s[55];

int main() {
	freopen("brackets2num2.in", "r", stdin);
	freopen("brackets2num2.out", "w", stdout);
	gets(s);
	n = strlen(s) / 2, m = n << 1;
	d[0][0] = 1;
	for (int i = 0; i < 45; i++)
		for (int j = 0; j < 45; j++)
		{
			d[i + 1][j + 1] += d[i][j];
			if (j > 0) d[i + 1][j - 1] += d[i][j];
		}
	
	LL ans = 0;
	for (int i = 0; i < 2 * n; i++) {
		if (s[i] == '(')
		{
			st[stn++] = '(';
			continue;
		}
		else ans += d[m - i - 1][stn + 1] * (1LL << ((2LL * n - stn - i - 1) / 2));

		if (s[i] == ')')
		{
			--stn;
			continue;
		}
		else if (stn && st[stn - 1] == '(')
				ans += d[m - i - 1][stn - 1] * (1LL << ((2LL * n - stn - i + 1) / 2));
	
		if (s[i] == '[')
		{
			st[stn++] = '[';
			continue;
		}
		else ans += d[m - i - 1][stn + 1] * (1LL << ((2LL * n - stn - i - 1) / 2));

		if (s[i] == ']')
		{
			--stn;
			continue;
		}
		else if (stn && st[stn - 1] == '[')
				ans += d[m - i - 1][stn - 1] * (1LL << ((2LL * n - stn - i + 1) / 2));
	} 
	cout<< ans<< endl;
	return 0;
}
