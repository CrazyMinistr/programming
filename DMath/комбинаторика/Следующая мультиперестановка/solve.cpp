#include<set>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, a[111111], b[111111];
multiset<pair<int, int> > s;
multiset<pair<int, int> > :: iterator it;
void next_perm()
{
	s.clear();
	bool f = 1;
	int Max = INT_MIN;
	for (int i = n; i; i--)
	{
		s.insert(make_pair(a[i], i));
		it = s.upper_bound(make_pair(a[i], n));
		if (a[i] >= Max)
		{
			Max = a[i];
			continue;
		}
		f = 0;
		swap(a[i], a[(*it).second]);
		sort(a + i + 1, a + n + 1);
		break;
	}
	for (int i = 1; i < n; i++) printf("%d ", f ? 0 : a[i]);
	printf("%d\n", f ? 0 : a[n]);
}

int main()
{
//*
	freopen("nextmultiperm.in", "r", stdin);
	freopen("nextmultiperm.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
	next_perm();
	return 0;
}
