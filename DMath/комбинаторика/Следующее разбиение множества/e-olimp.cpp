#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define pb push_back

int n, k, keep, l, x, newk;
char s[1000];
vector<int> a[222];
vector<int> fr, nxt;

vector<int> next(vector<int> s)
{
	vector<int> ans;
	ans.clear();
	int sMax = (int) s[(int) s.size() - 1];
	if (fr.size())
	{
		int frMax = fr[(int) fr.size() - 1];
		if (frMax > sMax)
		{
			int i = fr.size() - 1;
			while (i > 0 && fr[i - 1] > sMax) --i;
			ans = s;
			ans.pb(fr[i]);
			fr.erase(fr.begin() + i);
			return ans;
		}
	}
	if (s.size() < 3) return ans;
	for (int i = 0; i < (int) fr.size(); i++)
		if (fr[i] > s[(int) s.size() - 2])
		{
			for (int j = 0; j < (int) s.size() - 2; j++) ans.pb(s[j]);
			ans.pb(fr[i]);
			fr.erase(fr.begin() + i);
			fr.pb(s[(int) s.size() - 2]);
			fr.pb(s[(int) s.size() - 1]);
			return ans;
		}
	ans = s;
    ans.erase(ans.begin() + ans.size() - 2);
    fr.pb(s[(int) s.size() - 2]);
	return ans;
}

int main()
{
	scanf("%d%d\n", &n, &k);
	while (1)
	{
		if (!n) break;

		fr.clear();
		for (int i = 0; i < k; i++) a[i].clear();

		for (int i = 0; i < k; i++)
		{
			gets(s);
			l = strlen(s), x = 0;
			for (int j = 0; j < l; j++)
			{
				if (s[j] == ' ')
				{
					a[i].pb(x);
					x = 0;
					continue;
				}
				x = x * 10 + s[j] - 48;
			}
			a[i].pb(x);
		}
		keep = 0;
		for (int i = k - 1; i >= 0; i--)
		{
			nxt = next(a[i]);
			if (nxt.size())
			{
				a[i] = nxt;
				keep = i + 1;
				break;
			}
			else
			{
				for (int j = 0; j < (int) a[i].size(); j++) fr.pb(a[i][j]);
				sort(fr.begin(), fr.end());
			}
		}
		newk = keep + fr.size();
		cout<< n<< " "<< newk<< endl;
		for (int i = 0; i < keep; i++)
		{
			for (int j = 0; j < (int) a[i].size(); j++)
			{
				if (j) printf(" ");
				printf("%d", a[i][j]);
			}
			puts("");
		}
		sort(fr.begin(), fr.end());
		for (int i = 0; i < (int) fr.size(); i++) printf("%d\n", fr[i]);
		scanf("%d%d\n", &n, &k);
		if (!n) break;
		puts("");
	}
	return 0;
}
