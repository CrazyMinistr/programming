#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, x = 1, ans[111], m = 0;
LL f[111][111], num, k;

LL rec(int x, int s)
{
	if (f[s][x] >= 0) return f[s][x];
	if (s == 0 || s == x)
	{
		f[s][x] = 1;
		return 1;
	}
	f[s][x] = 0;
	for (int i = x; i <= s; i++) f[s][x] += rec(i, s - i);
	return f[s][x];
}

int main()
{
//*
	freopen("num2part.in", "r", stdin);
	freopen("num2part.out", "w", stdout);
//*/
	cin>> n>> num;
	for (int i = 0; i < 111; i++)
		for (int j = 0; j < 111; j++) f[i][j] = -1;

	while (n)
	{
		for (int i = n; i >= x; i--)
		{
			k = rec(x, n) - rec(i, n);
			if (num >= k)
			{
				ans[++m] = x = i;
				n -= i, num -= k;
				break;
			}
		}
	}
	for (int i = 1; i < m; i++) printf("%d+", ans[i]);
	cout<< ans[m]<< endl;
	return 0;
}
