#include<cstdio>
#include<iostream>
using namespace std;

int n, k, a[13];
void rec(int x, int l)
{
	if (x == n)
	{
		if (l == 0)
		{
			for (int i = 0; i < k; i++, puts(""))
				for (int j = 0; j < n; j++)
					if (a[j] == i)
						printf("%d ", j + 1);
			puts("");
		}
		return;
	}

	for (int i = 0; i < k - l; i++)
	{
		a[x] = i;
		rec(x + 1, l);
	}
	a[x] = k - l;
	rec(x + 1, l - 1);
}

int main()
{
	freopen("part2sets.in", "r", stdin);
	freopen("part2sets.out", "w", stdout);
	cin >> n >> k;
	rec(0, k);
	return 0;
}
