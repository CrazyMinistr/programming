import java.util.*;
import java.io.*;

public class E {
	FastScanner in;
	PrintWriter out;

	int n, m;
	final int N = 111111;
	final long INF = (long) 1e18;

	int[] x = new int[N];
	int[] y = new int[N];
	int[] p = new int[N];
	int[] zip = new int[N];
	int[] com = new int[N];
	boolean[] used = new boolean[N];

	long[] d = new long[N];
	long[] z = new long[N];

	long chinaMST(int u) {
		for (int i = 1; i <= n; i++)
			used[i] = false;

		long cur = 0, ans = 0;
		while (true) {
			for (int i = 1; i <= n; i++) {
				d[i] = INF;
				p[i] = com[i] = zip[i] = -1;
			}
			for (int i = 0; i < m; i++)
				if (x[i] != y[i] && y[i] != u && z[i] < d[y[i]]) {
					d[y[i]] = z[i];
					p[y[i]] = x[i];
				}
			cur = 0;
			boolean good = false;
			for (int i = 1; i <= n; i++) {
				if (used[i]) continue;
				if (p[i] == -1 && i != u) return INF;
				if (p[i] >= 0) cur += d[i];

				int s;
				for (s = i; s != -1 && com[s] == -1; s = p[s])
					com[s] = i;

				if (s != -1 && com[s] == i) {
					good = true;
					int j = s;
					do {
						zip[j] = s;
						used[j] = true;
						ans += d[j];
						j = p[j];
					} while (j != s);
					used[s] = false;
				}
			}
			if (!good)
				break;

			for (int i = 0; i < m; i++) {
				if (zip[y[i]] >= 0) z[i] -= d[y[i]];
				if (zip[x[i]] >= 0) x[i] = zip[x[i]];
				if (zip[y[i]] >= 0) y[i] = zip[y[i]];
				if (x[i] == y[i]) {
					--m;
					x[i] = x[m];
					y[i] = y[m];
					z[i] = z[m];
					--i;
				}
			}
		}
		return ans + cur;
	}

	public void solve() throws IOException {
		n = in.nextInt();
		m = in.nextInt();
		for (int i = 0; i < m; i++) {
			x[i] = in.nextInt();
			y[i] = in.nextInt();
			z[i] = in.nextLong();
		}
		long ans = chinaMST(1);
		if (ans == INF)
			out.println("NO");
		else
			out.println("YES\n" + ans);
	}

	public void run() {
		try {
			in = new FastScanner(new File("chinese.in"));
			out = new PrintWriter(new File("chinese.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}
	}

	public static void main(String[] arg) {
		new E().run();
	}
}
