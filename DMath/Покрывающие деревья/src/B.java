import java.util.*;
import java.io.*;

public class B {
	FastScanner in;
	PrintWriter out;
	final int N = 111111;
	int[] p = new int[N];

	class T implements Comparable<T> {
		int x, y, z;

		T(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		@Override
		public int compareTo(T other) {
			if (z < other.z)
				return -1;
			if (z > other.z)
				return 1;
			return 0;
		}
	}

	int get(int x) {
		if (x != p[x])
			p[x] = get(p[x]);
		return p[x];
	}

	public void solve() throws IOException {
		int n = in.nextInt(), m = in.nextInt();

		T[] points = new T[N];
		for (int i = 0; i < m; i++)
			points[i] = new T(in.nextInt(), in.nextInt(), in.nextInt());
		Arrays.sort(points, 0, m);
		for (int i = 1; i <= n; i++)
			p[i] = i;
		int kol = n, ans = 0;
		for (int i = 0; i < m; i++) {
			int x = get(points[i].x), y = get(points[i].y);
			if (x == y)
				continue;
			p[x] = y;
			ans += points[i].z;
			kol--;
			if (kol == 1)
				break;
		}
		out.println(ans);
	}

	public void run() {
		try {
			in = new FastScanner(new File("spantree2.in"));
			out = new PrintWriter(new File("spantree2.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new B().run();
	}
}
