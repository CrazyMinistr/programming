import java.util.*;
import java.io.*;

public class D {
	FastScanner in;
	PrintWriter out;
	final int N = 1007;

	int[] deg = new int[N];
	int[][] g = new int[N][N];
	int[][] s = new int[N][N];
	int[][] used = new int[N][N];
	ArrayList<Integer> nec = new ArrayList<Integer>();
	ArrayList<Integer> ans = new ArrayList<Integer>();

	void find(int v)
	{
		for (int i = 1; i <= g[v][0]; i++)
		{
			int to = g[v][i];
			if (used[v][to] < s[v][to])
			{
				used[v][to]++;
				used[to][v]++;
				find(to);
			}
		}
		ans.add(v);
	}
	
	public void solve() throws IOException {
		int n = in.nextInt();
		for (int i = 1; i <= n; i++) {
			int m = in.nextInt();
			if (m % 2 == 1)
				nec.add(i);
			g[i][0] = m;
			for (int j = 1; j <= m; j++)
			{
				g[i][j] = in.nextInt();
				s[i][g[i][j]]++;
			}
		}
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= g[i][0]; j++)
			{
				deg[g[i][j]]++;
				deg[i]++;
			}
		int k = 0;
		for (int i = 1; i <= n; i++)
			if ((deg[i] & 1) > 0)
				++k;
		if (k != 0 && k != 2)
		{
			out.println("-1");
			return;
		}

		if (nec.size() != 0)
			find(nec.get(0));
		else
			find(1);
		out.println(ans.size() - 1);
		for (int i = 0; i < ans.size(); i++)
			out.print(ans.get(i) + " ");
	}

	public void run() {
		try {
			in = new FastScanner(new File("euler.in"));
			out = new PrintWriter(new File("euler.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new D().run();
	}
}
