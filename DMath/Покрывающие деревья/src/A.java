import java.util.*;
import java.io.*;

public class A {
	FastScanner in;
	PrintWriter out;
	final int N = 5005;

	boolean[] used = new boolean[N];
	long[] minE = new long[N];
	long INF = (long) 1e15;
	
	long dist(Pair a, Pair b)
	{
		long res = ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
		return res;
	}
	
	class Pair {
		public int x, y;
		public Pair(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	public void solve() throws IOException {
		int n = in.nextInt();
		Pair[] points = new Pair[N];
		for (int i = 0; i < n; i++)
		{
			points[i] = new Pair(in.nextInt(), in.nextInt());
			minE[i + 1] = INF; 
		}
		for (int i = 0; i < n; i++)
		{
			int v = -1;
			for (int j = 0; j < n; j++)
				if (!used[j] && (v == -1 || minE[j] < minE[v]))
					v = j;
			used[v] = true;
			for (int j = 0; j < n; j++)
			{
				long d = dist(points[v], points[j]);
				if (d < minE[j] && !used[j])
					minE[j] = d;
			}
		}
		double sum = 0;
		for (int i = 0; i < n; i++)
			sum += Math.sqrt((double) minE[i]);
		out.println(sum);
	}

	public void run() {
		try {
			in = new FastScanner(new File("spantree.in"));
			out = new PrintWriter(new File("spantree.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new A().run();
	}
}
