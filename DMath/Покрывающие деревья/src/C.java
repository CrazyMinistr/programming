import java.util.*;
import java.io.*;

public class C {
	FastScanner in;
	PrintWriter out;
	final int N = 11111;
	final int inf = (int) 2e9;
	int[] p = new int[N];

	class T implements Comparable<T> {
		int x, y, z;

		T(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		@Override
		public int compareTo(T other) {
			if (z < other.z)
				return -1;
			if (z > other.z)
				return 1;
			return 0;
		}
	}

	int get(int x) {
		return (x != p[x]) ? p[x] = get(p[x]) : p[x];
	}

	public void solve() throws IOException {
		int n = in.nextInt(), m = in.nextInt();
		if (n == 1)
		{
			out.println("YES\n0");
			return;
		}
		T[] points = new T[N];
		for (int i = 0; i < m; i++)
			points[i] = new T(in.nextInt(), in.nextInt(), in.nextInt());
		Arrays.sort(points, 0, m);
		int min = inf;
		for (int it = 0; it < m; it++)
		{
			for (int i = 1; i <= n; i++)
				p[i] = i;

			int kol = n, sumMin = inf, sumMax = -inf;
			for (int i = it; i < m; i++) {
				int x = get(points[i].x), y = get(points[i].y);
				if (x == y)
					continue;
				p[x] = y;
				sumMax = Math.max(sumMax, points[i].z);
				sumMin = Math.min(sumMin, points[i].z);
				kol--;
				if (kol == 1)
					break;
			}
			if (kol != 1)
				continue;
			min = Math.min(min, sumMax - sumMin);
		}
		if (min == inf)
			out.println("NO");
		else
			out.println("YES\n" + min);
	}

	public void run() {
		try {
			in = new FastScanner(new File("mindiff.in"));
			out = new PrintWriter(new File("mindiff.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new C().run();
	}
}
