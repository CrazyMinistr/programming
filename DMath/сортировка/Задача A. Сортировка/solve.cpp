#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 111111

int n, a[N], b[N];

void mergeSort(int l, int r)
{
	if (l >= r) return;
	int m = (l + r) >> 1;
	mergeSort(l, m);
	mergeSort(m + 1, r);
	int it1 = l, it2 = m + 1, n = l;
	while (n <= r)
	{
		if (it1 <= m && (it2 > r || a[it1] <= a[it2]))
			b[n++] = a[it1++];
		else
			b[n++] = a[it2++];
	}
	for (int i = l; i <= r; i++)
		a[i] = b[i];
}

int main()
{
//*
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	mergeSort(1, n);
	for (int i = 1; i < n; i++)
		printf("%d ", a[i]);
	cout<< a[n]<< endl;
	return 0;
}
