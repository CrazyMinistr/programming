#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 1111

int tail[N];
int n, m, k;
string s[N], t[N];
void radix_sort(string *s)
{
	for (; k; --k, --m)
	{
		memset(tail, 0, sizeof(tail));
		for (int i = 0; i < n; i++)
			++tail[s[i][m - 1] - 'a'];

		for (int i = 1; i < 26; i++)
			tail[i] += tail[i - 1];

		for (int i = n - 1; i >= 0; i--)
			t[--tail[s[i][m - 1] - 'a']] = s[i];

		for (int i = 0; i < n; i++)
			s[i] = t[i];
	}
}

int main()
{
//*
	freopen("radixsort.in", "r", stdin);
	freopen("radixsort.out", "w", stdout);
//*/
	cin>> n>> m>> k;
	for (int i = 0; i < n; i++)
		cin>> s[i];
	radix_sort(s);
	for (int i = 0; i < n; i++)
		cout<< s[i]<< endl;
	return 0;
}
