#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n, m, k, r[155], a[20], x[155][20], y[155][20];
int main()
{
//*
	freopen("netcheck.in", "r", stdin);
	freopen("netcheck.out", "w", stdout);
//*/
	cin>> n>> m>> k;
	for (int i = 0; i < k; i++)
	{
		cin>> r[i];
		for (int j = 0; j < r[i]; j++)
		{
			cin>> x[i][j]>> y[i][j];
			if (x[i][j] > y[i][j])
				swap(x[i][j], y[i][j]);
		}
	}
	for (int mask = 0; mask < 1 << n; mask++)
	{
		for (int i = 0; i < n; i++)
				a[i + 1] = (mask & (1 << i)) > 0;

		for (int i = 0; i < k; i++)
			for (int j = 0; j < r[i]; j++)
				if (a[x[i][j]] > a[y[i][j]])
					swap(a[x[i][j]], a[y[i][j]]);

		for (int i = 2; i <= n; i++)
			if (a[i] < a[i - 1])
			{
				puts("No");
				return 0;
			}
	}
	puts("Yes");
	return 0;
}
