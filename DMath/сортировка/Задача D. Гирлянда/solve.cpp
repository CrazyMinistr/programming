#include<cmath>
#include<cstdio>
#include<iostream>
using namespace std;
#define eps 1e-7

int n;
double h[1111];

int main()
{
	freopen("garland.in", "r", stdin);
	freopen("garland.out", "w", stdout);
	cin>> n>> h[0];
    double ans = 1e9;
    double l = 0, r = h[0];
    for (int it = 0; it < 100; it++)
    {
        h[1] = (l + r) / 2;
        h[n - 1] = 0;
        bool fail = 0;
        for (int i = 2; i < n; i++)
        {
            h[i] = 2 * h[i - 1] - h[i - 2] + 2;
            if (h[i] < 0 || fabs(h[i]) < eps)
            {
                fail = 1;
                break;
            }
        }
        if (h[n - 1] > 0)
            ans = min(ans, h[n - 1]);

        if (fail) l = h[1];else r = h[1];
    }
    printf("%0.2f", ans);
	return 0;
}
