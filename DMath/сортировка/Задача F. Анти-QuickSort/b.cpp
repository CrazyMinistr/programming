#include<cstdio>
#include<algorithm>

int a[77777], n;
int main()
{
	freopen("antiqs.in", "r", stdin);
	freopen("antiqs.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) a[i] = i;
	for (int i = 2; i <= n; i++) std::swap(a[i], a[(i + 1) / 2]);
	for (int i = 1; i <= n; i++) printf("%d ", a[i]);
	puts("");
	return 0;
}
