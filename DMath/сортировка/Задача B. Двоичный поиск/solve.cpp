#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int a[111111];
int n, m, q, l, r, x;
int main()
{
//*
	freopen("binsearch.in", "r", stdin);
	freopen("binsearch.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	cin>> q;
	while (q--)
	{
		scanf("%d", &x);
		l = 0, r = n + 1;
		while (r - l > 1)
		{
			m = (l + r) >> 1;
			if (a[m] < x)
				l = m;
			else
				r = m;
		}
		printf("%d ", a[r] != x ? -1 : r);
		l = 0, r = n + 1;
		while (r - l > 1)
		{
			m = (l + r) >> 1;
			if (a[m] <= x)
				l = m;
			else
				r = m;
		}
		printf("%d\n", a[l] != x ? -1 : l);
	}
	return 0;
}
