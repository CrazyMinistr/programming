#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;

int a[30000003], n, k, A, B, C;

int qs(int l, int r, int k)
{
	if (l == r) return a[l];
	int x = a[l + (rand() % (r - l + 1))];
	int i = l, j = r;
	do
	{
		while (a[i] < x) ++i;
		while (a[j] > x) --j;
		if (i <= j)
		{
			swap(a[i], a[j]);
			++i, --j;
		}
	} while (i <= j);
	if (k <= j - l + 1)
		qs(l, j, k);
	if (k > i - l)
		qs(i, r, k - (i - l));
	return a[l + k - 1];
}

int main()
{
	freopen("kth.in", "r", stdin);
	freopen("kth.out", "w", stdout);
	srand((unsigned) time(0));
	scanf("%d%d%d%d%d%d%d", &n, &k, &A, &B, &C, &a[1], &a[2]);
	for (int i = 3; i <= n; i++)
		a[i] = A * a[i - 2] + B * a[i - 1] + C;
	printf("%d\n", qs(1, n, k));
	return 0;
}
