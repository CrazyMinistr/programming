#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }
template <typename T> T _min(T a, T b) { return a < b ? a : b; }
template <typename T> T _max(T a, T b) { return a > b ? a : b; }

using namespace std;

#define N 200004

pair<pair<int, int> , int> p[N], t[N];
pair<int, int> st[N];

int c[21][N], cnt[N], a[N], lcp[N];
int n, k, m, ans_len, ans_pos;
LL ans_ref = 0;

void radix_sort(pair<pair<int, int>, int> *p)
{
	for (int st = 0; st < 2; st++)
	{
		memset(cnt, 0, sizeof(cnt));
		for (int i = 0; i < n; i++)
			if (!st)
				++cnt[p[i].first.second + 1];
			else
				++cnt[p[i].first.first + 1];

		for (int i = 1; i <= n; i++)
			cnt[i] += cnt[i - 1];

		for (int i = n - 1; i >= 0; i--)
			if (!st)
				t[--cnt[p[i].first.second + 1]] = p[i];
			else
				t[--cnt[p[i].first.first + 1]] = p[i];

		for (int i = 0; i < n; i++)
			p[i] = t[i];
	}
}

void suff_array()
{
	memset(c, 0, sizeof(c));
	int cl = 0;
	for (int i = 0; i < n; i++)
		p[i] = make_pair(make_pair(a[i], 0), i);
	sort(p, p + n);

	for (int i = 0; i < n; i++)
	{
		cl += (i && p[i - 1].first != p[i].first);
		c[0][p[i].second] = cl;
	}

	k = 1;
	for (; (1 << (k - 1)) < n; k++)
	{
		for (int i = 0; i < n; i++)
		{
			if (i + (1 << (k - 1)) < n)
				p[i] = make_pair(make_pair(c[k - 1][i], c[k - 1][i + (1 << (k - 1))]), i);
			else
				p[i] = make_pair(make_pair(c[k - 1][i], -1), i);
		}
		radix_sort(p);
		cl = 0;
		for (int i = 0; i < n; i++)
		{
			cl += (i && p[i - 1].first != p[i].first);
			c[k][p[i].second] = cl;
		}
	}
}

int _lcp(int x, int y)
{
	int res = 0;
	for (int i = k - 1; i >= 0; i--)
		if (x < n && y < n && c[i][x] == c[i][y])
		{
			x += (1 << i);
			y += (1 << i);
			res += (1 << i);
		}
	return res;
}

int main()
{
//*
	freopen("refrain.in", "r", stdin);
	freopen("refrain.out", "w", stdout);
//*/
	cin>> n>> m;
	ans_len = ans_ref = n;
	for (int i = 0; i < n; i++)
		cin>> a[i];
	suff_array();
	for (int i = 0; i < n - 1; i++)
		lcp[i] = _lcp(p[i].second, p[i + 1].second);

	st[0] = make_pair(-1, -1);
	for (int i = 0, k = 1; i < n; i++)
	{
		for (; lcp[i] <= st[k - 1].first; --k)
			if (ans_ref < (LL) st[k - 1].first * (i - st[k - 2].second))
			{
				ans_ref = (LL) st[k - 1].first * (i - st[k - 2].second);
				ans_pos = p[i - 1].second;
				ans_len = st[k - 1].first;
			}
		st[k++] = make_pair(lcp[i], i);
	}
	cout<< ans_ref<< "\n"<< ans_len<< "\n";
	for (int i = 0; i < ans_len; i++)
		cout<< a[ans_pos + i]<< " ";
	return 0;
}
