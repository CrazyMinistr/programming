#include <cstdio>
#include <string>
#include <vector>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <algorithm>
#define N 100010
#define K 31
typedef char mas[N];

using namespace std;

mas s1,s2;
int md, l, r, n, m;
unsigned long long h, hh;

struct T
{
	int p;
	unsigned long long h;
} f[N * 2];

void hash(mas &s,int n)
{
	h = 0, hh = 1;
	for (int i = 0; i < n; i++)
	{
		h = h * K + s[i];
		hh = hh * K;
	}
}

bool my(T a, T b)
{
	return (a.h < b.h || (a.h == b.h && a.p < b.p));
}

int check(int c)
{
	hash(s1, c);
	int kol = 0, pp = 0;
	f[0].p = pp++;
	f[kol++].h = h;
	for (int i = c; i < n; i++)
	{
		h = h * K + s1[i] - hh * s1[i - c];
		f[kol].h = h;
		f[kol++].p = pp++;
	}
	int Kkol = kol;
	hash(s2, c);
	f[kol].h = h;
	f[kol++].p = pp++;
	for (int i = c; i < m; i++)
	{
		h = h * K + s2[i] - hh * s2[i - c];
		f[kol].h = h;
		f[kol++].p = pp++;
	}
	sort(f, f + kol, &my);
	vector<pair<int, bool> > v;
	for (int i = 1; i < kol; i++)
		if (f[i].h == f[i - 1].h && f[i - 1].p < Kkol && f[i].p >= Kkol)
			v.push_back(make_pair(f[i - 1].p, true));

	if (v.size() == 0)
		return -1;

	if (v.size() == 1)
		return v[0].first;

	for (int i = 0; i < c; i++)
	{
		char Min = 'z' + 1;
		for (int j = 0; j < (int) v.size(); j++)
			if (v[j].second)
				Min = min(Min, s1[v[j].first + i]);

		for (int j = 0; j < (int) v.size(); j++)
			if (v[j].second && s1[v[j].first + i] > Min)
				v[j].second = false;
	}
	Kkol = -1;
	for (int i = 0; i < (int) v.size(); i++)
		if (v[i].second)
			Kkol = v[i].first;
	return Kkol;
}

int main()
{
	freopen("common.in", "r", stdin);
	freopen("common.out", "w", stdout);
	gets(s1);
	gets(s2);
	n = strlen(s1);
	m = strlen(s2);
	l = 1, r = n;
	int ans_ps = 0, ans_len = 1;
	while (l < r)
	{
		md = (l + r) >> 1;
		int q = check(md);
		if (q == -1)
			r = md;
		else
		{
			l = md + 1;
			ans_ps = q;
			ans_len = md;
		}
	}
	int q = check(l);
	if (q == -1)
		ans_len = l - 1, ans_ps = check(l - 1);
	else
		ans_len = l, ans_ps = q;

	for (int i = ans_ps; i < ans_ps + ans_len; i++)
		cout<< s1[i];
	puts("");
	return 0;
}
