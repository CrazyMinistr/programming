#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }
template <typename T> T _min(T a, T b) { return a < b ? a : b; }
template <typename T> T _max(T a, T b) { return a > b ? a : b; }

using namespace std;

struct bore
{
	int next[26];
};
vector<bore> b;
char s[111];

int main()
{
//*
	freopen("trie.in", "r", stdin);
	freopen("trie.out", "w", stdout);
//*/
	gets(s);
	int m = 0;
	int n = strlen(s);
	for (int i = 0; i < n; i++)
	{
		int v = 0;
		for (int j = i; j < n; j++)
		{
			int p = s[j] - 'a';
			if (b.size() == v || b[v].next[p] == -1)
			{
				++m;
				bore t;
				memset(t.next, 255, sizeof(t.next));
				b.push_back(t);
				b[v].next[p] = b.size();
			}
			v = b[v].next[p];
		}
	}
	cout<< b.size() + 1<< " "<< m<< endl;
	for (int i = 0; i < (int) b.size(); i++)
		for (int j = 0; j < 26; j++)
			if (b[i].next[j] != -1)
				cout<< i + 1<< " "<< b[i].next[j] + 1<< " "<< char(j + 'a')<< endl;
	return 0;
}
