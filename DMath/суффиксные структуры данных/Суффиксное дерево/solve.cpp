#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <iostream>
using namespace std;

struct node
{
    int l, r, prev, link;
    map<char, int> next;
    bool leaf;
 
    node(int l = -1, int r = -1, int p = -1, bool leaf = false) : l(l), r(r), prev(p), leaf(leaf) { link = -1; };
 
    int length() { return r - l; }
 
    int& operator[](char c)
    {
        if (!next.count(c))
            next[c] = -1;
        return next[c];
    }
};
 
struct state
{
    int v, pos;
    state(int v, int p) : v(v), pos(p) { };
};
 
string str;
vector<node> suff;
int edges, n;
 
state go(state st, int l, int r)
{
    while (l < r)
    {
        node v = suff[st.v];
        if (st.pos == v.length())
        {
            st = state(v[str[l]], 0);
            if (st.v == -1)
                return st;
        }
        else
        {
            if (str[v.l + st.pos] != str[l])
                return state(-1, -1);
            if (r - l < v.length() - st.pos)
                return state(st.v, st.pos + r - l);
 
            l += v.length() - st.pos;
            st.pos = v.length();
        }
    }
    return st;
}
 
int split(state st)
{
    node v = suff[st.v];
    if (st.pos == v.length())
        return st.v;
 
    if (st.pos == 0)
        return v.prev;
 
    suff.push_back(node(v.l, v.l + st.pos, v.prev, false));
    ++edges;
    suff[v.prev][str[v.l]] = suff.size() - 1;
    suff.back()[str[v.l + st.pos]] = st.v;
    suff[st.v].prev = suff.size() - 1;
    suff[st.v].l += st.pos;
    return suff.size() - 1;
}
 
int get_link(int v)
{
    if (suff[v].link != -1)
        return suff[v].link;
 
    if (suff[v].prev == -1)
        return 0;
 
    int suf_link = get_link(suff[v].prev);
    int newlink = split(go(state(suf_link, suff[suf_link].length()), suff[v].l + (suff[v].prev == 0), suff[v].r));
    return suff[v].link = newlink;
}
 
int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	cin>> str;
    n = str.size();
    edges = 0;
    state st(0, 0);
    suff.resize(1);
 
    for (int i = 0; i < n; i++)
        for (;;)
        {
            state new_state = go(st, i, i + 1);
            if (new_state.v != -1)
            {
                st = new_state;
                break;
            }
 
            int splt = split(st);
            suff.push_back(node(i, n, splt, true));
            ++edges;
            suff[splt][str[i]] = suff.size() - 1;
 
            st.v = get_link(splt);
            st.pos = suff[st.v].length();
 
            if (splt == 0)
                break;
        }
 
    cout << suff.size() << " " << edges << "\n";
 
    for (size_t i = 0; i < suff.size(); i++)
        for (map<char, int>::iterator it = suff[i].next.begin(); it != suff[i].next.end(); ++it)
            if (it->second > -1)
                cout << i + 1 << ' ' << it->second + 1 << ' ' << suff[it->second].l + 1 << ' ' << suff[it->second].r << '\n';

    puts("");
    return 0;
}
