#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

struct list
{
	int x;
	list *prev;
	list()
	{
		x = 0;
		prev = NULL;
	}
};

struct Stack
{
	list *head;
	Stack(){ head = new list(); }
	
	void push(int x)
	{
		list* add = new list();
		add->x = x;
		add->prev = head;
		head = add;
	}

	int pop()
	{
		int x = head->x;
		list *t = head->prev;
		delete head;
		head = t;
		return x;
	}
} st;

int m, x;
int main()
{
	freopen("stack2.in", "r", stdin);
	freopen("stack2.out", "w", stdout);
	cin>> m;
	while (m--)
	{
		char c;
		scanf("\n%c", &c);
		if (c == '+')
		{
			scanf("%d", &x);
			st.push(x);
		}
		else printf("%d\n", st.pop());
	}
	return 0;
}
