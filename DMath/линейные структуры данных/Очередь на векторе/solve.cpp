#include<cstdio>
#include<vector>
#include<string>
#include<cstring>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

struct Queue
{
	int size, head, tail;
	int *v, *t;
	Queue()
	{
		size = 1, head = tail = 0;
		v = new int[10];
	}

	void push(int x)
	{
		if (tail == size)
		{
			t = new int[size << 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			v = t, size <<= 1;
			tail -= head, head = 0;
		}
		v[tail++] = x;
	}

	int pop()
	{
		int res = v[head++];
		if (tail - head < size >> 2)
		{
			t = new int[size >> 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			v = t, size >>= 1;
			tail -= head, head = 0;
		}
		return res;
	}
} q;

int main()
{
	freopen("queue1.in", "r", stdin);
	freopen("queue1.out", "w", stdout);
	int m;
	char c;
	cin>> m;
	while (m--)
	{
		scanf("\n%c", &c);
		if (c == '+')
		{
			int x;
			scanf("%d", &x);
			q.push(x);
		}
		else printf("%d\n", q.pop());
	}
	return 0;
}
