#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

struct mystack
{
	int size, n;
	int *v, *t;
	mystack()
	{
		size = 1, n = 0;
		v = new int[10];
	}

	int pick()
	{
		return n == 0 ? 0 : v[n - 1];
	}

	void push(int x)
	{
		if (n == size)
		{
			t = new int[size << 1];
			for (int i = 0; i < n; i++) t[i] = v[i];
			delete[] v;
			v = t, size <<= 1;
		}
		v[n++] = x;
	}

	void pop()
	{
		--n;
		if (n < size >> 2)
		{
			t = new int[size >> 1];
			for (int i = 0; i < n; i++) t[i] = v[i];
			delete[] v;
			v = t, size >>= 1;
		}
	}
} st;

string s;
int main()
{
	freopen("brackets.in", "r", stdin);
	freopen("brackets.out", "w", stdout);
	while (cin>> s)
	{
		st.n = 0;
		bool fail = 0;
		for (int i = 0; i < (int) s.size(); i++)
		{
			if (s[i] == '(')
				st.push(1);
			else if (s[i] == '[')
				st.push(2);
			else if (s[i] == ')')
			{
				if (st.pick() != 1)
				{
					fail = 1;
					break;
				}
				st.pop();
			}
			else if (s[i] == ']')
			{
				if (st.pick() != 2)
				{
					fail = 1;
					break;
				}
				st.pop();
			}
		}
		puts(fail || st.n ? "NO" : "YES");
	}
	return 0;
}
