#include<cstdio>
#include<vector>
#include<string>
#include<cstring>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

struct mystack
{
	int size, n;
	int *v, *t;
	mystack()
	{
		size = 1, n = 0;
		v = new int[10];
	}

	void push(int x)
	{
		if (n == size)
		{
			t = new int[size << 1];
			for (int i = 0; i < n; i++) t[i] = v[i];
			delete[] v;
			v = t, size <<= 1;
		}
		v[n++] = x;
	}

	int pop()
	{
		int res = v[--n];
		if (n < size >> 2)
		{
			t = new int[size >> 1];
			for (int i = 0; i < n; i++) t[i] = v[i];
			delete[] v;
			v = t, size >>= 1;
		}
		return res;
	}
} st;

string s;
int main()
{
	freopen("postfix.in", "r", stdin);
	freopen("postfix.out", "w", stdout);
	while (cin>> s)
	{
		if (s == "+" || s == "-" || s == "*")
		{
			int x = st.pop(),
				y = st.pop();
			if (s == "+") st.push(x + y);else
			if (s == "-") st.push(y - x);else
			if (s == "*") st.push(x * y);
		}
		else st.push(atoi(s.c_str()));
	}
	cout<< st.pop()<< endl;
	return 0;
}
