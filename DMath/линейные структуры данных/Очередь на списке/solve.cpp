#include<cstdio>
#include<vector>
#include<string>
#include<cstring>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;

struct list
{
	int x;
	list* prev;
	list* next;
	list()
	{
		x = 0;
		prev = NULL;
		next = NULL;
	}
};

struct Queue
{
	list* head;
	list* tail;
	Queue()
	{
		head = new list();
		tail = new list();
		head->prev = tail;
		tail->next = head;
	}

	void push(int x)
	{
		list* add = new list();
		add->x = x;
		add->next = tail->next;
		tail->next->prev = add;
		tail->next = add;
		add->prev = tail;
	}

	int pop()
	{
		list* del = head->prev;
		int x = del->x;
		head->prev = del->prev;
		del->prev->next = head;
		delete del;
		return x;
	}
} q;

int m, x;
int main()
{
	freopen("queue2.in", "r", stdin);
	freopen("queue2.out", "w", stdout);
	scanf("%d", &m);
	while (m--)
	{
		char c;
		scanf("\n%c", &c);
		if (c == '+')
		{
			scanf("%d", &x);
			q.push(x);
		}
		else printf("%d\n", q.pop());
	}
	return 0;
}
