#include<cstdio>
#include<vector>
#include<string>
#include<cstring>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

struct Queue
{
	int size, head, tail;
	int *v, *t;
	Queue()
	{
		size = 1, head = tail = 0;
		v = new int[10];
	}

	void push(int x)
	{
		if (tail == size)
		{
			t = new int[size << 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			v = t, size <<= 1;
			tail -= head, head = 0;
		}
		v[tail++] = x;
	}

	void pop()
	{
		++head;
		if (tail - head < size >> 2)
		{
			t = new int[size >> 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			v = t, size >>= 1;
			tail -= head, head = 0;
		}
	}

	void pop_back(){ --tail; }

	int Min(){ return v[head]; }

	int front(){ return v[head]; }

	bool empty(){ return tail - head == 0; }

	int back(){ return tail - head > 0 ? v[tail - 1] : INT_MIN; }

} qmin, q;

int main()
{
	freopen("queuemin.in", "r", stdin);
	freopen("queuemin.out", "w", stdout);
	int m;
	char c;
	cin>> m;
	while (m--)
	{
		scanf("\n%c", &c);
		if (c == '+')
		{
			int x;
			scanf("%d", &x);
			while (!qmin.empty() && qmin.back() > x) qmin.pop_back();
			q.push(x);
			qmin.push(x);
		}
		else if (c == '-')
		{
			if (!qmin.empty() && qmin.front() == q.front()) qmin.pop();
			q.pop();
		}
		else
			printf("%d\n", qmin.Min());
	}
	return 0;
}
