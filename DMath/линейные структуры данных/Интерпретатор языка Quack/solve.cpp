#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define mod 65536
#define hash_mod 3456789

struct Queue
{
	int *v, *t;
	int head, tail, size;
	Queue()
	{
		size = 1;
		head = tail = 0;
		v = new int[10];
	}

	int pop()
	{
		int res = v[head++];
		if (tail - head < size >> 2)
		{
			t = new int[size >> 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			tail -= head, head = 0;
			size >>= 1, v = t;
		}
		return res;
	}

	void push(int x)
	{
		if (tail == size)
		{
			t = new int[size << 1];
			for (int i = head; i < tail; i++) t[i - head] = v[i];
			delete[] v;
			tail -= head, head = 0;
			size <<= 1, v = t;
		}
		v[tail++] = x;
	}
} q;

string s;
int reg[30];
int ps[hash_mod + 10];
vector<string> v;

int hash()
{
	int h = 0;
	for (int i = 0; i < (int) s.size(); i++)
		h = (h * 31 + s[i]) % hash_mod;
	return h;
}

int main()
{
	freopen("quack.in", "r", stdin);
	freopen("quack.out", "w", stdout);
	while (cin>> s)
	{
		v.push_back(s);
		if (s[0] == ':')
		{
			s.erase(0, 1);
			ps[hash()] = v.size() - 1;
		}
	}
	for (int i = 0; i < (int) v.size(); i++)
	{
		s = v[i];
		if (s[0] == '+')
		{
			int x = q.pop(),
				y = q.pop();
		     q.push((x + y) % mod);
		}
		else if (s[0] == '-')
		{
			if (s.size() == 1)
			{
				int x = q.pop(),
					y = q.pop();
				q.push((x - y + 2 * mod) % mod);
			}
			else
				q.push(atoi(s.c_str()) % mod);
		}
		else if (s[0] == '*')
		{
			int x = q.pop(),
				y = q.pop();
			LL t = ((LL) x * y) % mod;
			q.push(t);
		}
		else if (s[0] == '/')
		{
			int x = q.pop(),
				y = q.pop();
			q.push(y == 0 ? 0 : x / y);
		}
		else if (s[0] == '%')
		{
			int x = q.pop(),
				y = q.pop();
			q.push(y == 0 ? 0 : x % y);
		}
		else if (s[0] == '>') reg[s[1] - 'a'] = q.pop();
		else if (s[0] == '<') q.push(reg[s[1] - 'a']);
		else if (s[0] == 'P')
		{
			if (s.size() == 1)
				printf("%d\n", q.pop());
			else
				printf("%d\n", reg[s[1] - 'a']);
		}
		else if (s[0] == 'C')
		{
			if (s.size() == 1)
				printf("%c", (char) (q.pop() % 256));
			else
				printf("%c", (char) (reg[s[1] - 'a'] % 256));
		}
		else if (s[0] == 'J')
		{
			s.erase(0, 1);
			i = ps[hash()];
		}
		else if (s[0] == 'Z')
		{
			if (reg[s[1] - 'a'] == 0)
			{
				s.erase(0, 2);
				i = ps[hash()];
			}
		}
		else if (s[0] == 'E')
		{
			if (reg[s[1] - 'a'] == reg[s[2] - 'a'])
			{
				s.erase(0, 3);
				i = ps[hash()];
			}
		}
		else if (s[0] == 'G')
		{
			if (reg[s[1] - 'a'] > reg[s[2] - 'a'])
			{
				s.erase(0, 3);
				i = ps[hash()];
			}
		}
		else if (s[0] == 'Q') break;
		else if (s[0] != ':') q.push(atoi(s.c_str()) % mod);
	}
	return 0;
}
