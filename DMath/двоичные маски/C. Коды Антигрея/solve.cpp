#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

bool was[66000];
int n, x, p3[13], a[13], b[13];
int d[3][2] = 
{
{1, 2},
{2, 0},
{0, 1}
};

void write(int x)
{
	int m = 0;
	while (x) a[++m] = x % 3, x /= 3;
	for (int i = 0; i < n - m; i++) printf("0");
	for (int i = m; i; i--) printf("%d", a[i]);
	puts("");
}

int ss3(int *a, int n)
{
	int x = 0;
	for (int i = 1; i <= n; i++) x += a[i] * p3[n - i];
	return x;
}

int next(int x)
{
	int m = 0, k;
	memset(a, 0, sizeof(a));
	while (x) a[++m] = x % 3, x /= 3;
	reverse(a + 1, a + 1 + n);
	while (1)
	{
		for (int msk = 0; msk < (1 << n); msk++) 
		{
			for (int i = n; i; i--)
			{
				k = (msk & (1 << (i - 1))) > 0;
				b[n - i + 1] = d[a[n - i + 1]][k];
			}
			x = ss3(b, n);
			if (!was[x])
			{
				was[x] = 1;
				return x;
			}
		}
	}
}

int main()
{
//*
	freopen("antigray.in", "r", stdin);
	freopen("antigray.out", "w", stdout);
//*/
	cin>> n;
	p3[0] = x = 1;
	for (int i = 1; i <= n; i++) p3[i] = p3[i - 1] * 3, x += p3[i] * (i < n);
	was[x] = 1;
	for (int it = 0; it < p3[n]; it++)
	{
		write(x);
		if (it + 1 == p3[n]) break;
		x = next(x);
	}
	return 0;
}
