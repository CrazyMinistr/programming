#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;

int n;
int main()
{
//*
	freopen("gray.in", "r", stdin);
	freopen("gray.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 0; i < (1 << n); i++, puts(""))
	{
		int x = i ^ (i >> 1);
		for (int j = n; j; j--) printf("%c", (x & (1 << (j - 1))) ? '1' : '0');
	}
	return 0;
}
