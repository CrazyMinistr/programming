import java.io.BufferedReader;
import java.io.PrintWriter;

import java.io.FileReader;

import java.io.IOException;

import java.util.StringTokenizer;

 

class Node {

        int key;

        Node left;

        Node right;

        Node parent;

        int count;

 

        public Node(int key, Node left, Node right, Node parent) {

                this.key = key;

                this.left = left;

                this.right = right;

                this.parent = parent;

                this.count = 1;

                if (left != null) {

                        this.count += left.count;

                        this.left.parent = this;

                }

                if (right != null) {

                        this.count += right.count;

                        this.right.parent = this;

                }

        }

 

        public void recount() {

                this.count = 1;

                if (left != null)

                        this.count += left.count;

                if (right != null)

                        this.count += right.count;

        }

}

 

class Record {

        SplayTree t1;

        SplayTree t2;

 

        public Record(SplayTree t1, SplayTree t2) {

                this.t1 = t1;

                this.t2 = t2;

        }

}

 

class SplayTree {

        public Node root;

 

        public SplayTree(Node root) {

                this.root = root;

        }

 

        public SplayTree() {

        }

 

        public void add(int i) {

                root = new Node(i, root, null, null);

        }

 

        public void splay(Node p) {

                while (p.parent != null) {

                        if (p.parent == root)

                                zig(p);

                        else if ((p == p.parent.left) && (p.parent == p.parent.parent.left))

                                zigzig(p);

                        else

                                zigzag(p);

                }

        }

 

        public void zig(Node p) {

                turn(p);

        }

 

        public void zigzig(Node p) {

                turn(p.parent);

                turn(p);

        }

 

        public void zigzag(Node p) {

                turn(p);

                turn(p);

        }

 

        public void turn(Node y) {

                Node x = y.parent;

                if (x == null) {

                        return;

                }

                if (y == x.left) {

                        y.parent = x.parent;

                        if (x.parent != null) {

                                if (x == x.parent.left)

                                        x.parent.left = y;

                                else

                                        x.parent.right = y;

                        }

                        x.left = y.right;

                        if (y.right != null)

                                x.left.parent = x;

                        x.parent = y;

                        y.right = x;

                        if (y.parent == null)

                                this.root = y;

                } else {

                        y.parent = x.parent;

                        if (x.parent != null) {

                                if (x == x.parent.left)

                                        x.parent.left = y;

                                else

                                        x.parent.right = y;

                        }

                        x.right = y.left;

                        if (y.left != null)

                                x.right.parent = x;

                        x.parent = y;

                        y.left = x;

                        this.root = y;

                }

                x.recount();

                y.recount();

        }

 

        public static Record split(SplayTree tree, int i) {

                Node p = tree.root;

                while (p.count != 0) {

                        if (p.left != null) {

                                if (p.left.count > i)

                                        p = p.left;

                                else if (p.left.count == i) {

                                        p = p.left;

                                        break;

                                } else if (p.left.count < i) {

                                        i -= p.left.count;

                                        i--;

                                        if (i == 0)

                                                break;

                                        else

                                                p = p.right;

                                }

                        } else {

                                i--;

                                if (i == 0)

                                        break;

                                else

                                        p = p.right;

                        }

                }

                tree.splay(p);

                Record t = new Record(new SplayTree(tree.root.left), new SplayTree(

                                tree.root));

                if (tree.root.left != null) {

                        tree.root.left.parent = null;

                        tree.root.left = null;

                        tree.root.recount();

                }

                return t;

        }

 

        public void replace(SplayTree tree, int l, int r) {

                if (l == 1)

                        return;

                if (r == root.count) {

                        Record t = split(tree, l);

                        SplayTree a = merge(t.t2, t.t1);

                        this.root = a.root;

                } else {

                        r++;

                        Record t = split(tree, r);

                        Record t1 = split(t.t1, l);

                        SplayTree a = merge(t1.t2, t1.t1);

                        a = merge(a, t.t2);

                        tree.root = a.root;

                }

        }

 

        public SplayTree merge(SplayTree l, SplayTree r) {

                Node k = l.root;

                while (k.right != null)

                        k = k.right;

                l.splay(k);

                k.right = r.root;

                r.root.parent = k;

                k.recount();

                SplayTree t = l;

                return t;

        }

 

        public void output(Node root) {

                if (root != null) {

                        output(root.left);

                        MoveToFront.out.print(root.key + " ");

                        output(root.right);

                }

        }

}

 

public class MoveToFront {

 

        static BufferedReader in;

        static StringTokenizer st;

        static PrintWriter out;

 

        static String nextToken() throws IOException {

                while (st == null || !st.hasMoreTokens()) {

                        st = new StringTokenizer(in.readLine());

                }

                return st.nextToken();

        }

 

        static Integer nextInt() throws IOException {

                return Integer.parseInt(nextToken());

        }

 

        public static void main(String[] args) throws NumberFormatException,

                        IOException {

                in = new BufferedReader(new FileReader("movetofront.in"));

                out = new PrintWriter("movetofront.out");

 
				
                //int n = nextInt();
                //int m = nextInt();
				
				String s = in.readLine();
				String[] tmp = s.split(" ");
				int n = Integer.parseInt(tmp[0]);
				int m = Integer.parseInt(tmp[1]);

                SplayTree tree = new SplayTree();

 

                for (int i = 1; i <= n; ++i) {

                        tree.add(i);

                }

 

                for (int i = 0; i < m; ++i) {

                       // int l = nextInt();

                        //int r = nextInt();
						
						s = in.readLine();
						tmp = s.split(" ");
						int l = Integer.parseInt(tmp[0]);
						int r = Integer.parseInt(tmp[1]);
						
                        tree.replace(tree, l, r);

                }

 

                tree.output(tree.root);

 

                in.close();

                out.close();

        }

}
