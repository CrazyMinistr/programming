#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;

typedef struct Tree
{
	int key;
	Tree *left, *right, *parent;
} Node;

#define NIL &sentinel
Node sentinel = {0, 0, 0, 0};

Node *root = NIL;
Node *y, *t;

Node* Max(Node *x)
{
	if (x->right != NIL)
		return Max(x->right);
	return x;
}

Node* Min(Node *x)
{
	if (x->left != NIL)
		return Min(x->left);
	return x;
}

Node *nextNode(Node *x, int key)
{
	if (x == NIL) return NIL;
	if (key < x->key)
		return (x->left != NIL) ? nextNode(x->left, key) : x;
	else
	if (key > x->key)
	{
		if (x->right != NIL)
			return nextNode(x->right, key);
		else
		{
			t = x;
			y = t->parent;
			while (y != NIL && t == y->right)
			{
				t = y;
				y = y->parent;
			}
			return y;
		}
	}
	else
	{
		if (x->right != NIL)
			return Min(x->right);

		t = x;
		y = t->parent;
		while (y != NIL && t == y->right)
		{
			t = y;
			y = y->parent;
		}
		return y;
	}
}

Node *prevNode(Node *x, int key)
{
	if (x == NIL) return NIL;
	if (key > x->key)
		return (x->right != NIL) ? prevNode(x->right, key) : x;
	else if (key < x->key)
	{
		if (x->left != NIL)
			return prevNode(x->left, key);
		else
		{
			t = x;
			y = t->parent;
			while (y != NIL && t == y->left)
			{
				t = y;
				y = y->parent;
			}
			return y;
		}
	}
	else
	{
		if (x->left != NIL)
			return Max(x->left);

		t = x;
		y = t->parent;
		while (y != NIL && t == y->left)
		{
			t = y;
			y = y->parent;
		}
		return y;
	}
}

void insertNode(Node *x, int key, Node *p)
{
	if (x == NIL)
	{
		x = (Node*) malloc(sizeof(*x));
		x->key = key;
		x->parent = p;
		x->left = NIL;
		x->right = NIL;
		if (p != NIL)
		{
			if (p->key > key)
				p->left = x;
			else
				p->right = x;
		}else root = x;
	}
	else insertNode(key > x->key ? x->right : x->left, key, x);
}

string findNode(Node *x, int key)
{
	if (x == NIL)
		return "false";
	if (key == x->key)
		return "true";
	if (key > x->key)
		return findNode(x->right, key);
	else
		return findNode(x->left, key);
	return "false";
}

void replace(Node *a, Node *b)
{
	if (a->parent == NIL)
		root = b;
	else if (a == a->parent->left)
		a->parent->left = b;
	else
		a->parent->right = b;
	if (b != NIL)
		b->parent = a->parent;
}

void deleteNode(Node *x, int key)
{
	if (x == NIL) return;
	if (key < x->key)
		deleteNode(x->left, key);
	else if (key > x->key)
		deleteNode(x->right, key);
	else if (x->left != NIL && x->right != NIL)
	{
		Node *t = x->right;
		while (t->left != NIL) t = t->left;
		x->key = t->key;
		replace(t, t->right);
	}
	else if (x->left != NIL)
		replace(x, x->left);
	else if (x->right != NIL)
		replace(x, x->right);
	else replace(x, NIL);
}

int main()
{
//*
	freopen("bstsimple.in", "r", stdin);
	freopen("bstsimple.out", "w", stdout);
//*/
	int x;
	string s;
	while (cin>> s)
	{
		Node *v;
		scanf("%d", &x);
		if (s[0] == 'i') if (findNode(root, x) == "false") insertNode(root, x, NIL);
		if (s[0] == 'd') deleteNode(root, x);
		if (s[0] == 'e') cout<< findNode(root, x)<< endl;
		if (s[0] == 'n')
		{
			v = nextNode(root, x);
			if (v == NIL)
				puts("none");
			else
				printf("%d\n", v->key);
		}
		if (s[0] == 'p')
		{
			v = prevNode(root, x);
			if (v == NIL)
				puts("none");
			else
				printf("%d\n", v->key);
		}
	}
	return 0;
}
