#include<iostream>
using namespace std;

struct Tree{
	int k;
	Tree *l, *r, *p;
};

typedef Tree T;
T *root = 0, *y, *t;

int next(T *x, int k){
    T *tr = x, *val = 0;
    while (tr){
        if (tr->k > k){
            val = tr;
            tr = tr->l;
        }else tr = tr->r;
    }
    return !val ? (int)2e9 : val->k;
}

int prev(T *x, int k){
    T *tr = x, *val = 0;
    while (tr){
        if (tr->k < k){
            val = tr;
            tr = tr->r;
        }else tr = tr->l;
    }
    return !val ? (int)2e9 : val->k;
}

void insert(T *x, int k, T *p){
	if (!x){
		x = new Tree();
		x->k = k;
		x->p = p;
		x->l = x->r = 0;
		if (!p)
		if (p->k > k) p->l = x;else p->r = x;else root = x;
	}else insert(k > x->k ? x->r : x->l, k, x);
}

string find(T *x, int k){
	if (!x) return "false";
	if (k == x->k) return "true";
	if (k > x->k) return find(x->r, k);else return find(x->l, k);
	return "false";
}

void f(T *a, T *b){
	if (!a->p) root = b;else
	if (a == a->p->l) a->p->l = b;else a->p->r = b;
	if (b) b->p = a->p;
}

void del(T *x, int k){
	if (!x) return;
	if (k < x->k) del(x->l, k);else
	if (k > x->k) del(x->r, k);else
	if (x->l && x->r){
		T *t = x->r;
		while (t->l) t = t->l;
		x->k = t->k;
		f(t, t->r);
	}
	else if (x->l) f(x, x->l);
	else if (x->r) f(x, x->r);else f(x, 0);
}

int main(){
	freopen("bstsimple.in", "r", stdin);
	freopen("bstsimple.out", "w", stdout);
	int x, v;
	string s;
	while (cin>> s){
		scanf("%d", &x);
		if (s[0] == 'i' && find(root, x) == "false") insert(root, x, 0);
		if (s[0] == 'd') del(root, x);
		if (s[0] == 'e') cout<< find(root, x)<< endl;
		if (s[0] == 'n') v = next(root, x), v == (int)2e9 ? cout<<v<< endl : puts("none");
		if (s[0] == 'p') v = prev(root, x), v == (int)2e9 ? cout<<v<< endl : puts("none");
	}
	return 0;
}
