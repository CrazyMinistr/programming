#include <algorithm>
#include <cstdio>
#include <iostream>
#include <ctime>
#include <cstdlib>
#define null NULL
#define inf 1000000000
using namespace std;
#define N 100111

int a[N];
struct treap {
	int y, u, mmin, cnt;
	treap *l, *r;
	treap() {};
	treap(int y, int u, treap * l = null, treap * r = null) {
		this->y = y;
		this->cnt = 1;
		this->l = l;
		this->r = r;
		this->u = this->mmin = u;
	}
};

typedef treap * ptreap;

int cnt(ptreap t) {
	if (t == null) {
		return 0;
	}
	return t->cnt;
}

int mm(ptreap t) {
	if (t == null) {
		return inf;
	}
	return t->mmin;
}

void recalc(ptreap t) {
	if (t != null) {
		t->cnt = 1 + cnt(t->l) + cnt(t->r);
		t->mmin = min(t->u, min(mm(t->l), mm(t->r)));
	}
}

void merge(ptreap & t, ptreap l, ptreap r) {
	if (l == null || r == null) {
		t = l ? l : r;
	}
	else {
		if (l->y > r->y) {
			merge(l->r, l->r, r);
			t = l;
		}
		else {
			merge(r->l, l, r->l);
			t = r;
		}
	}
	recalc(t);
}

void split(ptreap t, int x, ptreap & l, ptreap &r) {
	if (t == null) {
		r = l = null;
	}
	else {
		int q = cnt(t->l);
		if (x <= q) {
			split(t->l, x, l, t->l);
			r = t;
		}
		else {
			split(t->r, x - q - 1, t->r, r);
			l = t;
		}
	}
	recalc(l);
	recalc(r);
}

void insert(ptreap &t, int x, int y, int u) {
	ptreap l, r, m = new treap(y, u);
	split(t, x, l, r);
	merge(l, l, m);
	merge(t, l, r);
}

void erase(ptreap &t, int x) {
	ptreap l, r, m;
	split(t, x-1, l, r);
	split(r, x, m, r);
	merge(t, l, r);
}

int minim(ptreap &t, int l, int r) {
	ptreap a, b, c;
	split(t, r, a, b);
	split(a, l-1, a, c);
	int m = c->mmin;
	merge(a, a, c);
	merge(t, a, b);
	return m;
}

void tofront(ptreap &t, int l, int r) {
	ptreap a, b, c;
	split(t, r, a, b);
	split(a, l-1, a, c);
	merge(a, c, a);
	merge(t, a, b);
}

void print_tree(ptreap t) {
	if (t != null) {
		print_tree(t->l);
		cout << t->u << " ";
		print_tree(t->r);
	}
}

int main(){
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
	srand(time(0));
	int i, n, m, x, y;
	ptreap tree = null;

	scanf("%d %d\n", &n, &m);
	for (i = 0; i < n; i++) {
		insert(tree, i, (rand() << 15) ^ rand(), i+1);
	}
	for (i = 0; i < m; i++) {
		scanf("%d %d\n", &x, &y);
		tofront(tree, x, y);
	}
	print_tree(tree);
	return 0;
}
