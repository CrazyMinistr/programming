#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<string.h>
#include<iostream>
#include<algorithm>
#define null NULL
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 100111

int d[N], itd = 0;
struct treap
{
	int y, key;
	treap *l, *r;
	treap(int y, int key, treap * l = null, treap * r = null)
	{
		this->y = y;
		this->l = l;
		this->r = r;
		this->key = key;
	}
};

typedef treap* tree;
tree root = null;

tree next(tree x, int key)
{
    tree troot = x, ans = null;
    while (troot != null)
    {
        if (troot->key > key)
        {
            ans = troot;
            troot = troot->l;
		}else troot = troot->r;
	}
    return ans;
}

tree prev(tree x, int key)
{
    tree troot = x, ans = null;
    while (troot != null)
    {
        if (troot->key < key)
        {
            ans = troot;
            troot = troot->r;
		}else troot = troot->l;
	}
    return ans;
}

void split(tree t, int key, tree &l, tree & r)
{
	if (!t)
		l = r = NULL;
	else if (key < t->key)
		split (t->l, key, l, t->l), r = t;
	else
		split (t->r, key, t->r, r), l = t;
}

void insert(tree &t, tree it)
{
	if (t == null) t = it;
	else if (it->y > t->y)
		split(t, it->key, it->l, it->r), t = it;
	else
		insert(it->key < t->key ? t->l : t->r, it);
}


void merge(tree &t, tree l, tree r)
{
	if (!l || !r)
		t = l ? l : r;
	else if (l->y > r->y)
		merge(l->r, l->r, r), t = l;
	else
		merge(r->l, l, r->l), t = r;
}

void erase(tree &t, int key)
{
	if (t->key == key)
		merge(t, t->l, t->r);
	else
		erase(key < t->key ? t->l : t->r, key);
}

string find(tree t, int key)
{
	if (t == null) return "false";
	if (t->key == key) return "true";
	if (t->key < key)
		return find(t->r, key);
	else
		return find(t->l, key);
}

int main()
{
//*
	freopen("bst.in", "r", stdin);
	freopen("bst.out", "w", stdout);
//*/
	int x;
	string s;
	for (int i = 0; i < N; i++) d[i] = i;
	random_shuffle(d, d + N);
	while (cin>> s)
	{
		scanf("%d", &x);
		if (s[0] == 'i' && find(root, x) == "false")
		{
			tree v = new treap(d[itd++], x);
			insert(root, v);
		}
		if (s[0] == 'd' && find(root, x) == "true") erase(root, x);
		if (s[0] == 'e') cout<< find(root, x)<< endl;
		if (s[0] == 'n')
		{
			tree v = next(root, x);
			if (v == null)
				puts("none");
			else
				printf("%d\n", v->key);
		}
		if (s[0] == 'p')
		{
			tree v = prev(root, x);
			if (v == null)
				puts("none");
			else
				printf("%d\n", v->key);
		}
	}
	return 0;
}
