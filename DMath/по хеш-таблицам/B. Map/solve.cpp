#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define hashTableSize 1000007
using namespace std;

int x;
string s, key, value;
struct list
{
	string key, value;
	list *next;
	list()
	{
		key = "";
		value = "";
		next = NULL;
	}
};

int hash(string s)
{
	unsigned long long h = 0;
	int n = s.size();
	for (int i = 0; i < n; i++)
		h = h * 29 + s[i];

	return h % hashTableSize;
}

struct dataHashTable
{
	list *head;
	dataHashTable(){ head = new list(); }

	bool find(string key)
	{
		list *p = head;
		while (p-> key != "" && p->key != key) 
			p = p->next;
		return p->key == "" ? 0 : 1;
	}

	string get(string key)
	{
		list *p = head;
		while (p->key != "" && p->key != key)
			p = p->next;
		return p->key == "" ? "none" : p->value;
	}

	void insert(string key, string value)
	{
		if (find(key))
		{
			list *p = head;
			while (p->key != "" && p->key != key)
				p = p->next;
			p->value = value;
			return;
		}
		list *p, *p0;
		p = new list();
		p0 = head;
		head = p;
		p->next = p0;
		p->key = key;
		p->value = value;
	}

	void del(string key)
	{
		list *p0, *p;
		p = head, p0 = new list();
		while (p->key != "" && p->key != key)
			p0 = p, p = p->next;
		if (p->key == "") return;
		if (p0->key != "")
			p0->next = p->next;
		else
			head = p->next;
		delete p;
	}
} HashTable[hashTableSize];

int main()
{
//*
	freopen("map.in", "r", stdin);
	freopen("map.out", "w", stdout);
//*/
	while (cin>> s>> key)
	{
		if (s == "put")
		{
			cin>> value;
			HashTable[hash(key)].insert(key, value);
		}
		if (s == "delete")
			HashTable[hash(key)].del(key);

		if (s == "get")
			cout<< HashTable[hash(key)].get(key)<< endl;
	}
	return 0;
}
