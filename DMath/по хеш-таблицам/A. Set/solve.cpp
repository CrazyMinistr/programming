#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define hashTableSize 1000007

int x;
char s[11];
struct list
{
	int x;
	list *next;
	list()
	{
		x = (int) 2e9;
		next = NULL;
	}
};

int hash(int x)
{
	return (x + (int) 1e9) % hashTableSize;
}

struct dataHashTable
{
	list *head;
	dataHashTable(){ head = new list(); }

	bool find(int x)
	{
		list *p = head;
		while (p && p->x != x) 
			p = p->next;
		return p == NULL ? 0 : 1;
	}

	void insert(int x)
	{
		if (find(x)) return;
		list *p, *p0;
		p = new list();
		p0 = head;
		head = p;
		p->next = p0;
		p->x = x;
	}

	void del(int x)
	{
		list *p0, *p;
		p0 = NULL, p = head;
		while (p && p->x != x)
			p0 = p, p = p->next;
		if (!p) return;
		if (p0)
			p0->next = p->next;
		else
			head = p->next;
		delete p;
	}

} HashTable[hashTableSize];

int main()
{
//*
	freopen("set.in", "r", stdin);
	freopen("set.out", "w", stdout);
//*/
	while (scanf("%s%d", s, &x) == 2)
	{
		if (!strcmp(s, "insert"))
			HashTable[hash(x)].insert(x);
		if (!strcmp(s, "delete"))
			HashTable[hash(x)].del(x);
		if (!strcmp(s, "exists"))
			puts(HashTable[hash(x)].find(x) ? "true" : "false");
	}
	return 0;
}
