#include<list>
#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 1000007
 
int x;
char s[11];
list<int> hashTable[N];
list<int> :: iterator it;
 
int hash(int x)
{
	return (x + (int) 1e9) % N;
}
 
bool find(int x)
{
    int h = hash(x);
    for (it = hashTable[h].begin(); it != hashTable[h].end(); it++)
        if ((*it) == x)
            return 1;
    return 0;
}
 
void insert(int x)
{
    if (find(x)) return;
    int h = hash(x);
    hashTable[h].push_back(x);
}
 
void del(int x)
{
    if (!find(x)) return;
    int h = hash(x);
    for (it = hashTable[h].begin(); it != hashTable[h].end(); it++)
        if ((*it) == x)
        {
            hashTable[h].erase(it);
            break;
        }
}
 
int main()
{
//*
    freopen("set.in", "r", stdin);
    freopen("set.out", "w", stdout);
//*/
    while (scanf("%s%d", s, &x) == 2)
    {
        if (!strcmp(s, "insert"))
            insert(x);
        if (!strcmp(s, "delete"))
            del(x);
        if (!strcmp(s, "exists"))
            puts(find(x) ? "true" : "false");
    }
    return 0;
}
