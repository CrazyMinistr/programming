#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define hashTableSize 3000019
using namespace std;
#define X first
#define Y second

int x;
string s, key, value;
struct list
{
	string key, value;
	list *next, *prevReq, *nextReq;
	list()
	{
		key = value = "";
		next = prevReq = nextReq = NULL;
	}
};

list *prevAdd = new list();
int hash(string s)
{
	unsigned long long h = 0;
	int n = s.size();
	for (int i = 0; i < n; i++)
		h = h * 29 + s[i];

	return h % hashTableSize;
}

struct dataHashTable
{
	list *head;
	dataHashTable(){ head = NULL; }

	list *find(string key)
	{
		list *p = head;
		while (p != NULL && p->key != key) 
			p = p->next;
	   	return p;
	}

	string get(string key)
	{
		list *p = find(key);
		return p == NULL ? "none" : p->value;
	}

	void insert(string key, string value)
	{
		list *p = find(key);
		if (p != NULL)
			p->value = value;
		else
		{
			list *p0 = new list();
			p0->next = head;
			p0->key = key;
			p0->value = value;
			p0->prevReq = prevAdd;
			prevAdd->nextReq = p0;
			prevAdd = p0;
			head = p0;
		}
	}

	void del(string key)
	{
		list *p0, *p;
		p = head, p0 = NULL;
		while (p != NULL && p->key != key)
			p0 = p, p = p->next;

		if (p == NULL) return;

		if (p->prevReq->key != "")
			p->prevReq->nextReq = p->nextReq;

		if (p->nextReq != NULL)
			p->nextReq->prevReq = p->prevReq;

		if (p->key == prevAdd->key)
			prevAdd = p->prevReq;

		if (p0 != NULL)
			p0->next = p->next;
		else
			head = p->next;

		delete p;
	}

	string prev(string key)
	{
		list *p = find(key);
		return (p == NULL || p->prevReq->key == "") ? "none" : p->prevReq->value;

	}

	string next(string key)
	{
		list *p = find(key);
		return (p == NULL || p->nextReq == NULL) ? "none" : p->nextReq->value;
	}

} HashTable[hashTableSize];

int main()
{
//*
	freopen("linkedmap.in", "r", stdin);
	freopen("linkedmap.out", "w", stdout);
//*/
	while (cin>> s>> key)
	{
		if (s == "put")
		{
			cin>> value;
			HashTable[hash(key)].insert(key, value);
		}
		if (s == "delete")
			HashTable[hash(key)].del(key);

		if (s == "get")
			cout<< HashTable[hash(key)].get(key)<< endl;

		if (s == "prev")
			cout<< HashTable[hash(key)].prev(key)<< endl;

		if (s == "next")
			cout<< HashTable[hash(key)].next(key)<< endl;
	}
	return 0;
}
