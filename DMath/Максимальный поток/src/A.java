import java.util.*;
import java.io.*;

public class A {
	FastScanner in;
	PrintWriter out;

	final int N = 220;
	int[] x = new int[N * N];
	int[] y = new int[N * N];
	int[] p = new int[N * N];
	int[] parent = new int[N];
	boolean[] used = new boolean[N];
	
	int ii = 0;
	
	void addedge(int a, int b)
	{
		y[++ii] = b;
		p[ii] = x[a];
		x[a] = ii;
	}
	
	boolean dfs(int v)
	{
		if (used[v])
			return false;
		used[v] = true;
		for (int q = x[v]; q > 0; q = p[q])
			if (parent[y[q]] == -1 || dfs(parent[y[q]]))
			{
				parent[y[q]] = v;
				return true;
			}
		return false;
	}
	
	public void solve() throws IOException {
		int n = in.nextInt(),
			m = in.nextInt(),
			k = in.nextInt();
		for (int i = 0; i < k; i++)
			addedge(in.nextInt(), in.nextInt());
		for (int i = 1; i <= m; i++)
			parent[i] = -1;
		
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= n; j++)
				used[j] = false;
			dfs(i);
		}
		int ans = 0;
		for (int i = 1; i <= m; i++)
			if (parent[i] != -1)
				++ans;
		out.println(ans);
	}

	public void run() {
		try {
			in = new FastScanner(new File("matching.in"));
			out = new PrintWriter(new File("matching.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new A().run();
	}
}
