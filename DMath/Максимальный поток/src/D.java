import java.util.*;
import java.io.*;

public class D {
	FastScanner in;
	PrintWriter out;

	final int N = 111111;
	int[] x = new int[N];
	int[] y = new int[N];
	int[] p = new int[N];
	int[] parent = new int[N];
	boolean[] used = new boolean[N];
	
	int ii = 0;
	
	void addedge(int a, int b)
	{
		y[++ii] = b;
		p[ii] = x[a];
		x[a] = ii;
	}
	
	boolean dfs(int v)
	{
		if (used[v])
			return false;
		used[v] = true;
		for (int q = x[v]; q > 0; q = p[q])
			if (parent[y[q]] == -1 || dfs(parent[y[q]]))
			{
				parent[y[q]] = v;
				return true;
			}
		return false;
	}
	
	public void solve() throws IOException {
		int n = in.nextInt(),
			m = in.nextInt();
		for (int i = 0; i < m; i++)
			addedge(in.nextInt(), in.nextInt());
		for (int i = 1; i <= n; i++)
			parent[i] = -1;
		
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= n; j++)
				used[j] = false;
			dfs(i);
		}
		int ans = 0;
		for (int i = 1; i <= n; i++)
			if (parent[i] != -1)
				++ans;
		out.println(n - ans);
	}

	public void run() {
		try {
			in = new FastScanner(new File("paths.in"));
			out = new PrintWriter(new File("paths.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new D().run();
	}
}
