import java.util.*;
import java.io.*;

public class E {
	FastScanner in;
	PrintWriter out;

	int cntEdges = 1;

	class Edge {
		int from, to, id;
		long capacity, flow;
		public Edge back;

		public Edge(int from, int to, long capacity, long flow) {
			super();
			this.from = from;
			this.id = -1;
			if (capacity > 0)
				this.id = cntEdges++;

			this.to = to;
			this.capacity = capacity;
			this.flow = flow;
		}
	}

	class Graph {

		ArrayList<Edge>[] graph;
		int[] d;
		int[] idx;
		int s, t, n;

		@SuppressWarnings("unchecked")
		public Graph(int n) {
			graph = new ArrayList[n];
			for (int i = 0; i < n; i++)
				graph[i] = new ArrayList<Edge>();

			this.n = n;
			this.idx = new int[this.n];
			this.s = 0;
			this.t = n - 1;
		}

		public void addEdge(int from, int to, long capacity) {
			Edge e1 = new Edge(from, to, capacity, 0);
			Edge e2 = new Edge(to, from, 0, 0);
			e1.back = e2;
			e2.back = e1;
			graph[from].add(e1);
			graph[to].add(e2);
		}

		private boolean bfs() {
			Queue<Integer> queue = new ArrayDeque<Integer>();
			this.d = new int[n];
			queue.add(s);
			Arrays.fill(this.d, Integer.MAX_VALUE);
			this.d[s] = 0;
			while (!queue.isEmpty() && this.d[t] == Integer.MAX_VALUE) {
				int cur = queue.poll();
				for (Edge e : graph[cur]) {
					if (this.d[e.to] == Integer.MAX_VALUE && e.flow < e.capacity) {
						this.d[e.to] = this.d[cur] + 1;
						queue.add(e.to);
					}
				}
			}
			if (this.d[t] == Integer.MAX_VALUE)
				return false;
			return true;
		}

		private long dfs(int cur, long flow) {
			if (flow == 0)
				return 0;
			if (cur == this.t)
				return flow;
			for (; idx[cur] < graph[cur].size(); idx[cur]++) {
				Edge e = graph[cur].get(idx[cur]);
				if (d[e.to] != d[cur] + 1)
					continue;
				long f = dfs(e.to, Math.min(flow, e.capacity - e.flow));
				if (f > 0) {
					e.flow += f;
					e.back.flow -= f;
					return f;
				}
			}
			return 0;
		}

		public long maxFlow() {
			long flow = 0;
			while (true) {
				if (!bfs())
					break;
				long f;
				Arrays.fill(idx, 0);
				do {
					f = dfs(s, Integer.MAX_VALUE);
					flow += f;
				} while (f > 0);
			}
			return flow;
		}

		long decomposition(int cur, ArrayList<Integer> ret, long flow, boolean[] used) {
			if (cur == t)
				return flow;
			used[cur] = true;
			for (Edge e : graph[cur]) {
				if (!used[e.to] && e.flow > 0) {
					ret.add(e.id);
					long f = decomposition(e.to, ret, Math.min(flow, e.flow), used);
					e.flow -= f;
					return f;
				}
			}
			return 0;
		}

		ArrayList<Long> flows;
		ArrayList<ArrayList<Integer>> findDecomposition(long flow) {
			long curFlow = 0;
			flows = new ArrayList<Long>();
			ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
			while (curFlow < flow) {
				ArrayList<Integer> cur = new ArrayList<Integer>();
				long f = decomposition(s, cur, Integer.MAX_VALUE, new boolean[n]);
				flows.add(f);
				res.add(cur);
				curFlow += f;
			}
			return res;
		}
	}

	public void solve() throws IOException {
		int n = in.nextInt(), m = in.nextInt();
		Graph g = new Graph(n);
		for (int i = 0; i < m; i++) {
			int from = in.nextInt() - 1, to = in.nextInt() - 1, capacity = in.nextInt();
			g.addEdge(from, to, capacity);
		}
		long maxFlow = g.maxFlow();
		ArrayList<ArrayList<Integer>> ans = g.findDecomposition(maxFlow);
		
		out.println(ans.size());
		for (int i = 0; i < ans.size(); i++) {
			ArrayList<Integer> a = ans.get(i);
			out.print(g.flows.get(i) + " " + a.size());
			for (int j = 0; j < a.size(); j++)
				out.print(" " + a.get(j));
			out.println();
		}
	}

	public void run() {
		try {
			in = new FastScanner(new File("decomposition.in"));
			out = new PrintWriter(new File("decomposition.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new E().run();
	}
}
