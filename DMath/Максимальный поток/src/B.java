import java.util.*;
import java.io.*;
 
public class B {
    FastScanner in;
    PrintWriter out;
 
     
    class Edge  {
        int from, to;
        long capacity, flow;
         
        Edge backEdge;
        public Edge(int from, int to, long capacity, long flow) {
            super();
            this.from = from;
            this.to = to;
            this.capacity = capacity;
            this.flow = flow;
        }
         
    }
     
    class Graph {
        ArrayList<Edge>[] g;
        int[] d, idx;
        int n, s, t;
        public Graph(int n) {
            g = new ArrayList[n];
            this.n = n;
            s = 0;
            t = n - 1;
            d = new int[n];
            idx = new int[n];
            for (int i = 0; i < n; i++) {
                g[i] = new ArrayList<Edge>();
            }
        }
         
        public void addEdge(int from ,int to, long capacity, long flow) {
            Edge e1 = new Edge(from, to, capacity, 0);
            Edge e2 = new Edge(to, from, 0, 0);
            e1.backEdge = e2;
            e2.backEdge = e1;
            g[from].add(e1);
            g[to].add(e2);
        }
         
        private boolean bfs() {
            Arrays.fill(d, Integer.MAX_VALUE);
            d[s] = 0;
            Queue<Integer> queue = new ArrayDeque<Integer>();
            queue.add(s);
            while (!queue.isEmpty() && d[t] == Integer.MAX_VALUE) {
                int cur = queue.poll();
                for (Edge e : g[cur]) {
                    if (d[e.to] == Integer.MAX_VALUE &&  e.flow < e.capacity) {
                        d[e.to] = d[cur] + 1;
                        queue.add(e.to);
                    }
                }
            }
            if (d[t] == Integer.MAX_VALUE) {
                return false;
            }
            return true;
        }
        private long dfs(int cur, long flow) {
             
            if (flow == 0) {
                return 0;
            }
            if (cur == t) {
                return flow;
            }
            for (; idx[cur] < g[cur].size(); idx[cur]++) {
                Edge e = g[cur].get(idx[cur]);
                if (d[e.to] != d[e.from] + 1) {
                    continue;
                }
                long f = dfs(e.to, Math.min(flow, e.capacity - e.flow));
                if (f > 0) {
                    e.flow += f;
                    e.backEdge.flow -= f;
                    return f;
                }
            }
            return 0;
        }
        public long maxFlow() {
            long flow = 0;
            while (true) {
                if (!bfs()) {
                    break;
                }
                long f;
                Arrays.fill(idx, 0);
                do {
                    f = dfs(s, Integer.MAX_VALUE);
                    flow += f;
                } while (f > 0);
            }
            return flow;
        }
    }
     
    public void solve() throws IOException {
        int n = in.nextInt(), m = in.nextInt();
        Graph g = new Graph(n);
        for (int i = 0; i < m; i++) { 
            int from = in.nextInt() - 1, to = in.nextInt() - 1;
            long capacity = in.nextInt();
            g.addEdge(from, to, capacity, 0);
        }
        out.println(g.maxFlow());
    }
 
    public void run() {
        try {
            in = new FastScanner(new File("maxflow.in"));
            out = new PrintWriter(new File("maxflow.out"));
 
            solve();
 
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    class FastScanner {
        BufferedReader br;
        StringTokenizer st;
 
        FastScanner(File f) {
            try {
                br = new BufferedReader(new FileReader(f));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
 
        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
 
        int nextInt() {
            return Integer.parseInt(next());
        }
    }
 
    public static void main(String[] arg) {
        new B().run();
    }
}
