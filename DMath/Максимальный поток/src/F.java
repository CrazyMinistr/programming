import java.util.*;
import java.io.*;

public class F {
	FastScanner in;
	PrintWriter out;
	int cntEdges = 0;

	class Edge {
		int from, to, capacity, flow, id;
		public Edge back;

		public Edge(int from, int to, int capacity, int flow) {
			super();
			this.from = from;
			this.to = to;
			this.capacity = capacity;
			this.flow = flow;
			this.id = -1;
		}
	}

	class Graph {

		ArrayList<Edge>[] graph;
		int[] d;
		int[] idx;
		int s, t, n;

		@SuppressWarnings("unchecked")
		public Graph(int n) {
			graph = new ArrayList[n];
			for (int i = 0; i < n; i++) {
				graph[i] = new ArrayList<Edge>();
			}
			this.n = n;
			this.idx = new int[this.n];
			this.s = 0;
			this.t = n - 1;
		}

		public void addEdge(int from, int to, int capacity) {
			Edge e1 = new Edge(from, to, capacity, 0);
			Edge e2 = new Edge(to, from, 0, 0);
			e1.back = e2;
			if (from != s && to != t)
				e1.id = cntEdges++;

			e2.back = e1;
			graph[from].add(e1);
			graph[to].add(e2);
		}

		private boolean bfs() {
			Queue<Integer> queue = new LinkedList<Integer>();
			this.d = new int[n];
			queue.add(s);
			Arrays.fill(this.d, -1);
			this.d[s] = 0;
			while (!queue.isEmpty() && this.d[t] == -1) {
				int cur = queue.poll();
				for (Edge e : graph[cur]) {
					if (this.d[e.to] == -1 && e.flow < e.capacity) {
						this.d[e.to] = this.d[cur] + 1;
						queue.add(e.to);
					}
				}
			}
			if (this.d[t] == -1) {
				return false;
			}
			return true;
		}

		private int dfs(int cur, int flow) {
			if (flow == 0) {
				return 0;
			}
			if (cur == this.t) {
				return flow;
			}
			for (; idx[cur] < graph[cur].size(); idx[cur]++) {
				Edge e = graph[cur].get(idx[cur]);
				if (d[e.to] != d[cur] + 1) {
					continue;
				}
				int f = dfs(e.to, Math.min(flow, e.capacity - e.flow));
				if (f > 0) {
					e.flow += f;
					e.back.flow -= f;
					return f;
				}
			}
			return 0;
		}

		public int maxFlow() {
			int flow = 0;
			while (true) {
				if (!bfs()) {
					break;
				}
				int f;
				Arrays.fill(idx, 0);
				do {
					f = dfs(s, Integer.MAX_VALUE);
					flow += f;
				} while (f > 0);
			}
			return flow;
		}

	}

	public void solve() throws IOException {
		int n = in.nextInt(), m = in.nextInt();
		Graph g = new Graph(n + 2);
		g.s = 0;
		g.t = 1;
		int[] minFlow = new int[m];
		for (int i = 0; i < m; i++) {
			int from = in.nextInt() + 1, to = in.nextInt() + 1, l = in.nextInt(), c = in.nextInt();
			minFlow[i] = l;
			g.addEdge(g.s, to, l);
			g.addEdge(from, to, c - l);
			g.addEdge(from, g.t, l);
		}
		g.maxFlow();
		for (Edge e : g.graph[0]) {
			if (e.capacity > e.flow) {
				out.println("NO");
				return;
			}
		}
		out.println("YES");
		int[] ans = new int[m];
		for (int i = 2; i < n + 2; i++) {
			for (Edge e : g.graph[i]) {

				if (e.id == -1)
					continue;
				ans[e.id] = e.flow + minFlow[e.id];
			}
		}
		for (int i = 0; i < m; i++)
			out.println(ans[i]);
	}

	public void run() {
		try {
			in = new FastScanner(new File("circulation.in"));
			out = new PrintWriter(new File("circulation.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new F().run();
	}
}
