#include <iostream>
#include <string.h>

using namespace std;

const int maxn = 100;
const int maxm = 1000;
const long long inf = 9223372036854775807LL;

struct gragh {
	int u, v, c, w, rev, next;
} g[maxm * 2];

long long d[maxn];
int first[maxn], q[maxm * 8], pre[maxn];
bool vis[maxn];
int tot, src, snk;

inline void addedge(int u, int v, int c, int w)
{
	g[++tot].u = u;    g[tot].v = v;    g[tot].c = c;    g[tot].rev = tot + 1;
	g[tot].w = w;    g[tot].next = first[u];    first[u] = tot;
	g[++tot].u = v;    g[tot].v = u;    g[tot].c = 0;    g[tot].rev = tot - 1;
	g[tot].w = -w;    g[tot].next = first[v];    first[v] = tot;
}

bool spfa(int src, int snk)
{
    int front = 0, rear = 1, u, v;
    for (int i = 0; i < maxn; i++)
    	d[i] = inf;

    memset(vis, false, sizeof(vis));
    q[1] = src;
    d[src] = 0;
    vis[src] = true;
    while (front < rear)
    {
        u = q[++front];
    	if (front + 1 == (maxm << 3))
    		front = 0;

        for(int i = first[u]; i; i = g[i].next)
        {
            if (g[i].c > 0)
            {
                v = g[i].v;
                if (d[u] < inf && d[u] + g[i].w < d[v])
                {
                    d[v] = d[u] + g[i].w;
                    pre[v] = i;
                    if (!vis[v])
                    {
                        q[++rear] = v;
                        vis[v] = true;
                        if (rear + 1 == (maxm << 3))
                        	rear = 0;
                    }
                }
            }
        }
        vis[u] = false;
    }
    return (d[snk] < inf);
}

void aug(int src, int snk)
{
    int i;
    long long flow, maxflow = 0, mincost = 0;
    memset(pre, 0, sizeof(pre));
    while (spfa(src, snk))
    {
        flow = inf;
        for (i = snk; i = pre[i]; i = g[i].u)
            flow = min(flow, (long long) g[i].c);

        for (i = snk; i = pre[i]; i = g[i].u)
        {
            g[i].c -= flow;
            g[g[i].rev].c += flow;
            mincost += g[i].w * flow;
        }
        maxflow += flow;
    }
    cout << mincost << endl;
}

int main()
{
	freopen("mincost.in", "r", stdin);
	freopen("mincost.out", "w", stdout);

    int n, m, s, t, i, u, v, c, w;
    cin >> n >> m;
    for (i = 1; i <= m; ++i)
    {
        cin >> u >> v >> c >> w;
        addedge(u - 1, v - 1, c, w);
    }
    s = 0, t = n - 1;
    aug(s, t);
    return 0;
}
