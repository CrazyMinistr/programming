#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long
#define mp make_pair
#define pb push_back

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }

using namespace std;
#define N 100111

int n, x;
vector<int> q1, q2;

struct T
{
	int id, pi, pj;
	T() {}
	T(int id, int pi, int pj)
	{
		this->id = id;
		this->pi = pi;
		this->pj = pj;
	}
} w[N], tw[N];

bool my(T a, T b)
{
	return min(a.pi, a.pj) < min(b.pi, b.pj);
}

int main()
{
//*
	freopen("f2cmax.in", "r", stdin);
	freopen("f2cmax.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 0; i < n; i++)
	{
		cin>> x;
		w[i] = T(i, x, 0);
	}
	for (int i = 0; i < n; i++)
		cin>> w[i].pj;

	memmove(tw, w, sizeof(w));
	sort(w, w + n, &my);
	for (int i = 0; i < n; i++)
		if (w[i].pi < w[i].pj)
			q1.push_back(w[i].id);
		else
			q2.push_back(w[i].id);
	reverse(q2.begin(), q2.end());
	LL t1 = 0, t2 = 0;
	memmove(w, tw, sizeof(tw));
	for (size_t i = 0; i < q1.size(); i++)
	{
		t1 += 1ll * w[q1[i]].pi;
		t2 = max(t2, t1);
		t2 += 1ll * w[q1[i]].pj;
	}
	for (size_t i = 0; i < q2.size(); i++)
	{
		t1 += 1ll * w[q2[i]].pi;
		t2 = max(t2, t1);
		t2 += 1ll * w[q2[i]].pj;
	}
	cout<< t2<< endl;
	for (size_t i = 0; i < q1.size(); i++) cout<< q1[i] + 1<< " ";
	for (size_t i = 0; i < q2.size(); i++) cout<< q2[i] + 1<< " ";
	puts("");
	for (size_t i = 0; i < q1.size(); i++) cout<< q1[i] + 1<< " ";
	for (size_t i = 0; i < q2.size(); i++) cout<< q2[i] + 1<< " ";
	puts("");
	return 0;
}
