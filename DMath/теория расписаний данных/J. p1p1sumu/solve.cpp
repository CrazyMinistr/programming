#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }

using namespace std;
#define N 30000003

int n, A, B, C, D, a[N], d[N], ans = 0, t = 0;

int main()
{
//*
	freopen("p1p1sumu.in", "r", stdin);
	freopen("p1p1sumu.out", "w", stdout);
//*/
	cin>> n>> d[0]>> d[1]>> A>> B>> C>> D;
	if (d[0] > n) ++ans; else a[d[0]]++;
	if (d[1] > n) ++ans; else a[d[1]]++;
	for (int i = 2; i < n; i++)
	{
		d[i] = (1ll * A * d[i - 2] + 1ll * B * d[i - 1] + 1ll * C) % D;
		if (d[i] > n)
			++ans;
		else
			a[d[i]]++;
	}
	for (int i = 1; i <= n; i++)
		if (a[i])
		{
			if (t >= i)
				continue;
			int x = i - t;
			if (x <= a[i])
			{
				ans += x;
				t = i;
			}
			else
			{
				t += a[i];
				ans += a[i];
			}
		}
	cout<< ans<< endl;
	return 0;
}
