#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }

using namespace std;

#define N 111111

bool used[N];
vector<int> edge[N], top_seq;
int n, m, ans = -2147483647, first_free = 0;
int d[N], td[N], sch[N], fin[N], ans_seq[N], p[N];

void top_sort(int v)
{
	used[v] = true;
	for (size_t i = 0; i < edge[v].size(); i++)
		if (!used[edge[v][i]])
			top_sort(edge[v][i]);

	top_seq.push_back(v);
}

bool my(int a, int b)
{
	return d[a] < d[b];
}

int main()
{
//*
	freopen("pintreep1l.in", "r", stdin);
	freopen("pintreep1l.out", "w", stdout);
//*/
	cin>> n>> m;
	for (int i = 0; i < n; i++)
		cin>> d[i], td[i] = d[i];

	for (int i = 1; i < n; i++)
	{
		int x, y;
		cin>> x>> y;
		edge[x - 1].push_back(y - 1);
	}
	for (int i = 0; i < n; i++)
		if (!used[i])
			top_sort(i);

	for (int i = 0; i < n; i++)
		for (size_t j = 0; j < edge[top_seq[i]].size(); j++)
			d[top_seq[i]] = min(d[top_seq[i]], d[edge[top_seq[i]][j]] - 1);

	for (int i = 0; i < n; i++)
		p[i] = i;

	sort(p, p + n, &my);
	for (int i = 0; i < n; i++)
	{
		int t = max(first_free, fin[p[i]]);
		ans_seq[p[i]] = t;
		ans = max(ans, t - td[p[i]] + 1);
		sch[t]++;
		if (sch[t] == m)
			first_free = t + 1;

		if (edge[p[i]].size() == 0)
			break;

		int j = edge[p[i]][0];
		fin[j] = max(fin[j], t + 1);
	}

	cout<< ans<< endl;
	for (int i = 0; i < n; i++)
		cout<< ans_seq[i]<< " ";
	return 0;
}
