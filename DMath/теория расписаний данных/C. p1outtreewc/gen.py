#!/usr/bin/python3

from random import *

seed(12423)
maxn = 50000
maxw = 1000
maxt = 1000

class Job:
    def __init__(self, jobId, parent, time, weight):
        self.jobId = jobId
        self.parent = parent
        self.time = time
        self.weight = weight

def shuffleJobs(jobs):
    ids = [i + 1 for i in range(len(jobs))]
    shuffle(ids)
    for i in range(len(jobs)):
        jobs[i].jobId = ids[i]
    return jobs

def genJobs(size, timeBound, weightBound):
    jobs = []

    for i in range(size):
        time = randint(1, timeBound)
        weight = randint(1, weightBound)
        if len(jobs) == 0:
            parent = None
        else:
            parent = choice(jobs)
        jobs.append(Job(i + 1, parent, time, weight))

    return shuffleJobs(jobs)

def genChain(size):
    jobs = []

    for i in range(size):
        time = randint(1, maxt)
        weight = randint(1, maxw)
        if len(jobs) == 0:
            parent = None
        else:
            parent = jobs[i - 1]
        jobs.append(Job(i + 1, parent, time, weight))

    return shuffleJobs(jobs)

def genStar(size):
    jobs = []

    for i in range(size):
        time = randint(1, maxt)
        weight = randint(1, maxw)
        if (len(jobs) == 0):
            parent = None
        else:
            parent = jobs[0]
        jobs.append(Job(i + 1, parent, time, weight))

    return shuffleJobs(jobs)

def genSingle():
    return [Job(1, None, randint(1, maxw), randint(1, maxt))]

def genOverflow(size):
    jobs = genJobs(size, maxt, maxw)
    for job in jobs:
        job.weight = maxw
        job.time = maxt
    return jobs

def genMultipleAnswer(size):
    jobs = genJobs(size, maxt, maxw)
    w1 = t2 = randint(1, min(maxw, maxt))
    w2 = t1 = randint(1, min(maxw, maxt))
    for job in jobs[:len(jobs) // 2]:
        job.weight = w1
        job.time = t1
    
    for job in jobs[len(jobs) // 2:]:
        job.weight = w2
        job.time = t2

    return jobs


def makeTest(jobs):
    count = len(jobs)
    weights = [job.weight for job in jobs]
    times = [job.time for job in jobs]
    prec = [[job.jobId, job.parent.jobId] for job in filter(lambda j: j.parent != None, jobs)]
    shuffle(prec)
    return count, times, weights, prec

def writeTest(testFile, testData):
    count, times, weights, precedence = testData
    testFile.write("{}\n".format(count))
    testFile.write("{}\n".format(" ".join([str(t) for t in times])))
    testFile.write("{}\n".format(" ".join([str(w) for w in weights])))
    for edge in precedence:
        testFile.write("{} {}\n".format(edge[0], edge[1]))

userCount = 4 # increased by one because of 1-indexing
smallCount = 10 # n <= 10
mediumCount = 10 # 10 <= n <= 100
miscCount = 5
bigCount = 10 # 100 <= n <= 50000

files = [open("{:02d}".format(i), "w") for i in range(userCount, userCount + smallCount + mediumCount + miscCount + bigCount)]

small = files[0:smallCount]
medium = files[smallCount: smallCount + mediumCount]
misc = files[smallCount + mediumCount: smallCount + mediumCount + miscCount]
big = files[smallCount + mediumCount + miscCount: smallCount + mediumCount + miscCount + bigCount]


#print(len(small))
#print(len(medium))
#print(len(big))


for testFile in small:
    writeTest(testFile, makeTest(genJobs(randint(1, 8), maxt // 100, maxw // 100)))

for testFile in medium:
    writeTest(testFile, makeTest(genJobs(randint(10, 100), maxt // 10, maxw // 10)))

#print(userCount + smallCount + mediumCount)

writeTest(misc[0], makeTest(genSingle()))
writeTest(misc[1], makeTest(genChain(maxn)))
writeTest(misc[2], makeTest(genStar(maxn)))
writeTest(misc[3], makeTest(genOverflow(maxn)))
writeTest(misc[4], makeTest(genMultipleAnswer(maxn)))

for testFile in big:
    writeTest(testFile, makeTest(genJobs(randint(maxn / 2, maxn), maxt, maxw)))

for testFile in files:
    testFile.close()
