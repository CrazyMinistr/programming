#include <cstdio>
#include <vector>
#include <algorithm>
#include <iostream>
#include <queue>
using namespace std;

long long LONG_INF = 9223372036854775807;
int root = 0;

struct dsu
{
   struct node
   {
      int next;
      int id;
      int value;

      int next_n;
      int last_n;

      node()
      {
         next_n = 0;
      };

      node(int next, int id, int value)
      {
         this->next = next;
         this->id = id;
         this->value = value;
         next_n = value;
         last_n = value;
      }
   };

   node* arr;
   dsu(int n)
   {
      arr = new node[n];
      for(int i = 0; i < n; i++)
         arr[i] = node(i, i, i);
   }


   void link(node& x, node& y)
   {
      arr[x.last_n].next_n = y.id;
      x.last_n = y.last_n;
      x.next = y.value;
      y.id = x.id;
   }

   int find_set(node x)
   {
      if(x.value != x.next)
         arr[x.value].next = find_set(arr[x.next]);
      return arr[x.value].next;
   }

   void merge(node x, node y)
   {
      if(find_set(x) != find_set(y))
         link(arr[find_set(x)], arr[find_set(y)]);
   }
};

struct q
{
   long long w, p;
   int id;

   q() {};

   q(int w, int p, int id)
   {
      this->w = w;
      this->p = p;
      this->id = id;
   };
};

struct cmp
{
   bool operator() (q& a, q& b)
   {
      return a.w * b.p < b.w * a.p;
   }
};


int main()
{
   freopen("p1outtreewc.in", "r", stdin);
   freopen("p1outtreewc.out", "w", stdout);
   int n;
   cin >> n;
   vector<bool> isValid(n, true);
   // int invalidCounter = 0;

      vector<int> p(n);
      vector<int> w(n);

      vector<int> pi(n);
      vector<long long> wi(n);

      for(int i = 0; i < n; i++)
      {
         cin >> p[i];
         pi[i] = p[i];
      }

      for(int i = 0; i < n; i++)
      {
         cin >> w[i];
         wi[i] = w[i];
      }

      vector<int> pred(n, -1);

      for(int i = 0; i < n - 1; i++)
      {
         int u, v;
         cin >> u >> v;
         pred[u - 1] = v - 1;
      }

      for(int i = 0; i < n; i++)
      {
         if(pred[i] == -1)
         {
            root = i;
         }
      }

      w[root] = -LONG_INF;
      isValid[root] = false;
      vector<int> E(n);
      dsu J(n);
      priority_queue<q, vector<q>, cmp> que;
      for(int i = 0; i < n; i++)
      {
         E[i] = i;
         que.push(q(w[i], p[i], i));
      }

      for(int s = 0; s < n - 1; s++)
      {
         q qu = que.top();
         que.pop();
         while(!isValid[qu.id])
         {
            qu = que.top();
            que.pop();
         }
         isValid[qu.id] = false;
         int f = pred[qu.id];
         int j = qu.id;
         int i = J.arr[J.find_set(J.arr[f])].id;
         w[i] += w[j];
         p[i] += p[j];
         qu = q(w[i], p[i], i);
         que.push(qu);
         pred[j] = E[i];
         E[i] = E[j];
         J.merge(J.arr[i], J.arr[j]);
      }

      vector<long long> ans(n);
      long long res = 0;
      long long t = 0;
      int j = root;
      for(int i = 0; i < n; i++)
      {
         ans[J.arr[j].value] = t;
         t += pi[J.arr[j].value];
         res += t * wi[J.arr[j].value];
         j = J.arr[j].next_n;
      }

      cout << res << '\n';

      for(int i = 0; i < n; i++)
         cout << ans[i] << " ";
      return 0;
}
