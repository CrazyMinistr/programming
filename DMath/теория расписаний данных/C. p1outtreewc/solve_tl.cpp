
#include <set>
#include <map>
#include <ctime>
#include <queue>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>
 
using namespace std;
 
#define N 50055
#define inf (int) 1
struct T
{
    int parent;
    long long p, w;
    vector<int> child, sch;
} tree[N];
 
vector<int> q;
int p[N], w[N];
long long res[N];
 
bool my_compare(int i, int j)
{
    return tree[i].w * tree[j].p < tree[i].p * tree[j].w;
}
 
int main()
{
    freopen("p1outtreewc.in", "r", stdin);
    // freopen("p1outtreewc.out", "w", stdout);
    int n;
    cin>> n;
    for (int i = 0; i < n; i++)
    {
        cin>> tree[i].p;
        p[i] = tree[i].p;
        tree[i].parent = n;
        q.push_back(i);
    }
    for (int i = 0; i < n; i++)
    {
        cin>> tree[i].w;
        w[i] = tree[i].w;
        tree[i].sch.push_back(i);
    }
    for (int i = 1; i < n; i++)
    {
        int u, v;
        cin>> u>> v;
        --u, --v;
        tree[u].parent = v;
        tree[v].child.push_back(u);
    }
    int root;
    for (int i = 0; i < n; i++)
        if (tree[i].parent == n)
            root = i;
 
    tree[root].w = -inf;
    // while (q.size() > 1)
    for (int iter = 1; iter < n; iter++)
    {
        make_heap(q.begin(), q.end(), &my_compare);
        int ps = q.front();

        printf("iter %d: ps = %d parent = %d p = %lld w = %lld\n", iter, ps, tree[ps].parent, tree[ps].p, tree[ps].w);

        for (size_t i = 0; i < tree[ps].child.size(); i++)
        {
            int e = tree[ps].child[i];
            tree[e].parent = tree[ps].parent;
            tree[tree[ps].parent].child.push_back(e);
        }
        tree[ps].child.clear();
        tree[tree[ps].parent].p += tree[ps].p;
        tree[tree[ps].parent].w += tree[ps].w;
        tree[root].w = -inf;
 
        for (size_t i = 0; i < tree[ps].sch.size(); i++)
            tree[tree[ps].parent].sch.push_back(tree[ps].sch[i]);
 
        tree[ps].sch.clear();
        pop_heap(q.begin(), q.end(), &my_compare);
        q.pop_back();
    }
    long long ans = 0, t = 0;
    for (int i = 0; i < n; i++)
    {
        int ps = tree[root].sch[i];
        res[ps] = t;
        t += 1ll * p[ps];
        ans += t * 1ll * w[ps];
    }
    cout<< ans<< endl;
    for (int i = 0; i < n; i++)
        cout<< res[i]<< " ";
    puts("");
    return 0;
}
