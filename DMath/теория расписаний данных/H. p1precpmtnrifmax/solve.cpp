#include <iostream>
#include <fstream>
#include <vector>
#include <cassert>
#include <set>
#include <algorithm>

using namespace std;

typedef long long ll;

static const ll INF = 1e18;

ll globalAns = 0;

vector<ll> r, p, a, b, c;
vector<vector<pair<ll, ll> > > times;
vector<int> jobs;

ll calc(int i, ll x)
{
    return a[i] * x * x + b[i] * x + c[i];
}

void append(int i, pair<ll, ll> p)
{
    if (p.first != p.second)
        times[i].push_back(p);
}

bool cmp(int i, int j)
{
    return r[i] < r[j];
}

vector<pair<ll, ll> > solve(vector<int>);
vector<vector<bool> > g;

vector<vector<int> > decompose(vector<int> v)
{
    sort(v.begin(), v.end(), cmp);
    vector<vector<int> > res;
    ll time = -1;
    for (size_t i = 0; i < v.size(); i++)
    {
        int now = v[i];
        if (r[now] <= time)
            time += p[now];
        else
        {
            res.push_back(vector<int>());
            time = r[now] + p[now];
        }
        res.back().push_back(now);
    }
    return res;
}

pair<ll, ll> block(vector<int> v)
{
    if (v.size() == 1)
    {
        pair<ll, ll> s(r[v[0]], r[v[0]] + p[v[0]]);
        append (v[0], s);
        globalAns = max(globalAns, calc(v[0], s.second));
        return s;
    }

    ll start = INF;
    for (size_t i = 0; i < v.size(); i++)
        start = min(start, r[v[i]]);
    ll finish = start;
    for (size_t i = 0; i < v.size(); i++)
        finish += p[v[i]];

    int good = -1;
    ll best = INF;
    for (size_t i = 0; i < v.size(); i++)
    {
        int from = v[i];
        bool ok = true;
        for (size_t j = 0; j < v.size(); j++)
        {
            int to = v[j];
            if (g[from][to])
            {
                ok = false;
                break;
            }
        }
        if (!ok)
            continue;
        ll now = calc(from, finish);
        if (good == -1 || now < best)
        {
            good = v[i];
            best = now;
        }
    }

    globalAns = max(globalAns, best);

    vector<int> newJobs;
    for (size_t i = 0; i < v.size(); i++)
    {
        int now = v[i];
        if (now == good)
            continue;
        newJobs.push_back(now);
    }

    vector<pair<ll, ll> > res = solve(newJobs);
    ll left = start;

    for (size_t i = 0; i < res.size(); i++)
    {
        pair<ll, ll> now(left, res[i].first);
        append(good, now);
        left = res[i].second;
    }
    append(good, make_pair(left, finish));
    return make_pair(start, finish);
}

vector<pair<ll, ll> > solve(vector<int> jobs)
{
    if (jobs.size() == 0)
        return vector<pair<ll, ll> >();
    vector<vector<int> > a = decompose(jobs);
    vector<pair<ll, ll> > res;
    for (size_t i = 0; i < a.size(); i++)
        res.push_back(block(a[i]));
    return res;
}

vector<size_t> order;
vector<bool> used;

void dfs(size_t v)
{
    if (used[v])
        return;
    used[v] = true;
    for (size_t i = 0; i < used.size(); i++)
        if (g[v][i])
            dfs(i);
    order.push_back(v);
}

int main()
{
    ifstream in("p1precpmtnrifmax.in");
    ofstream out("p1precpmtnrifmax.out");
    int n;
    in >> n;

    times = vector<vector<pair<ll, ll> > > (n);
    p = vector<ll>(n);
    for (size_t i = 0; i < n; i++)
        in >> p[i];
    r = vector<ll> (n);
    for (size_t i = 0; i < n; i++)
        in >> r[i];
    g = vector<vector<bool> >(n, vector<bool>(n, false));
    int m;
    in >> m;
    for (int i = 0; i < m; i++)
    {
        int u, v;
        in >> u >> v;
        g[u - 1][v - 1] = true;
    }
    a = vector<ll>(n);
    b = vector<ll>(n);
    c = vector<ll>(n);
    for (int i = 0; i < n; i++)
    {
        in >> a[i] >> b[i] >> c[i];
    }

    used = vector<bool>(n, false);
    for (size_t i = 0; i < n; i++)
        if (!used[i])
            dfs(i);
    reverse(order.begin(), order.end());

    for (size_t i = 0; i < n; i++)
    {
        int from = order[i];
        for (size_t j = 0; j < n; j++)
            if (g[from][j])
                r[j] = max(r[from] + p[from], r[j]);
    }

    jobs = vector<int>(n);
    for (size_t i = 0; i < n; i++)
        jobs[i] = i;
    solve(jobs);
    out << globalAns << "\n";
    for (int i = 0; i < n; i++)
    {
        out << times[i].size() << " ";
        for (int j = 0; j < times[i].size(); j++)
        {
            pair<ll, ll> now = times[i][j];
            out << now.first << " " << now.second << " ";
        }
        puts("");
    }
}
    