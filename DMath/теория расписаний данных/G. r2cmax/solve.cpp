#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }

using namespace std;

#define N 1111
#define inf 100010

int a[N], b[N], d[2][inf];

int main()
{
//*
	freopen("r2cmax.in", "r", stdin);
	freopen("r2cmax.out", "w", stdout);
//*/
	int n, sum = 0;
	cin>> n;
	for (int i = 0; i < n; i++)
	{
		cin>> a[i];
		sum += a[i];
	}
	for (int i = 0; i < n; i++)
		cin>> b[i];

	for (int i = 0; i < inf; i++)
		d[0][i] = inf;

	d[0][0] = 0;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < inf; j++)
			d[1][j] = inf;

		for (int j = 0; j <= sum; j++)
			if (j - a[i] >= 0)
				d[1][j] = min(d[0][j - a[i]], d[0][j] + b[i]);
			else
				d[1][j] = min(inf, d[0][j] + b[i]);

		for (int j = 0; j < inf; j++)
			d[0][j] = d[1][j];
	}

	int ans = inf;
	for (int i = 0; i <= sum; i++)
		ans = min(ans, max(i, d[0][i]));

	cout<< ans<< endl;
	return 0;
}
