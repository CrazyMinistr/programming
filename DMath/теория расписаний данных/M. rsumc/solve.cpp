#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits.h>
 
using namespace std;
 
struct edge
{
	int u, v;
	long long f, w;
	edge* inv;
	edge() : u(0), v(0), f(0), w(0), inv(NULL) {}
	edge(int u_, int v_, int f_, int w_) : u(u_), v(v_), f(f_), w(w_) {}
};
 
const long long inf = INT_MAX;
int nn, mm;
int p[44][44], schedule[44][44];
 
long long max_flow_min_cost()
{
	// 0 - исток
	// 1..n - работы
	// n + 1..n + (n+1)*(m+1) - станки
	// (m + 1)*n + 2 - сток
	vector<int> jobs;
	vector<edge> edges = vector<edge>();
	int n, m;
	n = (mm + 2) * (nn + 1) + 2;
	for (int i = 1; i <= nn; i++)
	{
		edges.push_back(edge(0, i, 1, 0));
		edges.push_back(edge(i, 0, 0, 0));
	}
	for (int i = 1; i <= nn; i++)
		for (int j = 1; j <= mm; j++)
			for (int k = 1; k <= nn; k++)
			{
				edges.push_back(edge(i, nn + 1 + (j - 1) * nn + k - 1, 1, k * p[i][j]));
				jobs.push_back(edges.size() - 1);
				edges.push_back(edge(nn + 1 + (j - 1) * nn + k - 1, i, 0, -k * p[i][j]));
			}

	for (int j = 1; j <= mm; j++)
		for (int k = 1; k <= nn; k++)
		{
			edges.push_back(edge(nn + 1 + (j - 1) * nn + k - 1, n - 1, 1, 0));
			edges.push_back(edge(n - 1, nn + 1 + (j - 1) * nn + k - 1, 0, 0));
		}

	m = edges.size();
	for (int i = 0; i < m; i++)
		if (i % 2 == 0)
			edges[i].inv = &edges[i + 1];
		else
			edges[i].inv = &edges[i - 1];

	long long result = 0;
	bool f;
	while (true)
	{
		vector<long long> d(n, inf);
		d[0] = 0;
		vector<int> prev(n, -1);
		vector<int> e(n);
		for (int i = 0; i < n; i++)
		{
			f = true;
			for (size_t j = 0; j < edges.size(); j++)
			{
				if (edges[j].f > 0 && d[edges[j].v] > d[edges[j].u] + edges[j].w)
				{
					f = false;
					d[edges[j].v] = d[edges[j].u] + edges[j].w;
					prev[edges[j].v] = edges[j].u;
					e[edges[j].v] = j;
				}
			}
			if (f)
				break;
		}
		if (d[n - 1] >= inf)
			break;
		int p = n - 1;
		long long flow = inf;
		while (prev[p] != -1)
		{
			flow = min(flow, edges[e[p]].f);
			p = prev[p];
		}
		result += d[n - 1] * flow;
		p = n - 1;
		while (prev[p] != -1)
		{
			edges[e[p]].f -= flow;
			edges[e[p]].inv -> f += flow;
			p = prev[p];
		}
	}
	for (size_t i = 0; i < jobs.size(); i++)
	{
		if (edges[jobs[i]].f > 0)
			continue;

		int x = edges[jobs[i]].u;
		int y = edges[jobs[i]].v;
		y -= (nn + 1);
		schedule[y / nn][nn - (y % nn) - 1] = x;
	}
	return result;
}
 
int main()
{
	freopen("rsumc.in", "r", stdin);
	freopen("rsumc.out", "w", stdout);
	cin>> nn>> mm;
	for (int i = 1; i <= nn; i++)
		for (int j = 1; j <= mm; j++)
			cin>> p[i][j];

	cout<< max_flow_min_cost()<< endl;
	for (int i = 0; i < mm; i++)
	{
		int k = 0;
		for (int j = 0; j < nn; j++)
			if (schedule[i][j])
				++k;

		cout<< k;
		for (int j = nn - k; j < nn; j++)
			cout<< " "<< schedule[i][j];
		puts("");
	}
	return 0;
}
