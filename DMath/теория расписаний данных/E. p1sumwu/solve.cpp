#include <iostream>
#include <cstdio>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

struct T
{
	int deadline, cost, id;
	T() {}
	T(int deadline, int cost, int id)
	{
		this->deadline = deadline;
		this->cost = cost;
		this->id = id;
	}

	bool operator<(const T& x) const
	{
		return x.cost < cost;
	}
};

bool my(T a, T b)
{
	return a.deadline < b.deadline;
}

priority_queue<T> heap;

int main()
{
	freopen("p1sumwu.in", "r", stdin);
	freopen("p1sumwu.out", "w", stdout);
	int n;
	cin>> n;
	vector<T> a;
	a.clear();
	for (int i = 0; i < n; i++)
	{
		int d, w;
		cin>> d>> w;
		a.push_back(T(d, w, i));
	}
	int cnt = 0;
	long long res;
	sort(a.begin(), a.end(), &my);
	for (int i = 0; i < n; i++)
	{
		++cnt;
		heap.push(a[i]);
		if (cnt > a[i].deadline)
		{
			--cnt;
			res += heap.top().cost;
			heap.pop();
		}
	}
	vector<T> b;
	b.clear();
	while (!heap.empty())
	{
		b.push_back(heap.top());
		heap.pop();
	}
	sort(b.begin(), b.end(), &my);
	cnt = 0;
	vector<long long> ans(n, -1);
	for (size_t i = 0; i < b.size(); i++)
		ans[b[i].id] = cnt++;
	for (int i = 0; i < n; i++)
		if (ans[i] == -1)
			ans[i] = cnt++;
	cout<< res<< endl;
	for (int i = 0; i < n; i++)
		cout<< ans[i]<< " ";
	puts("");
	return 0;
}
