#include <set>
#include <map>
#include <ctime>
#include <queue>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

#define LL long long

template <typename T> T sqr(T x) { return x * x; }
template <typename T> T abs(T x) { return x < 0 ? -x : x; }

using namespace std;

#define N 111111
struct T
{
	int p, d, id;
	T() {}
	T(int p, int d, int id)
	{
		this->p = p;
		this->d = d;
		this->id = id;
	}

	bool operator <(const T& x) const
	{
		return p < x.p;
	}
} a[N];

int n;
LL ans[N];
priority_queue<T> heap;

bool my(T a, T b)
{
	return a.d < b.d;
}

int main()
{
//*
	freopen("p1sumu.in", "r", stdin);
	freopen("p1sumu.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 0; i < n; i++)
	{
		int x, y;
		cin>> x>> y;
		a[i] = T(x, y, i);
	}
	sort(a, a + n, &my);

	LL t = 0;
	for (int i = 0; i < n; i++)
		ans[i] = -1;

	for (int i = 0; i < n; i++)
	{
		T x = a[i];
		heap.push(x);
		ans[x.id] = 1;
		t += x.p;
		if (t > x.d)
		{
			T tmp = heap.top();
			heap.pop();
			ans[tmp.id] = -1;
			t -= tmp.p;
		}
	}
	t = 0;
	int res = 0;
	for (int i = 0; i < n; i++)
		if (ans[a[i].id] == 1)
		{
			++res;
			ans[a[i].id] = t;
			t += a[i].p;
		}
	cout<< res<< endl;
	for (int i = 0; i < n; i++)
		cout<< ans[i]<< " ";
	return 0;
}
