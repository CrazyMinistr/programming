#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

using namespace std;
 
struct T
{
    int t, num;
    bool f;
    T() { f = true; t = num = 0; }
} slow;

#define N 222222
int n;
long long m = 0;
long long Cmax = 0, sumF = 0, sumS = 0;
vector<int> a(N), b(N);
vector<int> I, J;
vector<long long> ansF(N);
vector<long long> ansS(N);

int main()
{
    freopen("o2cmax.in", "r", stdin);
    freopen("o2cmax.out", "w", stdout);
    long long t = 0;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> a[i];
        sumF += a[i];
    }
    for (int i = 0; i < n; ++i)
    {
        cin >> b[i];
        sumS += b[i];
        if (m < a[i] + b[i])
            m = a[i] + b[i];
        if (a[i] <= b[i])
            I.push_back(i);
        else
            J.push_back(i);
    }
    for (size_t i = 0; i < I.size(); ++i)
        if (slow.t < a[I[i]])
        {
            slow.num = I[i];
            slow.t = a[I[i]];
        }
    for (size_t i = 0; i < J.size(); ++i)
        if (slow.t < b[J[i]])
        {
            slow.num = J[i];
            slow.t = b[J[i]];
            slow.f = false;
        }
    Cmax = max(max(sumF, sumS), m);
    cout<< Cmax<< endl;
    if (slow.f)
    {
        ansS[slow.num] = 0;
        t = b[slow.num];
        for (size_t j = 0; j < I.size(); ++j)
            if (slow.num != I[j])
            {
                ansS[I[j]] = t;
                t += b[I[j]];
            }
        t = Cmax;
        for (size_t j = 0; j < J.size(); ++j)
        {
            t -= b[J[j]];
            ansS[J[j]] = t;
        }
        t = 0;
        for (size_t j = 0; j < I.size(); ++j)
            if (slow.num != I[j])
            {
                ansF[I[j]] = t;
                t += a[I[j]];
            }
        t = Cmax - a[slow.num];
        ansF[slow.num] = t;
        for (size_t j = 0; j < J.size(); ++j)
        {
            t -= a[J[j]];
            ansF[J[j]] = t;
        }
    }
    else
    {
        ansF[slow.num] = 0;
        t = a[slow.num];
        for (size_t j = 0; j < J.size(); ++j)
            if (slow.num != J[j])
            {
                ansF[J[j]] = t;
                t += a[J[j]];
            }
        t = Cmax;
        for (size_t j = 0; j < I.size(); ++j)
        {
            t -= a[I[j]];
            ansF[I[j]] = t;
        }
        t = 0;
        for (size_t j = 0; j < J.size(); ++j)
            if (slow.num != J[j])
            {
                ansS[J[j]] = t;
                t += b[J[j]];
            }
        t = Cmax - b[slow.num];
        ansS[slow.num] = t;
        for (size_t j = 0; j < I.size(); ++j)
        {
            t -= b[I[j]];
            ansS[I[j]] = t;
        }
    }
    for (int i = 0; i < n; ++i)
        cout << ansF[i]<< " ";
    puts("");
    for (int i = 0; i < n; ++i)
        cout << ansS[i]<< " ";
    puts("");
    return 0;
}

