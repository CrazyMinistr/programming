import java.util.*;
import java.io.*;
import java.math.BigInteger;

public class F {
    FastScanner in;
    PrintWriter out;

    final int N = 1111;
    int[] p = new int[N];
    int[] sum = new int[N];
    LinkedList<BigInteger>[] pol;
    BigInteger fmax;

    BigInteger f(int id, BigInteger t)
    {
    	BigInteger res = BigInteger.ZERO;
    	BigInteger x = BigInteger.ONE;
    	for (BigInteger i : pol[id])
    	{
    		res = res.add(x.multiply(i));
    		x = x.multiply(t);
    	}
    	return res;
    }

    public void solve() throws IOException {
    	int n = in.nextInt();
    	for (int i = 0; i < n; i++)
    		p[i] = in.nextInt();
    	pol = new LinkedList[n];
    	for (int i = 0; i < n; i++)
    	{
    		int m = in.nextInt();
    		pol[i] = new LinkedList<BigInteger>();
    		for (int j = 0; j <= m; j++)
    			pol[i].addFirst(new BigInteger(Integer.toString(in.nextInt())));
    	}
    	int e = in.nextInt();
    	int[][] ms = new int[n][n];
    	for (int i = 0; i < e; i++)
    	{
    		int a = in.nextInt() - 1;
    		int b = in.nextInt() - 1;
    		ms[a][b] = 1;
    		++sum[a];
    	}
    	fmax = BigInteger.ZERO;
    	int t = 0;
    	for (int i = 0; i < n; i++)
    		t += p[i];
    	LinkedList<Integer> q = new LinkedList<Integer>();
    	for (int i = n - 1; i >= 0; i--)
    	{
    		int minj = -1;
    		BigInteger minF = BigInteger.TEN.pow(100);
    		for (int j = 0; j < n; j++)
    			if (sum[j] == 0)
    			{
    				BigInteger newF = f(j, new BigInteger(Integer.toString(t)));
    				if (newF.compareTo(minF) == -1)
    				{
    					minF = newF;
    					minj = j;
    				}
    			}

    		fmax = fmax.max(f(minj, new BigInteger(Integer.toString(t))));
    		t -= p[minj];
    		q.addFirst(minj);
    		sum[minj] = Integer.MAX_VALUE;
    		for (int j = 0; j < n; j++)
    			if (ms[j][minj] > 0)
    				--sum[j];
    	}
    	int[] ans = new int[n];
    	for (int i : q)
    	{
    		ans[i] = t;
    		t += p[i];
    	}
    	out.println(fmax.toString());
    	for (int i = 0; i < n; i++)
    		out.print(ans[i] + " ");
    }

    public void run() {
        try {
            in = new FastScanner(new File("p1precfmax.in"));
            out = new PrintWriter(new File("p1precfmax.out"));

            solve();

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class FastScanner {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(File f) {
            try {
                br = new BufferedReader(new FileReader(f));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] arg) {
        new F().run();
    }
}
