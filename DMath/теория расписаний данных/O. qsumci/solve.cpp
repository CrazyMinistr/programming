#include <set>
#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
#include <cstdlib>
#include <cstddef>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>

using namespace std;

#define M 11000
#define N 50050
struct T
{
	int id, t, num;
	T() {}
	T(int id, int t)
	{
		this->id = id;
		this->t = t;
		num = 1;
	}

	bool operator < (const T &x) const
	{
		if (num * t < x.num * x.t)
			return true;
		else if (num * t > x.num * x.t)
			return false;
		else
			return id < x.id;
		// return (num * t < x.num * x.t); // 1 / x < 1 / y
	}
};

bool my_compare(pair<int, int> a, pair<int, int> b)
{
	return a.first > b.first;
}

multiset<T> s;
vector<int> sch[M];
vector<pair<int, int> > a;
int ansM[N], t[N];
long long ansT[N];

int main()
{
	freopen("qsumci.in", "r", stdin);
	freopen("qsumci.out", "w", stdout);
	int n, m;
	cin>> n>> m;
	for (int i = 0; i < n; i++)
	{
		int x;
		cin>> x;
		a.push_back(make_pair(x, i));
	}
	sort(a.begin(), a.end(), &my_compare);
	for (int i = 0; i < m; i++)
	{
		cin>> t[i];
		s.insert(T(i, (int) t[i]));
	}
	for (int i = 0; i < n; i++)
	{
		T x = (*s.begin());
		s.erase(s.begin());
		sch[x.id].push_back(i);
		++x.num;
		s.insert(x);
	}
	int numM = 0;
	long long ansCi = 0;
	for (int i = 0; i < m; i++)
	{
		// cout<< sch[i].size()<< endl;
		// if (sch[i].size() == 0)
		// 	continue;

		reverse(sch[i].begin(), sch[i].end());
		long long sumCi = 0, Ci = 0;
		for (size_t j = 0; j < sch[i].size(); j++)
		{
			ansM[a[sch[i][j]].second] = numM;
			ansT[a[sch[i][j]].second] = Ci;
			Ci += a[sch[i][j]].first * 1ll * t[numM];
			sumCi += Ci;
		}
		++numM, ansCi += sumCi;
	}
	cout<< ansCi<< endl;
	for (int i = 0; i < n; i++)
		cout<< ansM[i] + 1<< " "<< ansT[i]<< endl;
	return 0;
}
