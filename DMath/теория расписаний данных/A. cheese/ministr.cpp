#include <set>
#include <cmath>
#include <cstdio>
#include <vector>
#include <memory.h>
#include <iostream>
#include <algorithm>
using namespace std;

struct edge
{
	int to, r;
	double f, c;
	edge() {}
	edge(int to, double c, int r)
	{
		this->to = to;
		this->c = c;
		this->r = r;
		this->f = 0;
	}
};

int nodes, S, T, n, m, p[33], r[33], d[33], s[33];
vector<edge> x[2000];
bool u[2000];

bool dfs(int v, double d)
{
	if (v == T)
		return true;
	u[v] = true;
	for (size_t i = 0; i < x[v].size(); i++)
	{
		edge &t = x[v][i];
		if (t.f + d <= t.c && !u[t.to] && dfs(t.to, d))
		{
			t.f += d, x[t.to][t.r].f -= d;
			return true;
		}
	}
	return false;
}

double max_flow()
{
	double cu = 1 << 20, sum = 0;
	for (int i = 0; i < 40; i++, cu *= 0.5)
	while (true)
	{
		memset(u, 0, sizeof(u));
		if (!dfs(S, cu))
			break;
		sum += cu;
	}
	return sum;
}

int new_node()
{
	x[nodes].clear();
	return nodes++;
}

void addedge(int a, int b, double c)
{
	x[a].push_back(edge(b, c, x[b].size()));
	x[b].push_back(edge(a, 0, x[a].size() - 1));
}

int main()
{
	// freopen("cheese.in", "r", stdin);
	// freopen("cheese.out", "w", stdout);
	cin>> n>> m;
	for (int i = 0; i < n; i++)
	{
		cin>> p[i]>> r[i]>> d[i];
		sum_p += p[i];
	}

	for (int i = 0; i < m; i++)
		cin>> s[i];

	sort(s, s + m, greater<int>());
	for (int i = 0; i < m - 1; i++)
		s[i] -= s[i + 1];

	double ll = 0, rr = 3e6;
	set<double> q;
	for (int iter = 0; iter < 40; iter++)
	{
		double md = (ll + rr) / 2.0;
		nodes = 0;
		S = new_node();
		T = new_node();
		vector<double> tt;
		tt.clear();
		q.clear();
		for (int i = 0; i < n; i++)
		{
			addedge(new_node(), T, (double) p[i]);
			q.insert(r[i]);
			q.insert(d[i] + md);
		}
		for (set<double> :: iterator it = q.begin(); it != q.end(); it++)
			tt.push_back((*it));
		for (size_t i = 1; i < tt.size(); i++)
		{
			double ti = tt[i] - tt[i - 1];
			int cnt_nodes = nodes;
			for (int j = 0; j < m; j++)
				addedge(S, new_node(), ti * s[j] * (j + 1));
	
			for (int j = 0; j < m; j++)
				for (int k = 0; k < n; k++)
					if (r[k] <= tt[i - 1] && tt[i] <= d[k] + md)
						addedge(cnt_nodes + j, 2 + k, ti * s[j]);
		}
		int P = 0;
		for (int i = 0; i < n; i++)
			P += p[i];
		if (fabs(max_flow() - P) < 1e-9)
			rr = md;
		else
			ll = md;
	}
	cout.precision(13);
	cout<< fixed<< ll<< endl;
	return 0;
}
