#include <set>
#include <cmath>
#include <cstdio>
#include <vector>
#include <memory.h>
#include <iostream>
#include <algorithm>

using namespace std;

#define MAX_NODES 2000

struct E
{
	int to, r;
	double f, c;
	E() {}
	E(int to, double c, int r)
	{
		this->to = to;
		this->c = c;
		this->r = r;
		this->f = 0;
	}
};

bool u[MAX_NODES];
vector<E> edge[MAX_NODES];
int sum_p = 0, n, m, p[33], r[33], d[33], s[33], S, T, cnt_nodes;

bool dfs(int v, double d)
{
	if (v == T)
		return true;
	u[v] = true;
	for (size_t i = 0; i < edge[v].size(); i++)
	{
		E &t = edge[v][i];
		if (t.f + d <= t.c && !u[t.to] && dfs(t.to, d))
		{
			t.f += d;
			edge[t.to][t.r].f -= d;
			return true;
		}
	}
	return false;
}

double max_flow()
{
	double cu = 1 << 20, sum = 0;
	for (int i = 0; i < 40; i++, cu *= 0.5)
	while (true)
	{
		memset(u, 0, sizeof(u));
		if (!dfs(S, cu))
			break;
		sum += cu;
	}
	return sum;
}

int create_node()
{
	edge[cnt_nodes].clear();
	return cnt_nodes++;
}

void addedge(int a, int b, double c)
{
	edge[a].push_back(E(b, c, edge[b].size()));
	edge[b].push_back(E(a, 0, edge[a].size() - 1));
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	cin>> n>> m;
	for (int i = 0; i < n; i++)
	{
		cin>> p[i]>> r[i]>> d[i];
		sum_p += p[i];
	}
	for (int i = 0; i < m; i++)
		cin>> s[i];

	sort(s, s + m, greater<int>());
	for (int i = 0; i < m - 1; i++)
		s[i] -= s[i + 1];

	double ll = 0, rr = 3e6;
	set<double> q;
	for (int iter = 0; iter < 40; iter++)
	{
		double md = (ll + rr) / 2.0;
		cnt_nodes = 0;
		S = create_node();
		T = create_node();
		vector<double> t;
		t.clear();
		q.clear();
		for (int i = 0; i < n; i++)
		{
			addedge(S, create_node(), (double) p[i]);
			q.insert(r[i]);
			q.insert(d[i] + md);
		}
		for (set<double> :: iterator it = q.begin(); it != q.end(); it++)
			t.push_back((*it));

		for (size_t i = 1; i < t.size(); i++)
		{
			double interval = t[i] - t[i - 1];
			int node = cnt_nodes;
			for (int j = 0; j < m; j++)
				addedge(create_node(), T, interval * s[j] * (j + 1));

			for (int j = 0; j < m; j++)
				for (int k = 0; k < n; k++)
					if (r[k] <= t[i - 1] && t[i] <= d[k] + md)
						addedge(2 + k, node + j, interval * s[j]);
		}
		if (fabs(max_flow() - sum_p) < 1e-9)
			rr = md;
		else
			ll = md;
	}
	printf("%.13lf\n", ll);
	return 0;
}
