#include <set>
#include <algorithm>
#include <memory.h>
#include <climits>
#include <cstdio>
#include <iostream>
#include <bitset>
using namespace std;

#define N 1404

struct T
{
	int d, dm, id;
	T() {}
	T(int id_, int d_) : d(d_), dm(d_), id(id_) {}
	bool operator < (const T &x) const
	{
		return dm < x.dm || (dm == x.dm && id < x.id);
	}
} d[N];

// bitset<N> succ[N];
bool succ[N][N], a[N][N], used[N];
set<T> s;
int sch[2][N], deg[N], n, t, cur[2];

// void OR(bitset<N>& x, bitset<N> &y)
// {
// 	for (int i = 0; i < N; i++)
// 		x.set(i, x.test(i) | y.test(i));
// }

void dfs1(int v)
{
	used[v] = true;
	for (int i = 0; i < n; i++)
		if (a[v][i])
		{
			if (!used[i])
				dfs1(i);
			for (int j = 0; j < n; j++)
				succ[v][j] |= succ[i][j];

			// OR(succ[v], succ[i]);
			succ[v][i] = true;
			// succ[v].set(i);
		}
}

void dfs2(int v)
{
	used[v] = true;
	for (int i = 0; i < n; i++)
		if (succ[v][i] && !used[i])
		// if (succ[v].test(i) && !used[i])
			dfs2(i);

	int res = d[v].d, cnt = 0;
	for (set<T> :: iterator it = s.begin(); it != s.end(); it++)
		if (succ[v][(*it).id])
		// if (succ[v].test((*it).id))
		{
			++cnt;
			res = min(res, (*it).dm - (cnt + 1) / 2);
		}

	s.erase(s.find(d[v]));
	d[v].dm = res;
	s.insert(d[v]);
}

int main()
{
	freopen("p2precp1lmax.in", "r", stdin);
	freopen("p2precp1lmax.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &t);
		d[i] = T(i, t);
		s.insert(d[i]);
	}
	// for (set<T> :: iterator it = s.begin(); it != s.end(); it++) cout<< (*it).id<< " "; puts("");
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			scanf("%d", &t);
			a[i][j] = (t == 1);
		}

	// for (int i = 0; i < n; i++)
		// succ[i] = bit_set<n>;

	memset(used, false, sizeof(used));
	for (int i = 0; i < n; i++)
		if (!used[i])
			dfs1(i);

	memset(used, false, sizeof(used));
	for (int i = 0; i < n; i++)
		if (!used[i])
			dfs2(i);

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			deg[j] += a[i][j];

	t = 0;
	int cnt = 0, machine = 0, ans = INT_MIN;

	// for (set<T> :: iterator it = s.begin(); it != s.end(); it++) cout<< (*it).id<< " "; puts("");

	while (cnt < n)
	{
		cur[0] = cur[1] = INT_MIN;
		for (set<T> :: iterator it = s.begin(); it != s.end(); it++)
		{
			if (deg[(*it).id] == 0)
			{
				cur[machine] = (*it).id;
				machine ^= 1;
				++cnt;
			}
			if (cur[0] != INT_MIN && cur[1] != INT_MIN)
				break;
		}
		for (int i = 0; i < 2; i++)
		{
			int v = cur[i];
			if (v == INT_MIN)
			{
				sch[i][t] = -1;
				continue;
			}
			sch[i][t] = v + 1;
			ans = max(ans, t - d[v].d + 1);
			for (int j = 0; j < n; j++)
				deg[j] -= a[v][j];
			deg[v] = -1;
		}
		++t;
	}
	printf("%d %d\n", ans, t);
	for (int i = 0; i < 2; i++, puts(""))
		for (int j = 0; j < t; j++)
			printf("%d ", sch[i][j]);
			// cout<< sch[i][j]<< " ";
	return 0;
}
