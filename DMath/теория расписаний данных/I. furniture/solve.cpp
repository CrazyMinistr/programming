#include <memory.h>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#define MAXD 1000
#define MAXN 200
#define MAXM 100

struct T
{
	int d, id;
	int s[MAXM];
	T() {}
	T(int d, int id)
	{
		this->d = d;
		this->id = id;
		memset(s, 0, sizeof(s));
	}
	bool operator < (const T &x) const
	{
		return x.d < d;
	}
} d[MAXN];

bool sch[MAXD][MAXN];
int color[MAXD][MAXN], used_rows[MAXD][MAXM], used_col[MAXN][MAXM];
int n, m, v;

bool gen_schedule(int p)
{
	memset(sch, false, sizeof(sch));
	vector<int> cnt(MAXD, 0);
	for (int i = 0; i < p; i++)
	{
		for (int j = d[i].d; j > d[i].d - m; j--)
		{
			sch[j][i] = true;
			++cnt[j];
		}
		bool found = true;
		while (found)
		{
			found = false;
			for (int j = 0; j <= d[i].d; j++)
				if (cnt[j] > m)
				{
					if (j == 0)
						return false;
					for (int k = 0; k < n; k++)
						if (sch[j][k] && !sch[j - 1][k])
						{
							sch[j - 1][k] = true;
							sch[j][k] = false;
							--cnt[j];
							++cnt[j - 1];
							break;
						}
					found = true;
					break;
				}
		}
	}
	return true;
}

void fix(int x, int y, int c1, int c2)
{
	if (color[x][y] != -1)
		used_rows[x][color[x][y]] = -1,
		used_col[y][color[x][y]] = -1;

	color[x][y] = c1;
	if (used_rows[x][c1] != -1)
	{
		int ny = used_rows[x][c1];
		color[x][ny] = c2;
		used_col[ny][c1] = -1;
		if (used_col[ny][c2] != -1)
			fix(used_col[ny][c2], ny, c1, c2);

		used_rows[x][c2] = ny;
		used_col[ny][c2] = x;
	}
	used_rows[x][c1] = y;
	used_col[y][c1] = x;
}

bool my(T a, T b) { return a.id < b.id; }

int main()
{
	freopen("furniture.in", "r", stdin);
	freopen("furniture.out", "w", stdout);
	cin>> n>> m>> v;
	for (int i = 0; i < n; i++)
	{
		int x;
		cin>> x;
		d[i] = T(x - 1, i);
	}
	sort(d, d + n);
	int l = 0, r = n + 1;
	while (r >= 2 && d[r - 2].d < m - 1) r--;
	while (r - l > 1)
	{
		int md = (l + r) >> 1;
		if (gen_schedule(md))
			l = md;
		else
			r = md;
	}
	cout<< v * (n - l)<< endl;
	gen_schedule(l);
	memset(color, -1, sizeof(color));
	memset(used_rows, -1, sizeof(used_rows));
	memset(used_col, -1, sizeof(used_col));
	for (int i = 0; i < MAXD; i++)
		for (int j = 0; j < l; j++)
			if (sch[i][j])
				for (int k = 0; k < m; k++)
					if (used_col[j][k] == -1)
					{
						if (used_rows[i][k] == -1)
						{
							color[i][j] = k;
							used_rows[i][k] = j;
							used_col[j][k] = i;
						}
						else
						{
							for (int q = 0; q < m; q++)
								if (used_rows[i][q] == -1)
								{
									fix(i, j, k, q);
									break;
								}
						}
						break;
					}

	for (int i = 0; i < l; i++)
		for (int j = 0; j < MAXD; j++)
			if (sch[j][i])
				d[i].s[color[j][i]] = j + 1;

	for (int i = l; i < n; i++)
		for (int j = 0; j < m; j++)
			d[i].s[j] = MAXD + i * m + j;

	sort(d, d + n, &my);
	for (int i = 0; i < n; i++, puts(""))
		for (int j = 0; j < m; j++)
			cout<< d[i].s[j]<< " ";
	return 0;
}
