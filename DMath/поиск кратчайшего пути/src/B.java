import java.util.*;
import java.io.*;

public class B {
	FastScanner in;
	PrintWriter out;

	final int N = 2002;
	int[][] ms = new int[N][N];

	boolean[] used = new boolean[N];
	int[] d = new int[N];
	int n, s, f;

	void deik(int s) {
		for (int i = 1; i <= n; i++)
			if (ms[s][i] == -1)
				d[i] = (int) 1e9;
			else
				d[i] = ms[s][i];
		d[s] = 0;
		used[s] = true;
		int v = 0;
		for (int i = 1; i < n; i++)
		{
			int min = (int) 1e9;
			for (int j = 1; j <= n; j++)
				if (min > d[j] && !used[j])
				{
					v = j;
					min = d[j];
				}
			used[v] = true;
			for (int j = 1; j <= n; j++)
				if (ms[v][j] > -1 && !used[j] && min + ms[v][j] < d[j])
					d[j] = ms[v][j] + min;
		}
	}

	public void solve() throws IOException {
		n = in.nextInt();
		s = in.nextInt();
		f = in.nextInt();
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				ms[i][j] = in.nextInt();
		deik(s);
		if (d[f] == (int) 1e9)
			out.println("-1");
		else
			out.println(d[f]);
	}

	public void run() {
		try {
			in = new FastScanner(new File("pathmgep.in"));
			out = new PrintWriter(new File("pathmgep.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new B().run();
	}
}
