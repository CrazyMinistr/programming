import java.util.*;
import java.io.*;

public class F {
	FastScanner in;
	PrintWriter out;

	int[] d = new int[333];
	int[] p = new int[333];
	int[][] ms = new int[333][333];
	final int inf = (int) 1e9;
	
	public void solve() throws IOException {
		int n = in.nextInt();
		for (int i = 0; i < n; p[i++] = -1)
				for (int j = 0; j < n; j++)
					ms[i][j] = in.nextInt();
		int x = 0;
		for (int i = 0; i < n; i++)
		{
			x = -1;
			for (int j = 0; j < n; j++)
				for (int k = 0; k < n; k++)
					if (ms[j][k] < inf && d[k] > d[j] + ms[j][k])
					{
						d[k] = Math.max(-inf, d[j] + ms[j][k]);
						p[k] = j;
						x = k;
					}
		}
		if (x == -1)
			out.println("NO");
		else
		{
			int y = x;
			for (int i = 0; i < n; i++)
				y = p[y];

			ArrayList<Integer> ans = new ArrayList<Integer>();
			for (int cur = y; ; cur = p[cur])
			{
				ans.add(cur);
				if (cur == y && ans.size() > 1)
					break;
			}
			out.println("YES");
			out.println(ans.size());
			for (int i = ans.size() - 1; i >= 0; i--)
				out.print(ans.get(i) + 1 + " ");
		}
	}

	public void run() {
		try {
			in = new FastScanner(new File("negcycle.in"));
			out = new PrintWriter(new File("negcycle.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new F().run();
	}
}
