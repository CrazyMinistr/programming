import java.util.*;
import java.io.*;

public class C {
	FastScanner in;
	PrintWriter out;

	final int N = 222;
	int[][] ms = new int[N][N];
	
	public void solve() throws IOException {
		int n = in.nextInt();
		int m = in.nextInt();
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (i != j)
					ms[i][j] = (int) 1e9;

		for (int i = 0; i < m; i++)
		{
			int a = in.nextInt();
			int b = in.nextInt();
			int c = in.nextInt();
			if (ms[a][b] > c)
				ms[a][b] = c;
		}
		for (int k = 1; k <= n; k++)
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
					ms[i][j] = Math.min(ms[i][j], ms[i][k] + ms[k][j]);
		
		for (int i = 1; i <= n; i++, out.println(""))
			for (int j = 1; j <= n; j++)
				out.print(ms[i][j] + " ");
	}

	public void run() {
		try {
			in = new FastScanner(new File("pathsg.in"));
			out = new PrintWriter(new File("pathsg.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new C().run();
	}
}
