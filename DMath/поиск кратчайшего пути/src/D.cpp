#include<cstdio>
#include<queue>
#include<vector>
#include<algorithm>
#define N 33333
#define M 444444
using namespace std;

int n, m, d[N], x[M << 1], y[M << 1], w[M << 1], p[M << 1], Min, v, q, a, b, c, ii = 0;
priority_queue<pair<int, int> > Q;

void deik(int k)
{
	memset(d, 63, sizeof(d));
	d[k] = 0;
	Q.push(make_pair(0, k));
	while (!Q.empty())
	{
		Min = -Q.top().first;
		v = Q.top().second;
		Q.pop();
		if (d[v] < Min) continue;
		for (int q = x[v]; q; q = p[q])
			if (d[y[q]] > w[q] + Min)
			{
				d[y[q]] = w[q] + Min;
				Q.push(make_pair(-d[y[q]], y[q]));
			}
	}
}

int main()
{
	freopen("pathbgep.in", "r", stdin);
	freopen("pathbgep.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 0; i < m; i++)
	{
		scanf("%d%d%d", &a, &b, &c);
		y[++ii] = b, p[ii] = x[a], x[a] = ii, w[ii] = c;
		y[++ii] = a, p[ii] = x[b], x[b] = ii, w[ii] = c;
	}
	deik(1);
	for (int i = 1; i <= n; i++) printf("%d ", d[i]);
	return 0;
}
