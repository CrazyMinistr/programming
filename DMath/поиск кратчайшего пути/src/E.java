import java.util.*;
import java.io.*;

public class E {
	FastScanner in;
	PrintWriter out;

	final int N = 5555;
	int n, m, s, ii = 0;
	int[] y = new int[N];
	int[] x = new int[N];
	long[] z = new long[N];
	long[] d = new long[N];
	long[] u = new long[N];
	
	public void solve() throws IOException {
		n = in.nextInt();
		m = in.nextInt();
		s = in.nextInt();
		for (int i = 0; i < m; i++)
		{
			x[i] = in.nextInt();
			y[i] = in.nextInt();
			z[i] = in.nextLong();
		}
		long inf = (long) 3e18;
		for (int i = 1; i <= n; i++)
			d[i] = inf;
		d[s] = 0;
		boolean relax = true;
		int iter = 0;
		while (relax)
		{
			relax = false;
			for (int i = 0; i < m; i++)
				if (d[x[i]] < inf && d[y[i]] > -inf)
				{
					long w = d[x[i]] + z[i];
					if (w < d[y[i]])
					{
						relax = true;
						if (iter <= n)
						{
							d[y[i]] = w;
							if (d[y[i]] > inf)
								d[y[i]] = inf;
							if (d[y[i]] < -inf)
								d[y[i]] = -inf;
						}
						else
							d[y[i]] = -inf;
					}
				}
			++iter;
		}
		
		for (int i = 1; i <= n; i++)
			if (d[i] == inf)
				out.println("*");
			else if (d[i] == -inf)
				out.println("-");
			else
				out.println(d[i]);
	}

	public void run() {
		try {
			in = new FastScanner(new File("path.in"));
			out = new PrintWriter(new File("path.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong()
		{
			return Long.parseLong(next());
		}
	}

	public static void main(String[] arg) {
		new E().run();
	}
}
