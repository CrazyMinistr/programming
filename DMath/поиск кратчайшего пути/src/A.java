import java.util.*;
import java.io.*;

public class A {
	FastScanner in;
	PrintWriter out;

	final int N = 440000 * 2;
	int[] y = new int[N],
		  x = new int[N],
		  p = new int[N],
		  d = new int[N],
		  q = new int[N];

	int q1, q2;
	int ii = 0;
	
	void addedge(int a, int b)
	{
		y[++ii] = b;
		p[ii] = x[a];
		x[a] = ii;
	}
	
	void bfs(int v, int l)
	{
		if (d[v] <= l)
			return;
		d[v] = l;
		q[q1++] = v;
	}
	
	public void solve() throws IOException {
		int n = in.nextInt();
		int m = in.nextInt();
		for (int i = 1; i <= n; i++)
			d[i] = (int) 1e9;

		for (int i = 0; i < m; i++)
		{
			int a = in.nextInt(),
				b = in.nextInt();
			addedge(a, b);
			addedge(b, a);
		}
		q1 = 0;
		q2 = 0;
		bfs(1, 0);
		while (q1 != q2)
		{
			int V = q[q2++];
			for (int q = x[V]; q > 0; q = p[q])
				bfs(y[q], d[V] + 1);
		}
		for (int i = 1; i <= n; i++)
			out.print(d[i] + " ");
	}

	public void run() {
		try {
			in = new FastScanner(new File("pathbge1.in"));
			out = new PrintWriter(new File("pathbge1.out"));

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		FastScanner(File f) {
			try {
				br = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}

	public static void main(String[] arg) {
		new A().run();
	}
}
