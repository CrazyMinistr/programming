#include<cstdio>
#include<sstream>
#include<iostream>
#include<algorithm>
using namespace std;
#define N 200000 * 4

int mod, n, m;
struct T
{
	int a11, a12, a21, a22;
	T()
	{
		a11 = a22 = 1;
		a21 = a12 = 0;
	}
	T operator*(T const &b)
	{
		T c;
		c.a11 = (a11 * b.a11 + a12 * b.a21) % mod;
		c.a12 = (a11 * b.a12 + a12 * b.a22) % mod;
		c.a21 = (a21 * b.a11 + a22 * b.a21) % mod;
		c.a22 = (a21 * b.a12 + a22 * b.a22) % mod;
		return c;
	}
};

T tree[N], E;

void upd(int id, int l, int r, int num, T x)
{
	if (num < l || num > r) return;
	if (l == num && r == num)
	{
		tree[id] = x;
		return;
	}
	int m = (l + r) >> 1;
	upd(id << 1, l, m, num, x);
	upd((id << 1) + 1, m + 1, r, num, x);
	tree[id] = tree[id << 1] * tree[(id << 1) + 1];
}

T find(int id, int l, int r, int a, int b)
{
	if (b < l || a > r) return E;
	if (a <= l && r <= b) return tree[id];
	int m = (l + r) >> 1;
	return find(id << 1, l, m, a, b) * find((id << 1) + 1, m + 1, r, a, b);
}

int main()
{
	freopen("crypto.in", "r", stdin);
	freopen("crypto.out", "w", stdout);
	cin>> mod>> n>> m;
	for (int i = 0; i < n; i++)
	{
		T x;
		scanf("%d%d%d%d", &x.a11, &x.a12, &x.a21, &x.a22);
		upd(1, 1, n, i + 1, x);
	}
	while (m--)
	{
		int l, r;
		scanf("%d%d", &l, &r);
		T x = find(1, 1, n, l, r);
		printf("%d %d\n%d %d\n\n", x.a11, x.a12, x.a21, x.a22);
	}
}
