#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 500000 * 4

char s[3];
int n, l;
LL r, tree[N], x;

void upd(int id, int l, int r, int num, LL x)
{
	if (num < l || num > r) return;
	if (l == num && r == num)
	{
		tree[id] = x;
		return;
	}
	int m = (l + r) >> 1;
	upd(id << 1, l, m, num, x);
	upd((id << 1) + 1, m + 1, r, num, x);
	tree[id] = tree[id << 1] + tree[(id << 1) + 1];
}

LL find_sum(int id, int l, int r, int a, int b)
{
	if (b < l || a > r) return 0;
	if (a <= l && r <= b) return tree[id];
	int m = (l + r) >> 1;
	return (LL) find_sum(id << 1, l, m, a, b) + find_sum((id << 1) + 1, m + 1, r, a, b);
}

int main()
{
//*
	freopen("rsq.in", "r", stdin);
	freopen("rsq.out", "w", stdout);
//*/
	cin>> n;
	for (int i = 1; i <= n; i++)
	{
		scanf("%I64d", &x);
		upd(1, 1, n, i, x);
	}
	while (scanf("%s%d%I64d", s, &l, &r) == 3)
	{
		if (s[2] == 'm')
			printf("%I64d\n", find_sum(1, 1, n, l, (int) r));
		else
			upd(1, 1, n, l, r);
	}
	return 0;
}
