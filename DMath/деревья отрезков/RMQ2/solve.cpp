#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
#define LL long long
#define N 400000
#define inf 2000000000000000000ll

struct T
{
	LL add, color, Min;
	T()
	{
		add = 0;
		color = Min = inf;
	}
} tree[N];

int n, a, b, l, r;
char s[3];
LL x, v[N];

void build(int id, int l, int r)
{
	if (l > r) return;
	if (l == r)
	{
		tree[id].Min = v[l];
		return;
	}
	int m = (l + r) >> 1;
	build(id << 1, l, m);
	build((id << 1) + 1, m + 1, r);
	tree[id].Min = min(tree[id << 1].Min, tree[(id << 1) + 1].Min);
}

void push(int id)
{
	if (tree[id].color < inf)
	{
		tree[id << 1].Min = tree[(id << 1) + 1].Min =
		tree[id << 1].color = tree[(id << 1) + 1].color = tree[id].color;
		tree[id << 1].add = tree[(id << 1) + 1].add = tree[id].add;
		tree[id].color = inf;
		tree[id].add = 0;
	}
	else
	{
		tree[id << 1].add += tree[id].add;
		tree[(id << 1) + 1].add += tree[id].add;
		tree[id].add = 0;
	}
}

void inc(int id, int l, int r, int a, int b, LL x)
{
	if (b < l || a > r) return;
	if (a <= l && r <= b)
	{
		tree[id].add += x;
		return;
	}
	push(id);
	int m = (l + r) >> 1;
	inc(id << 1, l, m, a, b, x);
	inc((id << 1) + 1, m + 1, r, a, b, x);
	tree[id].Min = min(tree[id << 1].Min + tree[id << 1].add, tree[(id << 1) + 1].Min + tree[(id << 1) + 1].add);
}

void upd(int id, int l, int r, int a, int b, LL x)
{
	if (b < l || a > r) return;
	if (a <= l && r <= b)
	{
		tree[id].add = 0;
		tree[id].Min = tree[id].color = x;
		return;
	}
	push(id);
	int m = (l + r) >> 1;
	upd(id << 1, l, m, a, b, x);
	upd((id << 1) + 1, m + 1, r, a, b, x);
	tree[id].Min = min(tree[id << 1].Min + tree[id << 1].add, tree[(id << 1) + 1].Min + tree[(id << 1) + 1].add);
}

LL find(int id, int l, int r, int a, int b)
{
	if (b < l || a > r) return inf;
	if (tree[id].color < inf)
		return tree[id].color + tree[id].add;

	if (a <= l && r <= b)
		return tree[id].Min + tree[id].add;

	int m = (l + r) >> 1;
	return min(find(id << 1, l, m, a, b), find((id << 1) + 1, m + 1, r, a, b)) + tree[id].add;
}

int main()
{
	freopen("rmq2.in", "r", stdin);
	freopen("rmq2.out", "w", stdout);
	cin>> n;
	for (int i = 1; i <= n; i++)
		scanf("%I64d", &v[i]);

	build(1, 1, n);
	while (scanf("%s%d%d", s, &l, &r) == 3)
	{
		if (s[0] == 'm')
			printf("%I64d\n", find(1, 1, n, l, r));
		else
		{
			scanf("%I64d", &x);
			if (s[0] == 'a')
				inc(1, 1, n, l, r, x);
			else
				upd(1, 1, n, l, r, x);
		}
	}
	return 0;
}
