import java.io.*;
import java.util.*;

public class Task {
	static MyScan in;
	static PrintWriter out;

	public static void main(String[] args) throws IOException {
		in = new MyScan("rmq2.in");
		out = new PrintWriter("rmq2_.out");
		SegmentTreeRMQ2 stRmq2 = new SegmentTreeRMQ2();

		int n = in.nextInt();
		int[] a = new int[n];

		for (int i = 0; i < n; i++)
			a[i] = in.nextInt();
		stRmq2.build(a, 1, 0, n - 1);

		String s = in.nextToken();
		while (s != null) {
			int x = in.nextInt() - 1;
			int y = in.nextInt() - 1;
			long z;
			switch (s.charAt(0)) {
			case 'a':
				z = in.nextLong();
				stRmq2.add(1, 0, n - 1, x, y, z);
				break;
			case 's':
				z = in.nextLong();
				stRmq2.set(1, 0, n - 1, x, y, z);
				break;
			case 'm':
				out.println(stRmq2.min(1, 0, n - 1, x, y));
				break;
			}
			s = in.nextToken();
		}
		out.close();
	}

}

class SegmentTreeRMQ2 {
	private final int maxn = 100001;
	private final long maxv = (long) 2e18;
	private Node[] t = new Node[maxn * 4];

	private static class Node {
		public long min;
		public long add;
		public long newValue;

		public Node(long min, long add, long newValue) {
			this.min = min;
			this.add = add;
			this.newValue = newValue;
		}
	}

	public void build(int[] a, int v, int left, int right) {
		if (t[v] == null)
			t[v] = new Node(0, 0, maxv);
		if (left == right)
			t[v].min = a[left];
		else {
			int medium = (left + right) / 2;
			build(a, v * 2, left, medium);
			build(a, v * 2 + 1, medium + 1, right);
			t[v].min = Math.min(t[v * 2].min, t[v * 2 + 1].min);
		}
	}

	public long min(int v, int left, int right, int leftCurr, int rightCurr) {
		if (leftCurr > rightCurr)
			return maxv;
		if (t[v].newValue != maxv)
			return t[v].newValue + t[v].add;
		if (leftCurr == left && rightCurr == right) {
			if (t[v].newValue == maxv)
				return t[v].min + t[v].add;
			else
				return t[v].newValue + t[v].add;
		}
		int medium = (left + right) / 2;
		return Math
				.min(min(v * 2, left, medium, leftCurr,
						Math.min(rightCurr, medium)),
						min(v * 2 + 1, medium + 1, right,
								Math.max(leftCurr, medium + 1), rightCurr))
				+ t[v].add;
	}

	private void update(int v) {
		if (t[v].newValue != maxv) {
			t[v * 2].newValue = t[v].newValue;
			t[v * 2].min = t[v].newValue;
			t[v * 2].add = t[v].add;
			t[v * 2 + 1].newValue = t[v].newValue;
			t[v * 2 + 1].min = t[v].newValue;
			t[v * 2 + 1].add = t[v].add;
			t[v].newValue = maxv;
			t[v].add = 0;
		} else {
			t[v * 2].add += t[v].add;
			t[v * 2 + 1].add += t[v].add;
			t[v].add = 0;
		}
	}

	public void add(int v, int left, int right, int leftCurr, int rightCurr, long x) {
		if (leftCurr > rightCurr)
			return;
		if (leftCurr == left && rightCurr == right)
			t[v].add += x;
		else {
			update(v);
			int medium = (left + right) / 2;
			add(v * 2, left, medium, leftCurr, Math.min(rightCurr, medium), x);
			add(v * 2 + 1, medium + 1, right, Math.max(leftCurr, medium + 1),
					rightCurr, x);
			t[v].min = Math.min(t[v * 2].min + t[v * 2].add, t[v * 2 + 1].min
					+ t[v * 2 + 1].add);
		}
	}

	public void set(int v, int left, int right, int leftCurr, int rightCurr,
			long newValue) {
		if (leftCurr > rightCurr)
			return;
		if (leftCurr == left && rightCurr == right) {
			t[v].min = newValue;
			t[v].newValue = newValue;
			t[v].add = 0;
		} else {
			update(v);
			int medium = (left + right) / 2;
			set(v * 2, left, medium, leftCurr, Math.min(rightCurr, medium),
					newValue);
			set(v * 2 + 1, medium + 1, right, Math.max(leftCurr, medium + 1),
					rightCurr, newValue);
			t[v].min = Math.min(t[v * 2].min + t[v * 2].add, t[v * 2 + 1].min
					+ t[v * 2 + 1].add);
		}
	}
}

class MyScan {
	BufferedReader br;
	StringTokenizer st;

	public MyScan(String file) throws FileNotFoundException {
		br = new BufferedReader(new FileReader(file));
	}

	String nextToken() throws IOException {
		while (st == null || !st.hasMoreTokens()) {
			String s = br.readLine();
			if (s == null)
				return null;
			else
				st = new StringTokenizer(s);
		}
		return st.nextToken();
	}

	int nextInt() throws NumberFormatException, IOException {
		return Integer.parseInt(nextToken());
	}

	long nextLong() throws NumberFormatException, IOException {
		return Long.parseLong(nextToken());
	}

	double nextDouble() throws NumberFormatException, IOException {
		return Double.parseDouble(nextToken());
	}
}