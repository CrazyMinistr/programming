#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii
#define addedgeT(a, b) yT[++II] = b, pT[II] = xT[a], xT[a] = II
 
int y[N], x[N], p[N], ans[N], yT[N], xT[N], pT[N];
int n, m, a, b, num, ii = 0, order[N], top[N], nTop = 0, nOrder = 0, II = 0;
bool used[N];
 
void top_sort(int v)
{
    used[v] = 1;
    for (int q = x[v]; q; q = p[q])
        if (!used[y[q]])
            top_sort(y[q]);
    top[nTop++] = v;
}
 
void dfs(int v)
{
    used[v] = 1;
    order[nOrder++] = v;
    for (int q = xT[v]; q; q = pT[q])
        if (!used[yT[q]])
            dfs(yT[q]);
}
 
int main()
{
//*
    freopen("cond.in", "r", stdin);
    freopen("cond.out", "w", stdout);
//*/
    cin>> n>> m;
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d", &a, &b);
        addedge(a, b);
        addedgeT(b, a);
    }
    for (int i = 1; i <= n; i++)
        if (!used[i])
            top_sort(i);
    memset(used, 0, sizeof(used));
    for (int i = n; i; i--)
        if (!used[top[i - 1]])
        {
            ++num;
            nOrder = 0;
            dfs(top[i - 1]);
            for (int j = 0; j < nOrder; j++)
                ans[order[j]] = num;
        }
    cout<< num<< endl;
    for (int i = 1; i <= n; i++)
        printf("%d%c", ans[i], i < n ? ' ' : '\n');
    return 0;
}
