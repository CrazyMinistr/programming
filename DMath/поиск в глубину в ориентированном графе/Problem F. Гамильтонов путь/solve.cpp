#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii
 
int y[N], x[N], p[N], d[N];
int n, m, a, b, ii = 0;
bool f[N];
 
void dfs(int v)
{
    d[v] = 1;
    for (int q = x[v]; q; q = p[q])
        if (!d[y[q]])
            dfs(y[q]);

    for (int q = x[v]; q; q = p[q])
        d[v] = _max(d[v], d[y[q]] + 1);
}
 
int main()
{
//*
    freopen("hamiltonian.in", "r", stdin);
    freopen("hamiltonian.out", "w", stdout);
//*/
    cin>> n>> m;
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d", &a, &b);
        addedge(a, b);
        f[b] = 1;
    }
    for (int j = 1; j <= n; j++)
        if (!f[j])
        {
            dfs(j);
            puts(d[j] == n ? "YES" : "NO");
            break;
        }
    return 0;
}
