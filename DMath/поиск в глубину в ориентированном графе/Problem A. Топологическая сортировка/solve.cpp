#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
#define N 111111
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii
 
int ii = 0, n, m, a, b;
int y[N], x[N], p[N], used[N];
vector<int> ans;
 
void dfs(int v)
{
    used[v] = 1;
    for (int q = x[v]; q; q = p[q])
    {
        if (used[y[q]] == 1)
        {
            puts("-1");
            exit(0);
        }
        else if (!used[y[q]])
            dfs(y[q]);
    }
    used[v] = 2;
    ans.push_back(v);
}
 
int main()
{
//*
    freopen("topsort.in", "r", stdin);
    freopen("topsort.out", "w", stdout);
//*/
    cin>> n>> m;
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d", &a, &b);
        addedge(a, b);
    }
    for (int i = 1; i <= n; i++)
        if (!used[i])
            dfs(i);
    for (int i = (int) ans.size() - 1; i >= 0; i--)
        printf("%d%c", ans[i], i == 0 ? '\n' : ' ');
    return 0;
}