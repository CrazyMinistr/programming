#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii
#define N 111111
 
int ii = 0, n, m, a, b;
int used[N], y[N], x[N], p[N], pred[N];
vector<int> ans;
int cycle_st, cycle_end;
 
void dfs(int v)
{
    used[v] = 1;
    for (int q = x[v]; q && cycle_st == -1; q = p[q])
    {
        if (used[y[q]] == 0)
        {
            pred[y[q]] = v;
            dfs(y[q]);
        }
        else if (used[y[q]] == 1)
        {
            cycle_end = v;
            cycle_st = y[q];
            break;
        }
    }
    used[v] = 2;
}
 
int main()
{
//*
    freopen("cycle.in", "r", stdin);
    freopen("cycle.out", "w", stdout);
//*/
    cin>> n>> m;
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d", &a, &b);
        addedge(a, b);
    }
    cycle_st = -1;
    for (int i = 1; i <= n; i++)
        if (!used[i])
        {
            dfs(i);
            if (cycle_st != -1)
                break;
        }
    if (cycle_st == -1)
    {
        puts("NO");
        return 0;
    }
    puts("YES");
    for (int v = cycle_end; v != cycle_st; v = pred[v])
        ans.push_back(v);
 
    ans.push_back(cycle_st);
    if (ans.size() == 1)
        ans.push_back(ans[0]);
    reverse(ans.begin(), ans.end());
    for (size_t i = 0; i < ans.size(); i++)
        printf("%d%c", ans[i], i == ans.size() - 1 ? '\n' : ' ');
 
    return 0;
}
