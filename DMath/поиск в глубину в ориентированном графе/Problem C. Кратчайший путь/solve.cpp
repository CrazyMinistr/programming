#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<queue>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define N 222222
#define INF (int) 1e9
#define addedge(a, b, c) y[++ii] = b, p[ii] = x[a], x[a] = ii, w[ii] = c
#define mp make_pair
 
int ii = 0, n, m, s, t, a, b, c, nt = 0;
int y[N], x[N], p[N], w[N], d[N], pred[N], top[N];
bool used[N];
 
void top_sort(int v)
{
    used[v] = 1;
    for (int q = x[v]; q; q = p[q])
        if (!used[y[q]])
            top_sort(y[q]);
    top[++nt] = v;
}
 
int main()
{
//*
    freopen("shortpath.in", "r", stdin);
    freopen("shortpath.out", "w", stdout);
//*/
    cin>> n>> m>> s>> t;
    if (s == t)
    {
        puts("0");
        return 0;
    }
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d%d", &a, &b, &c);
        addedge(a, b, c);
    }
    top_sort(s);
    reverse(top + 1, top + nt + 1);
 
    for (int i = 1; i <= n; i++)
        d[i] = INF;
    d[s] = 0;
 
    for (int i = 1; i <= nt; i++)
        for (int q = x[top[i]]; q; q = p[q])
            d[y[q]] = _min(d[y[q]], d[top[i]] + w[q]);
 
    if (d[t] == INF)
    {
        puts("Unreachable");
        return 0;
    }
    cout<< d[t]<< endl;
    return 0;
}
