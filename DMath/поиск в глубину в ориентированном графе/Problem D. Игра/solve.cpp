#include<ctime>
#include<cstdio>
#include<vector>
#include<string>
#include<climits>
#include<cstdlib>
#include<cstddef>
#include<sstream>
#include<string.h>
#include<iostream>
#include<algorithm>
#define LL long long
#define _max(a, b) ((a) > (b) ? (a) : (b))
#define _min(a, b) ((a) < (b) ? (a) : (b))
using namespace std;
#define addedge(a, b) y[++ii] = b, p[ii] = x[a], x[a] = ii
#define N 111111
 
int n, m, s, a, b, ii = 0;
int y[N], x[N], p[N];
bool used[N], d[N];
 
void dfs(int v)
{
    used[v] = 1;
    for (int q = x[v]; q; q = p[q])
        if (!used[y[q]])
            dfs(y[q]);
    d[v] = 1;
    for (int q = x[v]; q; q = p[q])
        d[v] &= d[y[q]];
    d[v] ^= 1;
}
 
int main()
{
//*
    freopen("game.in", "r", stdin);
    freopen("game.out", "w", stdout);
//*/
    cin>> n>> m>> s;
    for (int i = 0; i < m; i++)
    {
        scanf("%d%d", &a, &b);
        addedge(a, b);
    }
    dfs(s);
    puts(d[s] ? "First player wins" : "Second player wins");
    return 0;
}
