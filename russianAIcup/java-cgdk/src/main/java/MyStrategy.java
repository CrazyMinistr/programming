import model.*;

import java.util.*;

public final class MyStrategy implements Strategy {
    // TODO: move to bonus

    Trooper self;
    World world;
    Game game;
    Move move;

    static Point middle = null;
    static Point suitablePoint = null;

    static final int[] dx = {1, 0, -1, 0};
    static final int[] dy = {0, 1, 0, -1};

    static final int minHealth = 61;
    static final int minCriticalHealth = 55;
    static final int minDistanceToTheTeam = 5;
    static final int minDistanceToSuitablePoint = 10;

    static final int moveIndexForRequestEnemy = 13;

    static Point commanderPoint = null;
    static Point medicPoint = null;
    static Point soldierPoint = null;

    static boolean isCommanderNeedSeatDown = false;
    static boolean isMedicNeedSeatDown = false;
    static boolean isSoldierNeedSeatDown = false;

    static boolean wasCall = false;

    static int currentDist = 0;

    class Point {
        int x, y;
        Point() {}

        Point(Point v) {
            this.x = v.x;
            this.y = v.y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (!(o instanceof Point))
                return false;

            Point point = (Point) o;
            return this.x == point.x && this.y == point.y;
        }

        Point(int _x, int _y) {
            this.x = _x;
            this.y = _y;
        }
    }

    private boolean enemiesIsVisible() {
        Trooper[] troopers = world.getTroopers();
        for (Trooper trooper : troopers)
            if (!trooper.isTeammate())
                return true;

        return false;
    }

    private Point searchSuitablePoint(Point point) {
        int pMin = Integer.MAX_VALUE;
        Point q = null;
        CellType[][] cellTypes = world.getCells();
        for (int i = 0; i < world.getWidth(); i++)
            for (int j = 0; j < world.getHeight(); j++)
                if (cellTypes[i][j] == CellType.FREE) {
                    if (suitablePoint != null && i == suitablePoint.x && j == suitablePoint.y)
                        continue;

                    int dMin = Math.abs(i - point.x) + Math.abs(j - point.y);
                    if (dMin < pMin) {
                        pMin = dMin;
                        q = new Point(i, j);
                    }
                }
        return q;
    }

    private Point getTeammateTypePoint(TrooperType type) {
        Trooper[] troopers = world.getTroopers();
        for (Trooper trooper : troopers)
            if (trooper.isTeammate() && trooper.getType() == type)
                return new Point(trooper.getX(), trooper.getY());
        return null;
    }

    private boolean shoot() {
        Trooper[] troopers = world.getTroopers();
        ArrayList<Trooper> list = new ArrayList<Trooper>();
        for (Trooper trooper : troopers)
            if (!trooper.isTeammate())
                list.add(trooper);

        Collections.sort(list, new Comparator<Trooper>() {
            @Override
            public int compare(Trooper x, Trooper y) {
                return Integer.compare(x.getHitpoints(), y.getHitpoints());
            }
        });

        if (self.isHoldingGrenade() && self.getActionPoints() >= game.getGrenadeThrowCost()) {
            for (Trooper trooper : list) {
                boolean canThrowGrenade = self.getDistanceTo(trooper) <= game.getGrenadeThrowRange();
                if (canThrowGrenade) {
                    move.setAction(ActionType.THROW_GRENADE);
                    move.setX(trooper.getX());
                    move.setY(trooper.getY());
                    return true;
                }
            }
        }

        if (self.getActionPoints() >= self.getShootCost()) {
            for (Trooper trooper : list) {
                if (self.getStance() == TrooperStance.KNEELING &&
                    self.getActionPoints() >= game.getStanceChangeCost() + self.getShootCost() &&
                    world.isVisible(self.getShootingRange(), self.getX(), self.getY(), TrooperStance.PRONE,
                                    trooper.getX(), trooper.getY(), trooper.getStance())) {
                        move.setAction(ActionType.LOWER_STANCE);
                        switch (self.getType()) {
                            case COMMANDER:
                                isCommanderNeedSeatDown = true;
                                break;
                            case FIELD_MEDIC:
                                isMedicNeedSeatDown = true;
                                break;
                            case SOLDIER:
                                isSoldierNeedSeatDown = true;
                                break;
                        }
                        return true;
                }
                boolean canShoot = world.isVisible(self.getShootingRange(), self.getX(), self.getY(), self.getStance(),
                        trooper.getX(), trooper.getY(), trooper.getStance());

                if (canShoot) {
                    move.setAction(ActionType.SHOOT);
                    move.setX(trooper.getX());
                    move.setY(trooper.getY());
                    return true;
                }
            }
        }
        return false;
    }

    private Point bfs(Point st, Point fn) {
        int n = world.getWidth(), m = world.getHeight();
        int[][] d = new int[n][m];
        Point[][] back = new Point[n][m];
        CellType[][] cells = world.getCells();
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                d[i][j] = Integer.MAX_VALUE;
        d[st.x][st.y] = 0;

        Queue<Point> q = new ArrayDeque<Point>();
        q.add(new Point(st.x, st.y));
        while (!q.isEmpty()) {
            Point v = q.poll();
            for (int i = 0; i < 4; i++) {
                int px = v.x + dx[i];
                int py = v.y + dy[i];
                if (px >= n || px < 0 || py >= m || py < 0)
                    continue;

                if((px != fn.x || py != fn.y) &&
                        (px == commanderPoint.x && py == commanderPoint.y ||
                                px == soldierPoint.x && py == soldierPoint.y ||
                                px == medicPoint.x && py == medicPoint.y))
                    continue;

                if (cells[px][py] == CellType.FREE && d[px][py] > d[v.x][v.y] + 1) {
                    d[px][py] = d[v.x][v.y] + 1;
                    back[px][py] = new Point(v);
                    q.add(new Point(px, py));
                }
            }
        }
        currentDist = d[fn.x][fn.y];
        Point result = new Point(fn.x, fn.y);
        while (result.x != st.x || result.y != st.y) {
            if (back[result.x][result.y].equals(st))
                return new Point(result);

            result = back[result.x][result.y];
        }
        return new Point(st.x, st.y);
    }

    private void changeStance() {
        TrooperStance curStance = self.getStance();
        ActionType action = ActionType.LOWER_STANCE;
        if (curStance == TrooperStance.PRONE)
            action = ActionType.RAISE_STANCE;

        move.setAction(action);
    }

    private boolean maySeatDown(TrooperType type) {
        if (self.getActionPoints() < game.getStanceChangeCost())
            return false;

        boolean ok = false;
        switch (type) {
            case COMMANDER:
                if (self.getDistanceTo(suitablePoint.x, suitablePoint.y) < minDistanceToSuitablePoint &&
                        self.getStance() != TrooperStance.KNEELING)
                    isCommanderNeedSeatDown = true;
                if (isCommanderNeedSeatDown) {
                    ok = true;
                    changeStance();
                    isCommanderNeedSeatDown = false;
                }
                break;
            case FIELD_MEDIC:
                if (self.getDistanceTo(suitablePoint.x, suitablePoint.y) < minDistanceToSuitablePoint &&
                        self.getStance() != TrooperStance.KNEELING)
                    isMedicNeedSeatDown = true;
                if (isMedicNeedSeatDown) {
                    ok = true;
                    changeStance();
                    isMedicNeedSeatDown = false;
                }
                break;
            case SOLDIER:
                if (self.getDistanceTo(suitablePoint.x, suitablePoint.y) < minDistanceToSuitablePoint &&
                        self.getStance() != TrooperStance.KNEELING)
                    isSoldierNeedSeatDown = true;
                if (isSoldierNeedSeatDown) {
                    ok = true;
                    changeStance();
                    isSoldierNeedSeatDown = false;
                }
                break;
        }
        return ok;
    }

    private int getMoveCost() {
        int moveCost = 0;
        switch (self.getStance()) {
            case PRONE:
                moveCost = game.getProneMoveCost();
                break;
            case KNEELING:
                moveCost = game.getKneelingMoveCost();
                break;
            case STANDING:
                moveCost = game.getStandingMoveCost();
                break;
        }
        return moveCost;
    }

    private void actionMove(TrooperType type) {
        if (maySeatDown(type))
            return;

        int moveCost = getMoveCost();
        if (self.getActionPoints() < moveCost)
            return;

        Point point = null;
        Point pointCommander = getTeammateTypePoint(TrooperType.COMMANDER);
        Point pointSoldier = getTeammateTypePoint(TrooperType.SOLDIER);
        Point pointMedic = getTeammateTypePoint(TrooperType.FIELD_MEDIC);
        Point curXY = new Point(self.getX(), self.getY());

        if (type == TrooperType.COMMANDER) {
            point = suitablePoint;
        }
        if (type == TrooperType.SOLDIER) {
            int min1 = 1000,
                min2 = 1000;

            if (pointCommander != null) {
                bfs(curXY, pointCommander);
                min1 = currentDist;
            }
            if (pointMedic != null) {
                bfs(curXY, pointMedic);
                min2 = currentDist + 5;
            }
            if (min1 < min2)
                point = pointCommander;
            else {
                if (min2 < min1)
                    point = pointMedic;
                else if (min1 != 1000)
                    point = pointCommander;
                else
                    point = suitablePoint;
            }
        }
        if (type == TrooperType.FIELD_MEDIC) {
            int min1 = Integer.MAX_VALUE,
                min2 = Integer.MAX_VALUE;

            if (pointCommander != null) {
                bfs(curXY, pointCommander);
                min1 = currentDist;
            }
            if (pointSoldier != null) {
                bfs(curXY, pointSoldier);
                min2 = currentDist + 5;
            }
            if (min1 < min2)
                point = pointCommander;
            else {
                if (min2 < min1)
                    point = pointSoldier;
                else if (min1 != 1000)
                    point = pointCommander;
                else
                    point = suitablePoint;
            }
        }
        Point nextPoint = bfs(curXY, point);
        move.setAction(ActionType.MOVE);
        move.setX(nextPoint.x);
        move.setY(nextPoint.y);
    }

    private boolean useMedikit(Trooper trooper, int minHitpoints, ActionType actionType) {
        if (trooper.getHitpoints() < minHitpoints) {
            move.setAction(actionType);
            move.setX(trooper.getX());
            move.setY(trooper.getY());
            return true;
        }
        return false;
    }

    private void moveCommander() {
        if (world.getMoveIndex() > 0 && world.getMoveIndex() % moveIndexForRequestEnemy == 0) {
            move.setAction(ActionType.REQUEST_ENEMY_DISPOSITION);
            return;
        }
        if (world.getMoveIndex() > 0 && (world.getMoveIndex() % (moveIndexForRequestEnemy + 1) == 0) || wasCall) {
            Player[] players = world.getPlayers();
            double minDist = Integer.MAX_VALUE;
            for (Player player : players) {
                if (player.getApproximateX() != -1 && !player.getName().equals("Ministr") && !player.getName().equals("MyStrategy")) {
                    Point ps = new Point(player.getApproximateX(), player.getApproximateY());
                    Point pf = searchSuitablePoint(ps);
                    if (minDist > self.getDistanceTo(pf.x, pf.y)) {
                        minDist = self.getDistanceTo(pf.x, pf.y);
                        suitablePoint = pf;
                    }
                }
            }
            System.out.println(world.getMoveIndex());
            if (minDist == Integer.MAX_VALUE) {
                wasCall = true;
                move.setAction(ActionType.REQUEST_ENEMY_DISPOSITION);
                return;
            }
            wasCall = false;
            return;
        }
        if (self.isHoldingMedikit()) {
            if (self.getActionPoints() >= game.getMedikitUseCost()) {
                if (useMedikit(self, minHealth, ActionType.USE_MEDIKIT))
                    return;

                Trooper[] troopers = world.getTroopers();
                for (Trooper trooper : troopers) {
                    if (!trooper.isTeammate())
                        continue;

                    for (int j = 0; j < 4; j++)
                        if (self.getX() == trooper.getX() + dx[j] && self.getY() == trooper.getY() + dy[j] &&
                                useMedikit(trooper, minHealth, ActionType.USE_MEDIKIT))
                            return;
                }
            }
        }
        if (enemiesIsVisible())
            if (shoot())
                return;

        actionMove(TrooperType.COMMANDER);
    }

    private void moveMedic() {
        if (self.getActionPoints() >= game.getFieldMedicHealCost()) {
            if (useMedikit(self, minHealth, ActionType.HEAL))
                return;

            int commanderHealth = 0,
                    soldierHealth = 0,
                    medicHealth = 0;

            Point commanderXY = null,
                    soldierXY = null,
                    medicXY = null;

            Trooper commander = null,
                    soldier = null,
                    medic = null;

            Trooper[] troopers = world.getTroopers();
            for (Trooper trooper : troopers) {
                if (!trooper.isTeammate())
                    continue;

                switch (trooper.getType()) {
                    case COMMANDER:
                        commander = trooper;
                        commanderHealth = trooper.getHitpoints();
                        commanderXY = new Point(trooper.getX(), trooper.getY());
                        break;
                    case FIELD_MEDIC:
                        medic = trooper;
                        medicHealth = trooper.getHitpoints();
                        medicXY = new Point(trooper.getX(), trooper.getY());
                        break;
                    case SOLDIER:
                        soldier = trooper;
                        soldierHealth = trooper.getHitpoints();
                        soldierXY = new Point(trooper.getX(), trooper.getY());
                        break;
                }
            }
            if (commanderHealth == 0 && soldierHealth == 0) {
                if (enemiesIsVisible())
                    if (shoot())
                        return;
            }

            if (0 < commanderHealth && commanderHealth < minCriticalHealth) {
                if (self.getDistanceTo(commanderXY.x, commanderXY.y) == 1) {
                    if (self.isHoldingMedikit() && self.getActionPoints() >= game.getMedikitUseCost())
                        useMedikit(commander, minCriticalHealth, ActionType.USE_MEDIKIT);
                    else
                        useMedikit(commander, minCriticalHealth, ActionType.HEAL);
                    return;
                } else {
                    if (self.getActionPoints() >= getMoveCost()) {
                        Point nextPoint = bfs(medicXY, commanderXY);
                        move.setAction(ActionType.MOVE);
                        move.setX(nextPoint.x);
                        move.setY(nextPoint.y);
                    }
                }
            }
            if (0 < soldierHealth && soldierHealth < minCriticalHealth && soldierHealth < medicHealth) {
                if (self.getDistanceTo(soldierXY.x, soldierXY.y) == 1) {
                    if (self.isHoldingMedikit() && self.getActionPoints() >= game.getMedikitUseCost())
                        useMedikit(soldier, minCriticalHealth, ActionType.USE_MEDIKIT);
                    else
                        useMedikit(soldier, minCriticalHealth, ActionType.HEAL);
                    return;
                } else {
                    if (self.getActionPoints() >= getMoveCost()) {
                        Point nextPoint = bfs(medicXY, soldierXY);
                        move.setAction(ActionType.MOVE);
                        move.setX(nextPoint.x);
                        move.setY(nextPoint.y);
                    }
                }
            }
            if (0 < medicHealth && medicHealth < minCriticalHealth) {
                if (self.isHoldingMedikit() && self.getActionPoints() >= game.getMedikitUseCost())
                    useMedikit(medic, minCriticalHealth, ActionType.USE_MEDIKIT);
                else
                    useMedikit(medic, minCriticalHealth, ActionType.HEAL);
                return;
            }
            if (enemiesIsVisible())
                if (shoot())
                    return;

            if (0 < commanderHealth && commanderHealth < 100) {
                if (self.getDistanceTo(commanderXY.x, commanderXY.y) == 1) {
                    if (self.isHoldingMedikit() && self.getActionPoints() >= game.getMedikitUseCost())
                        if (useMedikit(commander, minHealth, ActionType.USE_MEDIKIT))
                            return;
                    useMedikit(commander, 100, ActionType.HEAL);
                    return;
                } else {
                    if (self.getActionPoints() >= getMoveCost()) {
                        Point nextPoint = bfs(medicXY, commanderXY);
                        move.setAction(ActionType.MOVE);
                        move.setX(nextPoint.x);
                        move.setY(nextPoint.y);
                    }
                }
            }
            if (0 < soldierHealth && soldierHealth < 100) {
                if (self.getDistanceTo(soldierXY.x, soldierXY.y) == 1) {
                    if (self.isHoldingMedikit() && self.getActionPoints() >= game.getMedikitUseCost())
                        if (useMedikit(soldier, minHealth, ActionType.USE_MEDIKIT))
                            return;
                    useMedikit(soldier, 100, ActionType.HEAL);
                    return;
                } else {
                    if (self.getActionPoints() >= getMoveCost()) {
                        Point nextPoint = bfs(medicXY, soldierXY);
                        move.setAction(ActionType.MOVE);
                        move.setX(nextPoint.x);
                        move.setY(nextPoint.y);
                    }
                }
            }
            if (useMedikit(medic, 100, ActionType.HEAL))
                return;
        }
        if (enemiesIsVisible())
            if (shoot())
                return;

        actionMove(TrooperType.FIELD_MEDIC);
    }

    private void moveSoldier() {
        if (self.isHoldingMedikit()) {
            if (self.getActionPoints() >= game.getMedikitUseCost()) {

                if (useMedikit(self, minHealth, ActionType.USE_MEDIKIT))
                    return;

                Trooper[] troopers = world.getTroopers();
                for (Trooper trooper : troopers) {
                    if (!trooper.isTeammate())
                        continue;

                    for (int j = 0; j < 4; j++)
                        if (self.getX() == trooper.getX() + dx[j] && self.getY() == trooper.getY() + dy[j] &&
                                useMedikit(trooper, minHealth, ActionType.USE_MEDIKIT))
                            return;
                }
            }
        }
        if (enemiesIsVisible())
            if (shoot())
                return;

        actionMove(TrooperType.SOLDIER);
    }

    private void refreshPoints() {
        middle = new Point(world.getWidth() / 2 - 1, world.getHeight() / 2);
        Trooper[] troopers = world.getTroopers();
        for (Trooper trooper : troopers)
            if (trooper.isTeammate()) {
                switch (trooper.getType()) {
                    case COMMANDER:
                        commanderPoint = new Point(trooper.getX(), trooper.getY());
                        break;
                    case FIELD_MEDIC:
                        medicPoint = new Point(trooper.getX(), trooper.getY());
                        break;
                    case SOLDIER:
                        soldierPoint = new Point(trooper.getX(), trooper.getY());
                        break;
                }
            }
    }

    @Override
    public void move(Trooper self, World world, Game game, Move move) {
        this.self = self;
        this.world = world;
        this.game = game;
        this.move = move;

        refreshPoints();
        if (suitablePoint == null) {
            suitablePoint = searchSuitablePoint(middle);
        }

        if (self.getType() == TrooperType.COMMANDER) {
            moveCommander();
            commanderPoint = new Point(self.getX(), self.getY());
        }

        if (self.getType() == TrooperType.FIELD_MEDIC) {
            moveMedic();
            medicPoint = new Point(self.getX(), self.getY());
        }

        if (self.getType() == TrooperType.SOLDIER) {
            moveSoldier();
            soldierPoint = new Point(self.getX(), self.getY());
        }
    }
}
